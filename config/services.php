<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
	'stripe' => [
		'model'  => App\User::class,
		'key' => config("data.STRIPE_KEY"),
		'secret' => config("data.STRIPE_SECRET"),
	],
	'google' => [
        /*'client_id' => '1067634242381-nc6ke8b973rf8j2m0iofgh52if10ijm7.apps.googleusercontent.com',
        'client_secret' => 'tSkM1IC4MLz7gPUvEfiIFwhK',*/
		
		'client_id' => '54120529448-j288fvs2d8igb5nr9ppf83i42p4pqrcg.apps.googleusercontent.com',
        'client_secret' => 'Wfwahpz6PLnqBUQqgCoPsWym',
		
		'redirect' => 'https://airbnbcleans.com/google-login',
    ],
    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_CALLBACK_URL'),
    ],	
];
