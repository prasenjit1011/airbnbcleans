<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Location;
use App\CustomerCalendar;
use App\CleanerCalendar;
use App\CustVisitCleanerProfile;
use App\CleanerCalendarSlot;
use App\CustomerCalendarRequest;
use App\Mail\SendgridEmail;

use App\Address;
use App\Plan;
use App\Subscription;
use App\UsersMessage;
use DB;

class HomeController extends Controller
{
    /**
     * This controller will be use by Cleaner and Customer  
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $userType = Auth::user()->type;
			if($userType == 2){
				return redirect('admin/calendar');
			}else{
				return $next($request);
			}
		});	
    }

    public function index()
    {
		if(Auth::user()->type == 2){
			return redirect()->route('calendarmaster');
			return view('adminhome');
		}
		else{
			$sql = "update users set location_id = 1";
			DB::update($sql);
			
			$sql = "update  `schedule_cust_calendar` set cleaner_id = 13 where cleaner_id = 0";
			//DB::update($sql);
			
			$locationlist 	= Location::all();
			$userlist 		= array();
			$res = array();
			if(Auth::user()->type == 1){
				$chatUserType 	= 3;
			}else if(Auth::user()->type == 3){
				$chatUserType 	= 1;
			}
			
			$res 	= User::all();
			foreach($res as $val){
				$userlist[$val->id] = $val;
			}
			$addresslist 	= array();
			$res 			= Address::all();
			foreach($res as $val){
				$addresslist[$val->id] = $val;
			}
			
			$userAddress 	= Address::where('user_id',Auth::user()->id)->get();
			//dd($userAddress);
			$plans 		= Plan::all();
			$plan 		= $plans[0];
			$whr 		= array();
			$orderIds 	= array();
			if(Auth::user()->type == 1){
				$whr['customer_id'] = Auth::user()->id;
				$bookinglist	= CustomerCalendar::where($whr);
				//$bookinglist	= $bookinglist->where('dtd','>',date('Y-m-d H:i'));
				//$bookinglist	= $bookinglist->where('created_at','>',date('Y-m-d H:i:s',strtotime('-24 hours')))
				$bookinglist	= $bookinglist->orderBy('id', 'DESC')->get();	
																
				$cleanerRequestIds 		= array();
				$pendingBookingListArr	= array();
				
				$pendingBookingList 	= CustomerCalendar::select('id')->where($whr)->where('cleaner_id',0)->get()->toArray();;				
				foreach($pendingBookingList as $val){
					$pendingBookingListArr[] = $val['id'];
				}
				if(count($pendingBookingListArr)>0){
					$res = CustomerCalendarRequest::select('order_id','cleaner_id')->whereIn('order_id',$pendingBookingListArr)->get()->toArray();					
					foreach($res as $val){
						$cleanerRequestIds[$val['order_id']][] = $val['cleaner_id'];
					}
				}								
				//dd($cleanerRequestIds,$pendingBookingListArr);
			}
			elseif(Auth::user()->type == 3){
				$whr['cleaner_id'] = Auth::user()->id;
				
				$t			= time()-24*60*60;				
				$orderRequest = CustomerCalendarRequest::select('order_id')->where(array('status'=>1,array('created_at','>',date('Y-m-d H:i:s',$t)),'cleaner_id'=>Auth::user()->id))->distinct()->get()->toArray();
				foreach($orderRequest as $val){
					$orderIds[] = $val['order_id'];
				}
				
				
				DB::connection()->enableQueryLog();
				$bookinglist	= CustomerCalendar::where($whr);
				if(count($orderIds)>0){
					$bookinglist	= $bookinglist->orWhereIn('id', $orderIds);
				}
				
				//$bookinglist	= $bookinglist->where('dtd','>',date('Y-m-d H:i'));
				//$bookinglist	= $bookinglist->where('created_at','>',date('Y-m-d H:i:s',strtotime('-24 hours')))
				$bookinglist	= $bookinglist->orderBy('id', 'DESC')->get();//->toArray();				
				
				$queries = DB::getQueryLog();
				//dd($queries[0],$orderIds,$bookinglist);
			}
			else{
				die('-: Not Valid User Type :- ');
			}

			
			$bookingarr			= array();
			foreach($bookinglist as $val){
				$bookingarr[$val['dtd']][] = $val;
			}
			$orderlist			= Subscription::where('user_id',Auth::user()->id)->orderBy('id', 'DESC')->get();
			$cleanerCalendar	= array();
			/*$res 				= CleanerCalendar::where('cleaner_id',Auth::user()->id)->where('dtd','>=',date('Y-m-d'))->orderBy('dtd')->orderBy('start_time')->get()->toArray();
			foreach($res as $val){
				$cleanerCalendar[$val['dtd']][]	 = $val;
			}*/
			$res 				= CleanerCalendarSlot::where('cleaner_id',Auth::user()->id)->where('dtd','>=',date('Y-m-d'))->orderBy('dtd')->orderBy('slot')->get()->toArray();
			foreach($res as $val){
				$cleanerCalendar[$val['dtd']][]	 = $val;
			}
			//dd($cleanerCalendar);
			
			
			$viewedcnt	= CustVisitCleanerProfile::where('cleaner_id',Auth::user()->id)->count();			
			//dd($bookingarr);
			
			$myplan_option	= array();
			$plan_option 	= '';
			$chkstr 		= '';
			//$plan_option 	= '<select id="selectedPlan" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:50%;" onchange="searchcleaner()" >';			
			$planchk		= 1;
			$dtd			= date('Y-m-d',strtotime('-30 days'));
			$onetimeclean 	= 1;
			$expressclean 	= 1;
			$jumboclean 	= 1;
			$tokenArr		= array('onetimeclean'=>0,'expressclean'=>0,'jumboclean'=>0);
			$res 			= Subscription::where('user_id',Auth::user()->id)->where('created_at','>',$dtd)->orderBy('id', 'DESC')->get()->toArray();
			
			$myPlans 				= array();
			$myPlans['oneTime'] 		= 0;
			$myPlans['expressTime'] 	= 0;
			$myPlans['jumboTime'] 		= 0;
			foreach($res as $val){
				//dd($val);
				if($val['stripe_plan'] == config('data.onetimeclean')){					
					$tokenArr['onetimeclean']	+= $val['quantity']-$val['used_quantity'];					
					if($onetimeclean){
						$myplan_option[] = 1;
						$myPlans['oneTime'] 		= 1;						
						$onetimeclean 	 = 0;
						
						$clr 		= '1abc9c';
						$chkstr 	= '';
						if($planchk){
							
							$planchk 	= 0;
							$chkstr 	= 'checked';
							$clr 		= 'bc2653';
							$plan_option 	.= '<input type="hidden" name="selectedPlan" id="selectedPlan" value="1"  style="display:none1;"  />';
						}
						
						
						$plan_option 	.= '<span id="sp1" style="color:#fff; background-color:#'.$clr.'; padding: 5px; margin:0 5px; cursor:pointer;" onclick="changePlan(1)">OneTime</span>';
						//$plan_option 	.= '<option value="1">One Time Clean</option>';
					}
				}
				elseif($val['stripe_plan'] == config('data.expressclean')){					
					$tokenArr['expressclean']	+= $val['quantity']-$val['used_quantity'];					
					if($expressclean){
						$myplan_option[] = 2;
						$myPlans['expressTime'] 	= 1;						
						
						$expressclean 	= 0;
						
						$clr 		= '1abc9c';
						$chkstr 	= '';
						if($planchk){
							
							$planchk 	= 0;
							
							$clr 		= 'bc2653';
							$plan_option	.= '<input type="hidden" name="selectedPlan" id="selectedPlan" value="2"  style="display:none1;" />';
						}
						
						
						
						
						$plan_option 	.= '<span id="sp2" style="color:#fff; background-color:#'.$clr.'; padding: 5px; margin:0 5px; cursor:pointer;" onclick="changePlan(2)">Express</span>';
						//$plan_option 	.= '<option value="2">Express Clean</option>';
					}
				}
				elseif($val['stripe_plan'] == config('data.jumboclean')){
					$tokenArr['jumboclean']	+= $val['quantity']-$val['used_quantity'];					
					if($jumboclean){
						$myplan_option[] = 3;
						$jumboclean 	= 0;
						$myPlans['jumboTime'] 		= 1;
						$clr 		= '1abc9c';
						$chkstr 	= '';
						if($planchk){
							
							$planchk 	= 0;
							$chkstr 	= 'checked';
							$clr 		= 'bc2653';
							$plan_option	.= '<input type="hidden" name="selectedPlan" id="selectedPlan" value="3" style="display:none1;" />';
						}


						
						$plan_option 	.= '<span id="sp3" style="color:#fff; background-color:#'.$clr.'; padding: 5px; margin:0 5px; cursor:pointer;" onclick="changePlan(3)" >Jumbo</span>';
						//$plan_option 	.= '<option value="3">Jumbo Express Clean</option>';
					}
				}
			}
			
			if($myPlans['oneTime'] == 0){
				$plan_option 	.= '<span style="color:#fff; background-color:#CCC; padding: 5px; margin:0 5px; cursor:pointer;" onclick="notAvailablePlan(\'One Time\')" >One Time</span>';
			}
			if($myPlans['expressTime'] == 0){
				$plan_option 	.= '<span style="color:#fff; background-color:#CCC; padding: 5px; margin:0 5px; cursor:pointer;" onclick="notAvailablePlan(\'Express\')" >Express</span>';
			}
			if($myPlans['jumboTime']  == 0){
				$plan_option 	.= '<span style="color:#fff; background-color:#CCC; padding: 5px; margin:0 5px; cursor:pointer;" onclick="notAvailablePlan(\'Jumbo\')" >Jumbo</span>';	
			}
			
			//dd($myplan_option);
			//$plan_option .= '</select>';
			
			

			//dd($tokenArr);
			
			$inboxMsg = UsersMessage::select("sender_id","receiver_id","message","updated_at")->where('sender_id',Auth::user()->id)->orWhere('receiver_id',Auth::user()->id)->orderBy('id','DESC')->get()->toArray();
			//dd($inboxMsg);

			$msgcnt 	= 0;
			$msgarr = array();
			$res = UsersMessage::select('sender_id')->distinct()->where('receiver_id',Auth::user()->id)->where('receiver_read',1)->get()->toArray();
			foreach($res as $val){
				$msgarr[$val['sender_id']] = $val;
			}
			$msgcnt	= count($msgarr);
			
/*
1:Pending, 2:Completed, 3:Cancel by User, 4:Accepted by Cleaner, 5:Canceled by Cleaner, 6:Cancel by Admin, 7:Reschedule by User, 8: Mark as completed by Cleaner, 9 : Mark as disputed by customer, 10: Mark as completed by admin, 11: Mark as dispute by admin*/			

$jobStatus[0]['status']	= array(1);
$jobStatus[0]['title'] 	= 'Jobs Pending Acceptance';

$jobStatus[1]['status']	= array(4);
$jobStatus[1]['title'] 	= 'Scheduled Jobs';

$jobStatus[2]['status']	= array(8);
$jobStatus[2]['title'] 	= 'Job Completed';

$jobStatus[3]['status']	= array(9);
$jobStatus[3]['title'] 	= 'Disputed Job';

$jobStatus[4]['status']	= array(2);
$jobStatus[4]['title'] 	= 'Completed job';



			//dd($userAddress);
			
			if(Auth::user()->type == 1){
				return view('home',compact('userlist','locationlist','bookinglist','addresslist','userAddress','plan','orderlist','plan_option','myplan_option','tokenArr','inboxMsg','cleanerRequestIds','msgcnt','jobStatus','myPlans'));
			}
			else{
				return view('homecleaner',compact('userlist','locationlist','bookinglist','bookingarr','addresslist','userAddress','plan','orderlist','cleanerCalendar','viewedcnt','inboxMsg','msgcnt'));
			}			
		}
        
    }
	
	public function cancelschedule(Request $request){
		CustomerCalendar::where('id',$request->id)->where('customer_id',Auth::user()->id)->delete();
		$res = array('msg'=>'Clean schedule deleted successfully.');
		echo json_encode($res);
	}
	
	public function bookcleaner(Request $request){
		$selectedTime		= explode('-',$request->selectedTime);
		$selectedLocation	= explode('_',$request->selectedLocation);
		$selectedPlan		= $request->selectedPlan;
		$cleanerIdStr		= $request->cleanerId;
		$cleanerCharge		= 0;
		
		if($selectedPlan == 1){
			$cleanerCharge	= config('data.OnetimeCleanerCharge');
		}
		elseif($selectedPlan == 2){
			$cleanerCharge	= config('data.expressCleanerCharge');
		}
		elseif($selectedPlan == 3){
			$cleanerCharge	= config('data.jumboCleanerCharge');
		}
		
		//echo '<pre>';print_r($selectedTime);echo '</pre>';echo '<pre>';print_r($selectedLocation);//exit;
		
		$data['customer_id']			= Auth::user()->id;
		$data['cleaning_type']			= $selectedPlan;
		$data['cleaner_amt']			= $cleanerCharge;
		$data['address_id']				= $selectedLocation[0];	
		$data['location_id']			= $selectedLocation[1];
		$data['cleaner_id']				= 0;//$request->cleanerId;
		$data['dtd']					= date('Y-m-d',strtotime(str_replace('/','-',$request->selectedDate)));
		
		
		$data['from_time']				= $selectedTime[0];
		$data['to_time']				= $selectedTime[1];
		if($data['cleaning_type'] == 3){
			$data['to_time']	= date('H:i:s',strtotime($data['from_time'])+2*60*60);;
		}
		
		
		$data['contact_person']			= $request->contact_person;
		$data['contact_phonenumber'] 	= $request->contact_phonenumber;
		$data['bathroomdetails']		= $request->bathroomdetails;
		$data['bedroomdetails'] 		= $request->bedroomdetails;
		$data['property_access_type']	= $request->propertyAccessType;
		$data['accessdetails']			= $request->accessdetails;

		$orderRes 	= CustomerCalendar::create($data);
		
		$timestr	= '';
		$dtd		= $data['dtd'];
		$from_time 	= (float)str_replace(':00:00','.0',str_replace(':30:00','.5',$data['from_time']));
		$to_time 	= (float)str_replace(':00:00','.0',str_replace(':30:00','.5',$data['to_time']));
		//echo '****'.$dtd.'----'.$from_time.'---'.$to_time.'----';
		$uparr['status'] = 0;
		$uparr['updated_at'] = date('Y-m-d H:i:s');
		while($from_time<$to_time){
			$timestr .= '-->--'.$from_time;
			//CleanerCalendarSlot::where('cleaner_id',$data['cleaner_id'])->where('dtd',$dtd)->where('slot',$from_time)->update($uparr);
			$from_time += 1;
		}
		
		
		//$uparr['used_quantity']
		//Subscription::where()->update(array('used_quantity',''));
		$whr = '';
		if($selectedPlan == 1){
			$selectedPlan = config('data.onetimeclean');
		}
		elseif($selectedPlan == 2){
			$selectedPlan = config('data.expressclean');
		}
		elseif($selectedPlan == 3){
			$selectedPlan = config('data.jumboclean');
		}
		$sql = "UPDATE subscriptions SET used_quantity = used_quantity + 1 WHERE stripe_plan = '".$selectedPlan."' and user_id = ".$data['customer_id'];;
		//DB::update($sql);
		
		
		$cleanerIdArr = explode('_',$cleanerIdStr);
		$insArr['order_id']		= $orderRes->id;
		foreach($cleanerIdArr as $val){
			if($val != ''){
				$insArr['cleaner_id']	= $val;
				CustomerCalendarRequest::create($insArr);
			}
		}
		
		$res['timestr']	= $timestr;
		$res['status'] 	= 1;
		//$res['msg']		= '<a class="waves-effect waves-teal">Successfully your booking request is registered in our system. We will contact with you asap.....</a>';
		$res['msg']		= 'Successfully your booking request is registered in our system. We will contact with you asap.';
		return json_encode($res);
	}
	
	public function searchcleaner(Request $request){		
		$selectedTime			= explode('-',$request->selectedTime);
		$selectedLocation		= explode('_',$request->selectedLocation);;
		$location_id			= $selectedLocation[1];
		$dtd					= date('Y-m-d',strtotime(str_replace('/','-',$request->selectedDate)));
		$from_time				= $selectedTime[0];
		$to_time				= $selectedTime[1];
		//$id = CleanerCalendarSlot::where(array('dtd'=>$dtd))->get->toArray();
				
		$cleaner_list = User::select('id','name','profile_image','rating','task_complete')->where('type',3)->get()->toArray();
		return json_encode($cleaner_list);/* */
	}
	
	public function searchcleaner_old(Request $request){
		$selectedTime			= explode('-',$request->selectedTime);
		$selectedLocation		= explode('_',$request->selectedLocation);;
		$location_id			= $selectedLocation[1];
		$dtd					= date('Y-m-d',strtotime(str_replace('/','-',$request->selectedDate)));
		$from_time				= $selectedTime[0];
		$to_time				= $selectedTime[1];
						
		$ftime 		= (float)str_replace(':00:00','.0',str_replace(':30:00','.5',$from_time));
		$ftimearr 	= array($ftime);
		while($ftime<$to_time){
			$ftime += 1;
			//$ftimearr[] = $ftime;
		}

		$res = CleanerCalendarSlot::select('cleaner_id',DB::raw('count("cleaner_id") as total'))
					->where('dtd',$dtd)
					->whereIn('slot',$ftimearr)
					->where('status',1)
					->groupBy('cleaner_id')
					->get()
					->toArray();
		//print_r($res);exit;
		$user_list = array();
		foreach($res as $val){
			//echo '<br>'.$val['total'].'>='.count($ftimearr);
			if($val['total']>=count($ftimearr)){
				$user_list[] = $val['cleaner_id'];
			}
		}
		
		//print_r($user_list);exit;
		
		if(count($user_list) == 0){
			return json_encode($user_list);
		}
		
		
		
		
		$ulist	= array();
		$res 	= CustomerCalendar::where('location_id',$location_id)->where('dtd',$dtd)->where('from_time',$from_time)->get();
		foreach($res as $val){
			$ulist[] = $val->cleaner_id;
		}
		

		$i   = 0;
		$arr = array();
		$res = User::select('id','name','profile_image','rating','task_complete');
		$res = $res->where('type',3);
		//Location condition needs to add asap.
		//$res = $res->where('location_id',$location_id);
		$res = $res->whereIn('id',$user_list);
		if(count($ulist)>0){
			$res = $res->whereNotIn('id',$ulist);
		}
		$res = $res->get();
		
		$userlist = array();
		foreach($res as $val){
			$userlist[] = $val->id;
			$val->name = $val->name;//.'--'.$val->id;
			$arr[] = $val;
		}
		return json_encode($arr);
	}
	
	
	
	public function addreview(Request $request){
		/*$res['status']	= 1;
		return json_encode($res);	*/	
		
		
		$arr['rating'] 		= $request->ratingValue;
		$arr['rating_msg']	= $request->rating_msg;
		$schedule_id		= $request->schedule_id;
		$cleaner_id			= $request->cleaner_id;
		CustomerCalendar::where('id',$schedule_id)->update($arr);
		
		$res = CustomerCalendar::select(DB::raw('COUNT(id) task_complete'))
					->where('cleaner_id',$cleaner_id)
					->where('status',2)
					->get()->toArray();
		$task_complete 	= $res[0]['task_complete'];			
		$res = CustomerCalendar::select(DB::raw('COUNT(rating) total_review'),DB::raw('AVG(rating) rating'))
					->where('cleaner_id',$cleaner_id)
					->where('rating','>',0)
					->where('status',2)
					->get()->toArray();
		$res = $res[0];
		$rating 		= $res['rating'];
		$total_review 	= $res['total_review'];
		
		User::where('id',$cleaner_id)->update(array('rating'=>$rating,'total_review'=>$total_review,'task_complete'=>$task_complete));
		$res['status']	= 1;
		echo json_encode($res);
	}
	
	public function profile_update(Request $request){
		$locations = '';
		if(isset($request->location_id)){
			$locations = implode(',',$request->location_id);
		}
				
		$arr['name'] 			= $request->profile_name;
		$arr['email'] 			= $request->email;
		$arr['phone_number'] 	= $request->phone_number;		
		$arr['location_id'] 	= $locations;		
		$arr['address'] 		= $request->address;
		$file 					= $request->file('image');
		if(isset($file))
		{
			$img 					= rand(99,999).time();
			$destinationPath 		= 'profile';
			$file->move($destinationPath,$img.'.'.$file->getClientOriginalExtension());	
			if(in_array(strtolower($file->getClientOriginalExtension()),array('jpg','jpeg','png','bmp'))){
				$arr['profile_image']	= $img.'.'.$file->getClientOriginalExtension();
			}
		}

		if(Auth::user()->type == 3)
		{
			$arr['location_id'] = 0;
			if(isset($request->location_id)){
				$arr['location_id'] 				= $request->location_id;
			}
			
			$arr['dob']							= date('Y-m-d',strtotime(str_replace('/','-',$request->dob)));
			$arr['gender']						= $request->gender;			
			$arr['email_notification'] 			= $request->email_notification;
			$arr['text_notification'] 			= $request->text_notification;
			$arr['phone_notification'] 			= $request->phone_notification;
			$arr['preference_notification'] 	= $request->preference_notification;
			$arr['bank_details'] 				= $request->bank_details;
			$arr['paypal_details'] 				= $request->paypal_details;
			$arr['payooneer_prepaid_details'] 	= $request->payooneer_prepaid_details;
			$arr['venmo_details'] 				= $request->venmo_details;
			$arr['speaks']						= json_encode($request->speaks);
			
			
			if(is_array($request->speaks)){
				//echo 888;
			}
			else{
				//echo 999;
			}
			//dd($arr);
			
			
			$govtid 			= $request->file('govtid');
			if(isset($govtid))
			{
				$img 					= rand(99,999).time();
				$destinationPath 		= 'govtid';
				$govtid->move($destinationPath,$img.'.'.$govtid->getClientOriginalExtension());	
				if(in_array(strtolower($govtid->getClientOriginalExtension()),array('jpg','jpeg','png','bmp'))){
					$arr['govtid']		= $img.'.'.$govtid->getClientOriginalExtension();
				}
			}	
		}
		
		
		User::where('id',Auth::user()->id)->update($arr);		
		//dd($arr);
		return redirect('home');
	}
	
	public function cleanercalendardetails(Request $request){
		$calendar = array();
		$dtd = $request->dtd;
		$res = CleanerCalendar::where('cleaner_id',Auth::user()->id)->where('dtd',$dtd)->get()->toArray();
		foreach($res as $val){
			$val['dtd'] = date('d/m/Y',strtotime($val['dtd']));
			$cnt = CleanerCalendarSlot::where('cleaner_calendar_id',$val['id'])->where('status',0)->get()->count();
			$val['cnt']	= $cnt;
			$calendar[] = $val;
		}
		return json_encode($calendar);
	}
	
	public function taskdecline(Request $request){
		$arr['status'] = 0;
		CustomerCalendarRequest::where('order_id',$request->id)->where('cleaner_id',Auth::user()->id)->update($arr);
		//$arr['status'] = 5;
		//CustomerCalendar::where('id',$request->id)->where('cleaner_id',Auth::user()->id)->update($arr);
		$res['status'] 	= 5;
		$res['msg'] 	= 'Cancel by Cleaner';
		return json_encode($res);
	}
	
	public function taskaccepted(Request $request){
		//25-01-2020 :: 
		//$arr['status'] 	= 4;
		//CustomerCalendar::where('id',$request->id)->where('cleaner_id',Auth::user()->id)->update($arr);


/*** 28012020 ***/		
		$res 	= CustomerCalendar::where('id',$request->id)->get()->toArray();
		//print_r($res[0]['from_time']);exit;
		
		//echo Auth::user()->id;exit;
		$from_time 			= date('H',strtotime($res[0]['from_time']));
		$to_time 			= date('H',strtotime($res[0]['to_time']));
		$dtd				= $res[0]['dtd'];
		$selectedPlan 		= $res[0]['cleaning_type'];
		$customer_id		= $res[0]['customer_id'];
		$uparr['status']	= 0;
		DB::connection()->enableQueryLog();

		while($from_time<$to_time){
			$from_time = number_format($from_time,1);
			//echo $from_time.'++++'.$dtd.'****'.Auth::user()->id.'<br>';
			$res = CleanerCalendarSlot::where('cleaner_id',Auth::user()->id)->where('dtd',$dtd)->where('slot',$from_time)->where('status',1)->update($uparr);
			$insData = array();
			
			$insData['cleaner_id'] 	= Auth::user()->id;
			$insData['dtd']			= $dtd;
			$insData['slot']		= $from_time;
			$insData['status']		= 0;
			CleanerCalendarSlot::create($insData);
			//echo '<pre>';print_r($res);echo '</pre><hr>';
			$from_time += 1;
		}
		
$queries = DB::getQueryLog();
//dd($queries);		
/*		
		echo '------'.$dtd.'<br>';
		echo '------'.$from_time.'++++++'.$to_time;
		echo '+++++++';
		exit;
/*		
		$dtd		= $data['dtd'];
		$from_time 	= (float)str_replace(':00:00','.0',str_replace(':30:00','.5',$data['from_time']));
		$to_time 	= (float)str_replace(':00:00','.0',str_replace(':30:00','.5',$data['to_time']));
		//echo '****'.$dtd.'----'.$from_time.'---'.$to_time.'----';
		$uparr['status'] = 0;
		$uparr['updated_at'] = date('Y-m-d H:i:s');
		while($from_time<$to_time){
			$timestr .= '-->--'.$from_time;
			//
			$from_time += 1;
		}		
		
		
exit;
*/

		
		$arr['status'] 		= 4;
		$arr['cleaner_id'] 	= Auth::user()->id;
		CustomerCalendar::where('id',$request->id)->where('cleaner_id',0)->update($arr);
		
		$arr1['status']		= 0;	
		CustomerCalendarRequest::where('order_id',$request->id)->where('cleaner_id','!=',Auth::user()->id)->update($arr1);
		
		$arr1['status']		= 2;	
		CustomerCalendarRequest::where('order_id',$request->id)->where('cleaner_id',Auth::user()->id)->update($arr1);


		//Subscription::where()->update(array('used_quantity',''));
		
		if($selectedPlan == 1){
			$selectedPlan = config('data.onetimeclean');
		}
		elseif($selectedPlan == 2){
			$selectedPlan = config('data.expressclean');
		}
		elseif($selectedPlan == 3){
			$selectedPlan = config('data.jumboclean');
		}
		$sql = "UPDATE subscriptions SET used_quantity = used_quantity + 1 WHERE stripe_plan = '".$selectedPlan."' and user_id = ".$customer_id;
		DB::update($sql);


		
		$res = array();
		$res['status'] 	= 4;
		$res['msg'] 	= 'Accepted by Cleaner';
		return json_encode($res);
	}
	
	public function markascomplete(Request $request){
		
		if($request->jid != 0 && Auth::user()->type == 3)
		{
			$arr	= array();
			$imgarr	= array();
			$res 	= CustomerCalendar::select('status','images')->where('id',$request->jid)->where('cleaner_id',Auth::user()->id)->get()->toArray();
			$arr['status'] 	= 8;
			if(isset($res[0]['status']) && $res[0]['status'] == 9){
				$arr['status'] 	= 9;
			}
			
			if(isset($res[0]['images'])){
				$imgarr = json_decode($res[0]['images']);
				//dd($imgarr);
			}
			
			
			$files 	= $request->file('room_img');
			if(isset($files))
			{
				foreach($files as $file)
				{
					$img 					= rand(99,999).time();
					$destinationPath 		= 'room_img';
					$file->move($destinationPath,$img.'.'.$file->getClientOriginalExtension());	
					if(in_array(strtolower($file->getClientOriginalExtension()),array('jpg','jpeg','png','bmp'))){
						$imgarr[]	= $img.'.'.$file->getClientOriginalExtension();
					}
				}
			}
			$arr['images'] = json_encode($imgarr);
			
			CustomerCalendar::where('id',$request->jid)->where('cleaner_id',Auth::user()->id)->update($arr);
			//dd('upload completed.');
			return redirect('home');
		}
		elseif($request->id != 0 && Auth::user()->type == 1)
		{
			$arr['status'] 	= 2;
			$arr['disputeMsg'] 	= $request->disputeMsg;
			$res['msg'] 	= 'Job id '.$request->id.' mark as completed.';
			if(isset($request->type) && $request->type == 'dispute'){
				$arr['status'] 	= 9;
				$res['msg'] 	= 'Job id '.$request->id.' mark as dispute.';
			}
			CustomerCalendar::where('id',$request->id)->where('customer_id',Auth::user()->id)->update($arr);
			$msg = $res['msg'];
			
			
			$res 	= CustomerCalendar::select('status','cleaner_id')->where('id',$request->id)->get()->toArray();
			$res	= $res[0];
			$data['sender_id']		= Auth::user()->id;
			//$data['receiver_id']	= $request->msgReceiverId;
			$data['receiver_id']	= $res['cleaner_id'];
			$data['message']		= $msg;			
			UsersMessage::create($data);			
			
			$res['status'] 	= 2;
			return json_encode($res);			
		}
	}
	
	public function cleanerCalendarAvailableTime(Request $request){
		$slot 				= array();
		$bookslot 			= array();
		$arr['dtd'] 		= date('Y-m-d',strtotime(str_replace('/','-',$request->dtd)));
		$arr['cleaner_id']	= $request->cleanerId;
		$t = (int)date("H")+1;
		//echo $t+7;
		
		$res = CleanerCalendarSlot::select('slot','status')->where($arr)->whereIn('status',array(0,2));
		if($arr['dtd'] == date('Y-m-d')){
			$res = $res->where('slot','>',$t);
		}
		$res = $res->get()->toArray();
		//$res = CleanerCalendarSlot::select('slot','status')->where($arr)->where('status',1)->get()->toArray();
		foreach($res as $val){
			$slot[] = $val['slot'];

		}
		//echo '<pre>';print_r($slot);exit;
		$str = '';
		$i = config('data.start_time');
		while($i<config('data.end_time')){
			$i++;
			$available = 1;
			if(count($slot)>0 && in_array($i,$slot)){
				$available = 0;
			}

			if($available == 1){
				if($i == 12){
					$str .= '<li onclick="hiremetime('.$i.')" class="cleanertime" style="background-color:#1abc9c;font-size:12px; color:#FFF;" >';
					$str .= ' 12 O\'Clock</li>';
				}
				else{
					$str .= '<li onclick="hiremetime('.$i.')" class="cleanertime" style="background-color:#1abc9c;font-size:12px; color:#FFF;" >';
					$str .= ' '.($i<12?$i.'AM':($i-12).'PM').'</li>';							
				}
			}
		}
		if($str == ''){
			$str .= '<li class="cleanertime" style="font-size:12px; color:#FFF; width:100%; text-align:center;" >Cleaner not available for work.</li>';									
		}
		return $str;
	}
	
	public function cleanerCalendarTime(Request $request){
		$slot 				= array();
		$bookslot 			= array();
		$unavailableslot	= array();
		$arr['dtd'] 		= $request->dtd;
		$arr['cleaner_id']	= Auth::user()->id;
		//$res = CleanerCalendarSlot::select('slot','status')->where($arr)->whereIn('status',array(0,1,3))->get()->toArray();
		$res = CleanerCalendarSlot::select('slot','status','dtd')->where($arr)->get()->toArray();
		foreach($res as $val){
			if(in_array($val['status'],array(0,1)))
			{
				$myavailable[] = $val['slot'];
				$slot[] = $val['slot'];
				if($val['status'] == 0){
					$bookslot[] = $val['slot'];
				}							
			}
			else{
				$unavailableslot[] = $val['slot'];
			}
		}
		//echo '++++<pre>';print_r($myavailable);echo '<pre>';print_r($unavailableslot);echo '<pre>';//print_r($unavailableslot);
		
		$chkall	= ' checked ';
		$str 	= '';
		$i = config('data.start_time');
		while($i<config('data.end_time')){
			$i++;
			$chk = ' checked ';
			
			if(in_array($i,$slot)){
				$chk = ' checked ';
			}
			else{
				$chkall = '  ';
			}
			
			if(in_array($i,$bookslot)){
				$class = 'cleanertime_bookslot';
				$str .= '<li onclick="setmycalendar('.$i.')" class="'.$class.'" title="Already schedule for cleaning." >';
				//$str .= '<input type="checkbox" name="myTime[]" value="'.$i.'" '.$chk.' style="margin-right:5px;" />';
				
				if($i == 12){
					$str .= '12 O\'Clock</li>';
				}else{
					$str .= $i<12?$i.' AM':($i-12).'PM'.'</li>';
				}
			}
			else{
				$bgc = '';
				if(in_array($i,$unavailableslot)){
					$chk = ' ';
					$bgc = 'background-color:#d9534f;';
				}
				/*if($chk != ''){
					//$bgc = 'background-color:#5bc0de;';
				}*/
				
				
				$class = 'cleanertime';			
				$str .= '<li onclick="setmycalendar('.$i.')" class="'.$class.'"   style="'.$bgc.'"  >';
				$str .= '<input type="checkbox" name="myTime[]" value="'.$i.'" '.$chk.' class="cleandarchkbox" style="margin-right:5px;" />';
				
				if($i<12)
					$str .= $i;
				elseif($i == 12)
					$str .= 12;
				else
					$str .= ($i-12);
				
				$str .= ($i<12?'AM':'PM').'</li>';
			}
		}
		
		$str1 	= '<li class="cleanertime"  onclick="selectmyall()" ><input type="checkbox" '.$chkall.' id="selectall" name="selectall" value="selectall" />All</li>';
		$str 	= $str1.$str;
		return $str;
	}
	
	public function addCalendarTime(Request $request){
		$arr['cleaner_calendar_id']	= 9994;		
		$arr['cleaner_id']	= Auth::user()->id;
		$arr['status']		= $request->availability_status;
		$arr['dtd']			= date('Y-m-d',strtotime(str_replace('/','-',$request->scheduleDate)));
		$myTime				= $request->myTime;
		
		//echo '<pre>';print_r($myTime);exit;
		if(isset($myTime) && count($myTime)>0)
		{
		foreach($myTime as $val){
			$arr['slot'] 	= $val;
			$cnt = CleanerCalendarSlot::where('cleaner_id',$arr['cleaner_id'])->where('dtd',$arr['dtd'])->where('slot',$arr['slot'])->get()->count();
			if($cnt == 0){
				CleanerCalendarSlot::create($arr);
			}
			else{
				CleanerCalendarSlot::where('cleaner_id',$arr['cleaner_id'])->where('dtd',$arr['dtd'])->where('slot',$arr['slot'])->update(['status'=>$arr['status']]);
			}
			
			/*$arr['slot'] 	= $val +  0.5;;
			$cnt = CleanerCalendarSlot::where('cleaner_id',$arr['cleaner_id'])->where('dtd',$arr['dtd'])->where('slot',$arr['slot'])->get()->count();
			if($cnt == 0){
				CleanerCalendarSlot::create($arr);				
			}
			else{
				CleanerCalendarSlot::where('cleaner_id',$arr['cleaner_id'])->where('dtd',$arr['dtd'])->where('slot',$arr['slot'])->update(['status'=>$arr['status']]);
			}*/			
		}
		}
		return 1;
		exit;
		
		$arr['start_time']	= $start_time 	= $request->scheduleStartTime;
		$arr['end_time']	= $end_time		= $request->scheduleEndTime;
		$res = CleanerCalendar::create($arr);
		//echo '<pre>';print_r($res['id']);
		unset($arr['start_time']);
		unset($arr['end_time']);
		$arr['cleaner_calendar_id']	= $res['id'];

		$res['status'] = 1;
		echo json_encode($res);
	}
	
	public function addCalendarTime_old(Request $request){
		echo '<pre>';print_r($_POST);echo '</pre>';exit;
		$arr['cleaner_id']	= Auth::user()->id;
		$arr['dtd']			= date('Y-m-d',strtotime(str_replace('/','-',$request->scheduleDate)));
		$arr['start_time']	= $start_time 	= $request->scheduleStartTime;
		$arr['end_time']	= $end_time		= $request->scheduleEndTime;
		$res = CleanerCalendar::create($arr);
		//echo '<pre>';print_r($res['id']);
		unset($arr['start_time']);
		unset($arr['end_time']);
		$arr['cleaner_calendar_id']	= $res['id'];
		while($start_time<$end_time){
			$arr['slot'] 	= $start_time;
			$start_time		+= 0.5;
			CleanerCalendarSlot::create($arr);
		}
		$res['status'] = 1;
		echo json_encode($res);
	}
	
	public function deletecalendar(Request $request){
		$res = CleanerCalendarSlot::where('cleaner_calendar_id',$request->id)->delete();
		$res = CleanerCalendar::where('id',$request->id)->delete();
		return json_encode($res);
	}
	
	public function cleanerprofile(Request $request){
		$id = $request->id;
		$whr['cleaner_id']	= $id;
		$whr['customer_id']	= Auth::user()->id;
		$cnt = CustVisitCleanerProfile::where($whr)->count();
		if(!$cnt){
			CustVisitCleanerProfile::insert($whr);
		}
		
		$user = User::select('id', 'name', 'email', 'details', 'phone_number','phone_number_verified','profile_image','govtid','govtid_verified','selfie','gender','dob','rating','total_review','task_complete','total_earning','speaks','created_at')->find($id)->toArray();
		$user['created_at'] = date('F, Y',strtotime($user['created_at']));
		
		
		if($user['details'] != ''){
			$user['details']	= '<h3>About</h3><p>'.$user['details'].'</p>';
		}
		
		$user['email_verified'] = 1;
		if($user['email_verified']){
			$user['email_verified'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check green" aria-hidden="true"></i> Email ID';
		}else{
			$user['email_verified'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times red" aria-hidden="true"></i> Email ID';
		}
		if($user['govtid_verified']){
			$user['govtid_verified'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check" aria-hidden="true"></i> Govt. ID';
		}else{
			$user['govtid_verified'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times" aria-hidden="true"></i> Govt. ID';
		}
		if($user['phone_number_verified']){
			$user['phone_number_verified'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check" aria-hidden="true"></i> Phone';
		}else{
			$user['phone_number_verified'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times" aria-hidden="true"></i> Phone';
		}
		if($user['selfie']){
			$user['selfie'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check" aria-hidden="true"></i> Selfie';
		}else{
			$user['selfie'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times" aria-hidden="true"></i> Selfie';
		}
		
		if(in_array($user['speaks'],array('english','spanish'))){
			$user['cleaner_speaks'] = '&nbsp; &nbsp; '.ucfirst($user['speaks']);
		}
		else{
			$user['cleaner_speaks'] = '&nbsp; &nbsp; English, Spanish';
		}
		
		$user['cleaner_speaks'] = '';
		$lanarr = json_decode($user['speaks']);
		if(is_array($lanarr))
		foreach($lanarr as $val){
			$user['cleaner_speaks'] .= ucfirst($val).',';
		}
		$user['cleaner_speaks'] = substr($user['cleaner_speaks'],0,-1);
		
		
		
	
		$i = 0;
		$user_rating = '';
		while($i<$user['rating']){
			$i++;
			$user_rating .= '<i class="fa fa-star" style="font-size: small; color:#e2bd09;" ></i>';
		}
		if($user_rating == ''){
			$user_rating = 'N/A';
		}
		$user['rating'] = $user_rating;
		
		
		$userlist = array();
		$res = User::get()->toArray();
		foreach($res as $val){
			$userlist[$val['id']]	= $val;
		}
		
		$reviewlist = array();
		$res = CustomerCalendar::select('*')->where('rating','>',0)->where('cleaner_id',$id)->get()->toArray();
		foreach($res as $val){
			$val['customer_name']	= $userlist[$val['customer_id']]['name'];
			$val['created_at']		= date('F, Y',strtotime($userlist[$val['customer_id']]['created_at']));
			$val['profile_image']	= $userlist[$val['customer_id']]['profile_image'];
			$va['rating_dtd']		= date('F, Y',strtotime($val['rating_dtd']));
			$reviewlist[] 			= $val;
		}
		$user['reviewlist'] = $reviewlist;
		echo json_encode($user);
	}
	
	public function sendmsg(Request $request){		
		$data['sender_id']		= Auth::user()->id;
		$data['receiver_id']	= $request->msgReceiverId;
		$data['message']		= $request->msg;
		//print_r($data);
		UsersMessage::create($data);
		
		/*
		$options = array('cluster' => 'ap2','useTLS' => true);
		$pusher = new Pusher\Pusher('d47bd2012723cc4d02e9','37fb7e0bd9ee630b00f2','955082',$options);
		//$data['message'] = ' Date : '.date('d-m-Y h:i a'); 
		$pusher->trigger('my-channel', 'my-event', $data);*/
		event(new \App\Events\FormSubmitted($data));
		$res['status']	= 1;
		return json_encode($res);
	}
	
	public function msghistory(Request $request){
		$id 	= $request->id;
		$uid 	= Auth::user()->id;
		
		UsersMessage::whereIn('sender_id',array($id,$uid))->whereIn('receiver_id',array($id,$uid))->update(array('receiver_read'=>0));
		$res 	= UsersMessage::select('sender_id','receiver_id','message')->whereIn('sender_id',array($id,$uid))->whereIn('receiver_id',array($id,$uid))->get()->toArray();
		return json_encode($res);
	}

	public function customer(Request $request){
		$id 	= $request->id;
		
		$userlist = array();
		$res = User::get()->toArray();
		foreach($res as $val){
			$userlist[$val['id']]	= $val;
		}		
		
		$res 	= User::where('id',$id)->where('type',1)->get()->toArray();
		if(isset($res[0])){
			$user = $res[0];
			$user['email_verified'] = 1;
			if($user['email_verified']){
				$user['email_verified'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check green" aria-hidden="true"></i> Email ID';
			}else{
				$user['email_verified'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times red" aria-hidden="true"></i> Email ID';
			}
 
		$reviewlist = array();
		$res = CustomerCalendar::select('*')->where('rating','>',0)->where('customer_id',$id)->get()->toArray();
		foreach($res as $val){
			$val['customer_name']	= $userlist[$val['cleaner_id']]['name'];
			$val['created_at']		= date('F, Y',strtotime($userlist[$val['cleaner_id']]['created_at']));
			$val['profile_image']	= $userlist[$val['cleaner_id']]['profile_image'];
			$va['rating_dtd']		= date('F, Y',strtotime($val['rating_dtd']));
			$reviewlist[] 			= $val;
		}
		$user['reviewlist'] = $reviewlist;			
			
			return json_encode($user);
		}
		else{
			return json_encode(array('status'=>false));
		}			
	}
	
	public function getWorkOrder(Request $request){
		$cleaner_id 		= $request->id;
		$whr['customer_id']	= Auth::user()->id;
		$whr['cleaner_id']	= 0;
		$res = CustomerCalendar::where($whr)->where('dtd','>=',date('Y-m-d'))->get()->toArray();
		//dd($res);
		$str = '';
		$i = 0;
		while($i<1){$i++;
		foreach($res as $val){
			$plan = '';
			if($val['cleaning_type'] == 1)
				$plan = 'One Time Cleaning';
			if($val['cleaning_type'] == 2)
				$plan = 'Express Cleaning';
			if($val['cleaning_type'] == 3)
				$plan = 'Jumbo Cleaning';
				
			$str .= '<li class="col-md-12" style="border-bottom:1px solid #FFF; margin-bottom:2%; padding-bottom:5px;">
						<div class="col-md-5">
							<p>Job Id : '.$val['id'].'</p>
							<p>Plan : '.$plan.'</p>
						</div>
						<div class="col-md-5">
							<p>Schedule Date : '.date('d/m/Y h:i',strtotime($val['dtd'].' '.$val['from_time'])).'</p>
							<p>Address : NewYork City'.$val['address_id'].'</p>
						</div>
						<div class="col-md-2"><input type="button" value="Select" class="btn btn-success btn-sm" onclick="selectcleaner('.$val['id'].')" /></div>
					</li>';
		}
		}
		
		if($str != ''){
			$str = '<ul>'.$str.'</ul>';
			$str .= '<div class="modal-footer" style="border:none !important; text-align:center">
				<h4 style=" margin:10px 0 5px; color:#17d8c6;">OR</h4>
				<button type="button" data-dismiss="modal" class="btn btn-info btn-sm" onclick="selectDate()">Create New Work Order</button>
			</div>';		
		}
		else{
			$str = '<div class="modal-footer" style="border:none !important; text-align:center">
				<button type="button" data-dismiss="modal" class="btn btn-info btn-sm" onclick="selectDate()">Create New Work Order</button>
			</div>';
		}
	
		
		
		return $str;
	}
	
	public function selectCleanerWorkOrder(Request $request){
		$data['cleaner_id']	= $request->cleanerId;
		$data['order_id']	= $request->workorder_id;
		$cnt = CustomerCalendarRequest::select('*')->where($data)->get()->count();
		if($cnt){
			return $res['status']	= 0;
			$res['msg']		= 'Already work order sent to cleaner.';
		}else{
			CustomerCalendarRequest::create($data);
			return $res['status']	= 1; 
			$res['msg']		= 'Work order sent to cleaner.';
			
		}
		return json_encode($res);
	}

	public function testmail(){
		$data = ['message' => 'This is a send grid test mail!'];
		\Mail::to('riyan04314@gmail.com')->send(new SendgridEmail($data));
		return 'Mail sent successfully!';
	}
}