<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Location;
use App\CustomerCalendar;
use App\CleanerCalendar;
use App\CustVisitCleanerProfile;
use App\CleanerCalendarSlot;

use App\Address;
use App\Plan;
use App\Subscription;
use DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	//public $timestamps = true;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		if(Auth::user()->type == 2){
			return redirect()->route('calendarmaster');
			return view('adminhome');
		}else{
			$locationlist 	= Location::all();
			$userlist 		= array();
			$res 			= User::all();
			foreach($res as $val){
				$userlist[$val->id] = $val;
			}
			$addresslist 	= array();
			$res 			= Address::all();
			foreach($res as $val){
				$addresslist[$val->id] = $val;
			}
			
			$userAddress 	= Address::where('user_id',Auth::user()->id)->get();
			//dd($userAddress);
			$plans = Plan::all();
			$plan = $plans[0];
			$whr = array();
			if(Auth::user()->type == 1){
				$whr['customer_id'] = Auth::user()->id;
			}
			elseif(Auth::user()->type == 3){
				$whr['cleaner_id'] = Auth::user()->id;
			}
			
			$bookinglist		= CustomerCalendar::where($whr)->get();
			$bookingarr			= array();
			foreach($bookinglist as $val){
				$bookingarr[$val['dtd']][] = $val;
			}
			$orderlist			= Subscription::where('user_id',Auth::user()->id)->orderBy('id', 'DESC')->get();
			$cleanerCalendar	= array();
			$res 				= CleanerCalendar::where('cleaner_id',Auth::user()->id)->where('dtd','>=',date('Y-m-d'))->orderBy('dtd')->orderBy('start_time')->get()->toArray();
			foreach($res as $val){
				$cleanerCalendar[$val['dtd']][]	 = $val;
			}
			
			$viewedcnt	= CustVisitCleanerProfile::where('cleaner_id',Auth::user()->id)->count();			
			//dd($bookingarr);
			
			
			if(Auth::user()->type == 1){
				return view('home',compact('userlist','locationlist','bookinglist','addresslist','userAddress','plan','orderlist'));
			}
			else{
				return view('homecleaner',compact('userlist','locationlist','bookinglist','bookingarr','addresslist','userAddress','plan','orderlist','cleanerCalendar','viewedcnt'));
			}			
		}
        
    }
	
	public function cancelschedule(Request $request){
		CustomerCalendar::where('id',$request->id)->where('customer_id',Auth::user()->id)->delete();
		$res = array('msg'=>'Clean schedule deleted successfully.');
		echo json_encode($res);
	}
	
	public function bookcleaner(Request $request){
		$selectedTime					= explode('-',$request->selectedTime);
		$selectedLocation				= explode('_',$request->selectedLocation);
		$data['customer_id']			= Auth::user()->id;
		$data['address_id']				= $selectedLocation[0];
		$data['location_id']			= $selectedLocation[1];
		$data['cleaner_id']				= $request->cleanerId;
		$data['dtd']					= date('Y-m-d',strtotime(str_replace('/','-',$request->selectedDate)));
		$data['from_time']				= $selectedTime[0];
		$data['to_time']				= $selectedTime[1];
		CustomerCalendar::insert($data);
		
		$timestr	= '';
		$dtd		= $data['dtd'];
		$from_time 	= (float)str_replace(':00:00','.0',str_replace(':30:00','.5',$data['from_time']));
		$to_time 	= (float)str_replace(':00:00','.0',str_replace(':30:00','.5',$data['to_time']));
		//echo '****'.$dtd.'----'.$from_time.'---'.$to_time.'----';
		$uparr['status'] = 0;
		$uparr['updated_at'] = date('Y-m-d H:i:s');
		while($from_time<=$to_time){
			$timestr .= '-->--'.$from_time;
			CleanerCalendarSlot::where('cleaner_id',$data['cleaner_id'])->where('dtd',$dtd)->where('slot',$from_time)->update($uparr);
			$from_time += 0.5;
		}
		
		$res['timestr']	= $timestr;
		$res['status'] 	= 1;
		$res['msg']		= '<a class="waves-effect waves-teal">Successfully your booking request is registered in our system. We will contact with you asap.</a>';
		echo json_encode($res);
	}
	
	public function searchcleaner(Request $request){
		$selectedTime			= explode('-',$request->selectedTime);
		$selectedLocation		= explode('_',$request->selectedLocation);
		$location_id			= $selectedLocation[1];
		$dtd					= date('Y-m-d',strtotime(str_replace('/','-',$request->selectedDate)));
		$from_time				= $selectedTime[0];
		$to_time				= $selectedTime[1];
		
		
		//echo $dtd.'+++'.$from_time.'<hr>';
		$res = CleanerCalendarSlot::select('cleaner_id','dtd','slot','status')->where('dtd',$dtd)->where('slot',$from_time)->where('status',1)->get()->toArray();
		//echo json_encode($res);
		$user_list = array();
		foreach($res as $val){
			$user_list[] = $val['cleaner_id'];
		}
		//print_r($user_list);
		
		
		
		
		
		$ulist	= array();
		$res 	= CustomerCalendar::where('location_id',$location_id)->where('dtd',$dtd)->where('from_time',$from_time)->get();
		foreach($res as $val){
			$ulist[] = $val->cleaner_id;
		}
		
		$i   = 0;
		$arr = array();
		$res = User::select('id','name','profile_image','rating','task_complete')->where('type',3)->where('location_id',$location_id)->whereNotIn('id',$ulist)->whereIn('id',$user_list)->get();
		$userlist = array();
		foreach($res as $val){
			$userlist[] = $val->id;
			$val->name = $val->name.'--'.$val->id;
			$arr[] = $val;
		}
		//echo json_encode($userlist);
		
		echo json_encode($arr);
	}
	
	public function searchcleanerbk(Request $request){
		$selectedTime			= explode('-',$request->selectedTime);
		$location_id			= $request->selectedLocation;
		$dtd					= date('Y-m-d',strtotime(str_replace('/','-',$request->selectedDate)));
		$from_time				= $selectedTime[0];
		$to_time				= $selectedTime[1];

		$i   = 0;
		$arr = array();
		$res = User::select('id','name')->where('type',3)->where('location_id',$location_id)->get();
		//$res = User::select('id','name')->where('type',3)->get();
		foreach($res as $val){
			$arr[] = $val;
		}
		echo json_encode($arr);
	}
	
	public function addreview(Request $request){
		$arr['rating'] 		= $request->ratingValue;
		$arr['rating_msg']	= $request->rating_msg;
		$schedule_id		= $request->schedule_id;
		$cleaner_id			= $request->cleaner_id;
		CustomerCalendar::where('id',$schedule_id)->update($arr);
		
		
		
		$res = CustomerCalendar::select(DB::raw('COUNT(id) task_complete'))
					->where('cleaner_id',$cleaner_id)
					->where('status',2)
					->get()->toArray();
		$task_complete 	= $res[0]['task_complete'];			
		$res = CustomerCalendar::select(DB::raw('COUNT(rating) total_review'),DB::raw('AVG(rating) rating'))
					->where('cleaner_id',$cleaner_id)
					->where('rating','>',0)
					->where('status',2)
					->get()->toArray();
		$res = $res[0];
		$rating 		= $res['rating'];
		$total_review 	= $res['total_review'];
		
		User::where('id',$cleaner_id)->update(array('rating'=>$rating,'total_review'=>$total_review,'task_complete'=>$task_complete));
		$res['status']	= 1;
		echo json_encode($res);
	}
	
	public function profile_update(Request $request){
		$arr['name'] 			= $request->profile_name;
		$arr['email'] 			= $request->email;
		$arr['phone_number'] 	= $request->phone_number;
		
		$file 					= $request->file('image');
		if(isset($file))
		{
			$img 					= rand(99,999).time();
			$destinationPath 		= 'profile';
			$file->move($destinationPath,$img.'.'.$file->getClientOriginalExtension());	
			if(in_array(strtolower($file->getClientOriginalExtension()),array('jpg','jpeg','png','bmp'))){
				$arr['profile_image']	= $img.'.'.$file->getClientOriginalExtension();
			}
		}

		if(Auth::user()->type == 3)
		{
			$arr['dob']		= date('Y-m-d',strtotime(str_replace('/','-',$request->dob)));
			$arr['gender']	= $request->gender;
			$arr['address'] 					= $request->address;
			$arr['email_notification'] 			= $request->email_notification;
			$arr['text_notification'] 			= $request->text_notification;
			$arr['phone_notification'] 			= $request->phone_notification;
			$arr['preference_notification'] 	= $request->preference_notification;
			$arr['bank_details'] 				= $request->bank_details;
			$arr['paypal_details'] 				= $request->paypal_details;
			$arr['payooneer_prepaid_details'] 	= $request->payooneer_prepaid_details;
			$arr['venmo_details'] 				= $request->venmo_details;
			
			
			$govtid 			= $request->file('govtid');
			if(isset($govtid))
			{
				$img 					= rand(99,999).time();
				$destinationPath 		= 'govtid';
				$govtid->move($destinationPath,$img.'.'.$govtid->getClientOriginalExtension());	
				if(in_array(strtolower($govtid->getClientOriginalExtension()),array('jpg','jpeg','png','bmp'))){
					$arr['govtid']		= $img.'.'.$govtid->getClientOriginalExtension();
				}
			}	
		}
		User::where('id',Auth::user()->id)->update($arr);
		//dd($arr);
		
		
		return redirect('home');
	}
	
	public function cleanercalendardetails(Request $request){
		$calendar = array();
		$dtd = $request->dtd;
		$res = CleanerCalendar::where('cleaner_id',Auth::user()->id)->where('dtd',$dtd)->get()->toArray();
		foreach($res as $val){
			$val['dtd'] = date('d/m/Y',strtotime($val['dtd']));
			$calendar[] = $val;
		}
		return json_encode($calendar);
	}
	
	public function taskdecline(Request $request){
		$arr['status'] = 5;
		CustomerCalendar::where('id',$request->id)->where('cleaner_id',Auth::user()->id)->update($arr);
		$res['status'] 	= 5;
		$res['msg'] 	= 'Cancel by Cleaner';
		return json_encode($res);
	}
	
	public function taskaccepted(Request $request){
		$arr['status'] 	= 4;
		CustomerCalendar::where('id',$request->id)->where('cleaner_id',Auth::user()->id)->update($arr);
		$res['status'] 	= 4;
		$res['msg'] 	= 'Accepted by Cleaner';
		return json_encode($res);
	}
	
	public function markascomplete(Request $request){
		$arr['status'] 	= 2;
		CustomerCalendar::where('id',$request->id)->where('customer_id',Auth::user()->id)->update($arr);
		$res['status'] 	= 2;
		$res['msg'] 	= 'Schedule task completed.';
		return json_encode($res);
	}
	

	
	public function addCalendarTime(Request $request){
		$arr['cleaner_id']	= Auth::user()->id;
		$arr['dtd']			= date('Y-m-d',strtotime(str_replace('/','-',$request->scheduleDate)));
		$arr['start_time']	= $start_time 	= $request->scheduleStartTime;
		$arr['end_time']	= $end_time		= $request->scheduleEndTime;
		$res = CleanerCalendar::create($arr);
		//echo '<pre>';print_r($res['id']);
		unset($arr['start_time']);
		unset($arr['end_time']);
		$arr['cleaner_calendar_id']	= $res['id'];
		while($start_time<$end_time){
			$arr['slot'] 	= $start_time;
			$start_time		+= 0.5;
			CleanerCalendarSlot::create($arr);
		}
		$res['status'] = 1;
		echo json_encode($res);
	}
	
	public function deletecalendar(Request $request){
		$res = CleanerCalendarSlot::('cleaner_calendar_id',$request->id)->delete();
		$res = CleanerCalendar::where('id',$request->id)->delete();
		return json_encode($res);
	}
	
	public function cleanerprofile(Request $request){
		$id = $request->id;
		$whr['cleaner_id']	= $id;
		$whr['customer_id']	= Auth::user()->id;
		$cnt = CustVisitCleanerProfile::where($whr)->count();
		if(!$cnt){
			CustVisitCleanerProfile::insert($whr);
		}
		
		$user = User::select('name', 'email', 'details', 'phone_number','phone_number_verified','profile_image','govtid','govtid_verified','gender','dob','rating','total_review','task_complete','total_earning','created_at')->find($id)->toArray();
		$user['created_at'] = date('F, Y',strtotime($user['created_at']));
		
		
		if($user['details'] != ''){
			$user['details']	= '<h3>About</h3><p>'.$user['details'].'</p>';
		}
		
		$user['email_verified'] = 1;
		if($user['email_verified']){
			$user['email_verified'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check green" aria-hidden="true"></i> Verified';
		}else{
			$user['email_verified'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times red" aria-hidden="true"></i> Not Verified';
		}
		if($user['govtid_verified']){
			$user['govtid_verified'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check" aria-hidden="true"></i> Verified';
		}else{
			$user['govtid_verified'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times" aria-hidden="true"></i> Not Verified';
		}
		if($user['phone_number_verified']){
			$user['phone_number_verified'] = '&nbsp; &nbsp; <i style="color:#0F0;" class="fa fa-check" aria-hidden="true"></i> Verified';
		}else{
			$user['phone_number_verified'] = '&nbsp; &nbsp; <i style="color:#F00;" class="fa fa-times" aria-hidden="true"></i> Not Verified';
		}
		
		
		$userlist = array();
		$res = User::get()->toArray();
		foreach($res as $val){
			$userlist[$val['id']]	= $val;
		}
		
		$reviewlist = array();
		$res = CustomerCalendar::select('*')->where('cleaner_id',$id)->get()->toArray();
		foreach($res as $val){
			$val['customer_name']	= $userlist[$val['customer_id']]['name'];
			$val['created_at']		= date('F, Y',strtotime($userlist[$val['customer_id']]['created_at']));
			$val['profile_image']	= $userlist[$val['customer_id']]['profile_image'];
			$va['rating_dtd']		= date('F, Y',strtotime($val['rating_dtd']));
			$reviewlist[] 			= $val;
		}
		$user['reviewlist'] = $reviewlist;
		echo json_encode($user);
	}
}