<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DB;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function getLocation(Request $request){
		
		if(!isset($request->searchTerm)){ 
			$fetchData = DB::select("select id, city_name as text, state_abbreviation, state_name from locations_new where state_name != '' order by state_name limit 5");
		}else{ 
			$search = $request->searchTerm;   
			$fetchData = DB::select("select  id, city_name as text, state_abbreviation, state_name  from locations_new  where state_name != '' and city_name like '%".$search."%' limit 50");
		} 
		
		$arr = array();
		foreach($fetchData as $val){
			$x['id'] 		= $val->id;
			$x['text'] 	= $val->text.', '.$val->state_abbreviation;
			$arr[] = $x;
		}
		echo json_encode($arr);
		
	}
	
    public function dbupdate()
    {
		$sql = "select city_name,state_abbreviation,state_name,id from locations_new where state_abbreviation = '' limit 100 ";
		$res = DB::select($sql);//->get()->toArray();
		foreach($res as $val){
			$arr = explode(',',$val->city_name);
			$sql = "update locations_new set city_name = '".$arr[0]."',state_abbreviation = '".$arr[1]."',state_name = '".$arr[2]."' where id = ".$val->id;			
			DB::update($sql);
			echo time();
			//exit;
		}
	}
}
