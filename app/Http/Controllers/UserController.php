<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Auth;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use App\Address;
use App\Location;
use App\CleanerScheduleCalendar;
use App\CustomerCalendar;
use App\CleanerCalendar;
use App\Plan;
use App\Subscription;

class UserController extends Controller
{
	public function __construct()
    {
		$this->type = 0;
		$this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $userType = Auth::user()->type;
			if($userType != 2){
				return redirect('home');
			}else{
				return $next($request);
			}
		});
	}

	/*function admin(){
		**if(Auth::user()->id != 1){
			return view('home');
		}*			
	}*/
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users 		= User::where('type','=',1)->orderBy('id','DESC')->paginate(150);
		$address 	= array();
		$location 	= array();		
		$res 		= Location::select('id','name')->where('id','>',0)->get()->toArray();
		$i = 0;
		foreach($res as $val){
			$i++;
			$location[$val['id']] 		= $val['name'];
		}				
		$res 		= Address::select('user_id','location_id','address_details')->where('id','>',0)->get()->toArray();
		$i = 0;
		foreach($res as $val){
			$i++;
			$address[$val['user_id']][] 		= $val;
		}
		//dd($location);
		
		$subscription = array();
		$res = Subscription::orderBy('id','DESC')->get()->toArray();
		foreach($res as $val){
			$subscription[$val['user_id']][] = $val;
		}
		//dd($subscription);
		
		return view('UserCRUD.index',compact('users','address','location','subscription'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('UserCRUD.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'           
        ]);
        User::create($request->all());
        return redirect()->route('user.index')
                        ->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('UserCRUD.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user 		= User::find($id);
		$address 	= Address::where('user_id',$user['id'])->get()->toArray();
		$calendar 	= CleanerScheduleCalendar::where('cleaner_id',$user['id'])->get()->toArray();

		if(TRUE){
			
			$res = CleanerCalendar::select("id","cleaner_id","dtd","start_time","end_time","status")->where('dtd','>=',date('Y-m-d'))->where('cleaner_id',$id)->orderBy('dtd')->orderBy('cleaner_id')->orderBy('start_time')->get()->toArray();
			$calendarmaster = array();
			foreach($res as $val){
				$cleaner_time['start_time']	 = $val['start_time'];
				$cleaner_time['end_time']	 = $val['end_time'];
				$cleaner_time['status']	 	= $val['end_time'];
				$calendarmaster[$val['dtd']][] = $cleaner_time;
			}
			
			$res			= CustomerCalendar::select('id','status','cleaner_id','dtd','from_time','to_time')->where('dtd','>=',date('Y-m-d'))->where('cleaner_id',$id)->orderBy('dtd')->orderBy('cleaner_id')->orderBy('from_time')->get()->toArray();
			$bookingmaster 	= array();
			foreach($res as $val){
				$sctime['status']		= $val['status'];
				$sctime['from_time']	= str_replace(':00:00','',str_replace(':30:00','',$val['from_time']));
				$sctime['to_time']		= str_replace(':00:00','',str_replace(':30:00','',$val['to_time']));
				$bookingmaster[$val['dtd']][] = $sctime;
			}
			//echo '<pre>';print_r($bookingmaster);exit;
			
			
		}
		
		//dd($calendarmaster);
		
		
        return view('UserCRUD.edit',compact('calendarmaster','bookingmaster','user','address','calendar'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',            
        ]);


        User::find($id)->update($request->all());

        return redirect()->route('user.index')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('user.index')
                        ->with('success','User deleted successfully');
    }
	
	public function cleaner(Request $request){
        $users 		= User::where('type','=',3)->orderBy('id','DESC')->paginate(150);
		$address 	= array();
		$location 	= array();
		
		$res 		= Location::select('id','name')->where('id','>',0)->get()->toArray();
		$i = 0;
		foreach($res as $val){
			$i++;
			$location[$val['id']] 		= $val['name'];
		}
		
		
		$res 		= Address::select('user_id','location_id','address_details')->where('id','>',0)->get()->toArray();
		$i = 0;
		foreach($res as $val){
			$i++;
			$address[$val['user_id']][] 		= $val;
		}

		return view('UserCRUD.cleaner',compact('users','address','location'))
            ->with('i', ($request->input('page', 1) - 1) * 5);		
	}
	
	public function slot_create(Request $request){
		if($request->user_id == 0){
			return false;
		}		
		$data['cleaner_id']			= $request->user_id;
		$data['location_id']		= $request->location_id;
		$data['status'] 			= 1;
		$data['slot_start_time'] 	= $request->start;
		$data['slot_end_time'] 		= $request->end;
		CleanerScheduleCalendar::insert($data);
	}
	
	public function slot_resize(Request $request){
		if($request->user_id == 0){
			return false;
		}
		$dtd = substr($request->newStart,0,10);
		$res = CleanerScheduleCalendar::where('cleaner_id','=',$request->user_id)->where('slot_start_time', 'like', $dtd.'%')->delete();
		
		$data['status'] 			= 1;
		$data['cleaner_id']			= $request->user_id;
		$data['slot_start_time'] 	= $request->newStart;
		$data['slot_end_time'] 		= $request->newEnd;
		CleanerScheduleCalendar::insert($data);
	}
	
	public function slot_move(Request $request){
		echo 'Start Time :: '.$request->id;
		echo 'Start Time :: '.$request->user_id;
		echo 'Start Time :: '.$request->newStart;
		echo 'Start Time :: '.$request->newEnd;
	}
	
	public function calendar(Request $request){
		$location_id = 1;
		if(isset($request->location_id)){
			$location_id = $request->location_id;
		}
		$calendar 	= CleanerScheduleCalendar::where([['id','>',0],['location_id','=',$location_id]])->get()->toArray();
        //$calendar 	= CleanerScheduleCalendar::where([['id','>',0]])->get()->toArray();
        return view('UserCRUD.calendar',compact('calendar','location_id'));
	}
	
	public function orderlist(Request $request)
	{
		$locationlist 	= array();
		$res 			= Location::all();
		foreach($res as $val){
			$locationlist[$val->id]	= $val;
		}
		$userlist 		= array();
		$res 			= User::all();
		foreach($res as $val){
			$userlist[$val->id] = $val;
		}
		$addresslist 	= array();
		$res 			= Address::all();
		foreach($res as $val){
			$addresslist[$val->id] = $val;
		}
		//dd($addresslist);
		$bookinglist	= CustomerCalendar::all();
		return view('UserCRUD.orderlist',compact('userlist','locationlist','bookinglist','addresslist'));
	}
	
	
	public function calendarmaster(Request $request){
		$res = CleanerCalendar::select("id","cleaner_id","dtd","start_time","end_time","status")->where('dtd','>=',date('Y-m-d'))->orderBy('dtd')->orderBy('cleaner_id')->orderBy('start_time')->get()->toArray();
		$calendarmaster = array();
		foreach($res as $val){
			$cleaner_time['start_time']	 = $val['start_time'];
			$cleaner_time['end_time']	 = $val['end_time'];
			$cleaner_time['status']	 	= $val['end_time'];
			$calendarmaster[$val['dtd']][$val['cleaner_id']][] = $cleaner_time;
		}
		
		$res			= CustomerCalendar::select('id','status','cleaner_id','dtd','from_time','to_time')->where('dtd','>=',date('Y-m-d'))->orderBy('dtd')->orderBy('cleaner_id')->orderBy('from_time')->get()->toArray();
		$bookingmaster 	= array();
		foreach($res as $val){
			$sctime['status']		= $val['status'];
			$sctime['from_time']	= str_replace(':00:00','',str_replace(':30:00','',$val['from_time']));
			$sctime['to_time']		= str_replace(':00:00','',str_replace(':30:00','',$val['to_time']));
			$bookingmaster[$val['dtd']][$val['cleaner_id']][] = $sctime;
		}
		//echo '<pre>';print_r($bookingmaster);exit;
		
        $res 	= User::where('type','=',3)->get()->toArray();
		$users	= array();
		foreach($res as $val){
			$users[$val['id']]	= $val; 
		}
/*		$address 	= array();
		$location 	= array();
		
		$res 		= Location::select('id','name')->where('id','>',0)->get()->toArray();
		$i = 0;
		foreach($res as $val){
			$i++;
			$location[$val['id']] 		= $val['name'];
		}
		
		
		$res 		= Address::select('user_id','location_id','address_details')->where('id','>',0)->get()->toArray();
		$i = 0;
		foreach($res as $val){
			$i++;
			$address[$val['user_id']][] 		= $val;
		}
*/
		return view('UserCRUD.calendarmaster',compact('calendarmaster','bookingmaster','users'))
            ->with('i', ($request->input('page', 1) - 1) * 5);		

	}
	
	public function payment(Request $request){
		$id = $request->id;
		CustomerCalendar::where('id',$id)->update(array('status'=>12,'updated_at'=>date('Y-m-d H:i:s')));
		return redirect()->route('orderlist')->with('success','Work id '.$id.' marked as paid successfully!');
	}
}