<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CustomerCalendarRequest extends Model
{
	protected $table = 'schedule_cust_calendar_request';
    public $fillable = ['order_id','cleaner_id','status'];
}