<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CleanerCalendarSlot extends Model
{
	protected $table = 'cleaner_calendar_slot';
    public $fillable = ['cleaner_id','cleaner_calendar_id','dtd','slot','status'];
}


