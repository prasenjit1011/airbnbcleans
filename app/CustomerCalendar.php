<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CustomerCalendar extends Model
{
	protected $table = 'schedule_cust_calendar';
    public $fillable = ['status','cleaning_type','customer_id','address_id','location_id','cleaner_id','dtd','from_time','to_time','rating','total_review','cleaner_amt','contact_person','contact_phonenumber','bathroomdetails','bedroomdetails','property_access_type','accessdetails'];
}