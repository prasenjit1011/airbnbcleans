<?php //echo '<pre>';print_r($bookinglist);exit;?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
	<head>
		<!-- Basic Page Needs -->
		<meta charset="UTF-8">
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		<title>{{ env('APP_NAME')}}</title>

		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="author" content="riyan04314@gmail.com">

		<!-- Mobile Specific Metas -->
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Boostrap style -->
		<!-- <link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/bootstrap.min.css"> -->

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/bootstrap4-alpha3.min.css">

		<!-- FONTS-->
		<link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">

		<!-- Theme style -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/style.css">

		<!-- Calendar -->
		<link href='/userdashboard/stylesheets/fullcalendar.min.css' rel='stylesheet' />
		<link href='/userdashboard/stylesheets/fullcalendar.print.min.css' rel='stylesheet' media='print' />

		<!-- Responsive -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/responsive.css">

		<!-- Favicon -->
	    <link href="/userdashboard/images/icon/favicon.png" rel="shortcut icon">
<style>

/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:1.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}
</style>
<script>
var ratingValue = 0;
var schedule_id = 0;
</script>		
	</head>
	<body>
<input type="hidden" id="cancid" value="0" />
		<!-- Loader -->
		<div class="loader">
		  	<div class="inner one"></div>
		  	<div class="inner two"></div>
		  	<div class="inner three"></div>
		</div>

		<header id="header" class="header fixed">
			<div class="navbar-top">
				<div class="curren-menu info-left">
					<div class="logo">
						<a href="/" title="">
							<img src="/userdashboard/img/logo.png" alt="One Admin">
						</a>
					</div><!-- /.logo -->
					<div class="top-button">
						<span></span>
					</div><!-- /.top-button -->					
				</div><!-- /.curren-menu -->
				<ul class="info-right">
					<li class="user">
						<div class="avatar">
							<img src="/profile/{{ Auth::user()->profile_image }}" alt="">
						</div>
						<div class="info">
							<p class="name">{{ Auth::user()->name }}</p>
							<p class="address"></p>
						</div>
						<div class="arrow-down">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
							<i class="fa fa-angle-up" aria-hidden="true"></i>
						</div>
						<div class="dropdown-menu">
							<ul>
								<li>
									<div class="avatar d-none">
										<img src="/profile/{{ Auth::user()->profile_image }}" alt="">
									</div>
									<div class="clearfix"></div>
								</li>
								<li>
									<a href="#" class="waves-effect" onclick="javascript:$('.setting').click()" title="">My Account</a>
								</li>
								<li>
									<a class="waves-effect" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>									
								</li>
							</ul>
						</div><!-- /.dropdown-menu -->
						<div class="clearfix"></div>
					</li><!-- /.user -->
					<li class="button-menu-right">
						<img src="/userdashboard/img/icon/menu-right.png" alt="">
					</li><!-- /.button-menu-right -->
				</ul><!-- /.info-right -->
				<div class="clearfix"></div>
			</div>	<!-- /.navbar-top -->
		</header><!-- /header <-->

		<section class="vertical-navigation left">
			<div class="user-profile">
				<div class="user-img">
					<a href="#" title="">
						<div class="img">
							<img src="/profile/{{ Auth::user()->profile_image }}" alt="">
						</div>
						<div class="status-color blue heartbit style1"></div>
					</a>
				</div>
				<ul class="user-options">
					<li class="name"><a href="#" title="">{{ Auth::user()->name }}</a></li>
					<li class="options"></li>
				</ul>
			</div>
			<ul class="sidebar-nav">
				<li class="dashboard waves-effect waves-teal active">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/monitor.png" alt="">
					</div>
					<span>Jobs</span>
				</li>
				<li class="setting waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/setting-02.png" alt="">
					</div>
					<span>My Profile</span>
				</li>
				
				<li class="pages waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/calendar.png" alt="">
					</div>
					<span>Calendar</span>
				</li>	
				<li class="pages waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/setting-02.png" alt="">
					</div>
					<span>Review</span>
				</li>
				<li class="appsold waves-effect waves-teal" style="display:none1;">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/apps.png" alt="">
					</div>
					<span>Messages</span>
				</li>
				<li class="appsold waves-effect waves-teal CustomerProfile" style="display:none1;">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/apps.png" alt="">
					</div>
					<span>Customer</span>
				</li>
				
			</ul>
		</section><!-- /.vertical-navigation -->

		<main>
			<section id="dashboard">	
				@php($lightbox = 0)
				@php($allimages = array())
				@if(Auth::user()->type == 3)					
					@php($rows = array('pendingResponse','scheduleJob','waitingForCustomerConfirm','completedJob','disputedJob'))
					@foreach($rows as $r)
						@if($r == 'pendingResponse')
							@php($rid = 'sjl')
							@php($arr = array(1))
							@php($title = 'Pending Job List')
						@elseif($r == 'scheduleJob')
							@php($rid = 'sjl')
							@php($arr = array(3,4,5,6,7))
							@php($title = 'Schedule Job List')
						@elseif($r == 'completedJob')
							@php($rid = 'cjl')
							@php($arr = array(2,12))
							@php($title = 'Completed Job List')
						@elseif($r == 'waitingForCustomerConfirm')
							@php($rid = 'wfcc')
							@php($arr = array(8))
							@php($title = 'Waiting for confirm')
						@elseif($r == 'disputedJob')
							@php($rid = 'djl')
							@php($arr = array(9))
							@php($title = 'Disputed Job List')
						@endif						
						<div class="rows">
							<div class="box box-danger left" style="height:auto;">
								<div class="box-header with-border">
									<div class="box-inbox right" style="height:auto;">
										<h3 class="cleanerlist">{{ $title }}</h3>
										<div class="box-content">
											<ul class="inbox-list cleanerlist">
												@foreach($bookinglist as $val)
													@if(in_array($val->status,$arr))
														@if($val->status == 1 && $val->created_at < date('Y-m-d H:i:s',strtotime('-24 hours')))
														@else
														<li class="waves-effect">
															<a href="#" title="">
																<div class="left">
																	<img src="/profile/{{ $userlist[$val->customer_id]->profile_image }}" style="max-width:50px;">
																	<div class="info">
																		<p class="name">Customer Name : @if(isset($userlist[$val->customer_id]->name)){{ $userlist[$val->customer_id]->name}}@endif</p>
																		<p>Job ID : {{ $val->id }}</p>
																		@if($val->status == 1)
																			<p>Status : Pending</p>
																		@elseif($val->status == 2)
																			<p>Status : Completed</p>
																			<p>Payment : In Progress</p>
																		@elseif($val->status == 3)
																			<p>Status : Canceled</p>
																		@elseif($val->status == 4)
																			<p>Status : Accepted by Cleaner</p>
																		@elseif($val->status == 5)
																			<p>Status : Cancel by Cleaner</p>
																		@elseif($val->status == 6)
																			<p>Status : Cancel by Admin</p>
																		@elseif($val->status == 7)
																			<p>Status : Rescheduled</p>
																		@elseif($val->status == 8)
																			<p>Status : Waiting for client confirmation</p>
																		@elseif($val->status == 9)
																			<p>Status : Mark as dispute by client</p>
																		@elseif($val->status == 12)
																			<p>Status : Completed</p>
																			<p>Payment : Paid</p>
																		@endif
																		<p>Schedule Date : <?php echo date('d-m-Y',strtotime($val->dtd)).', '.substr($val->from_time,0,5).' - '.substr($val->to_time,0,5);?></p>
																		<p>Address : @if(isset($addresslist[$val->address_id]->address_details)){{ $addresslist[$val->address_id]->address_details }}@endif</p>							
																	</div>
																</div>
																<div class="right">
																	<a href="#" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" onclick="customerdetails({{$val->customer_id}})" >View Customer</a>
																	@if(in_array($val->status,array(8,9)) &&  strtotime($val->dtd.' '.$val->to_time) < time())
																		<a href="#ex7" id="markascomplete" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" rel="modal:open" onclick="javascript:$('#jid').val({{$val->id}})" >Upload Images</a>
																	@endif
																	
																	@if($val->status == 4 &&  strtotime($val->dtd.' '.$val->to_time) < time())
																		<a href="#ex7" id="markascomplete" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" rel="modal:open" onclick="javascript:$('#jid').val({{$val->id}})" >Click to mark as completed</a>
																	@endif
																
																
																
																	@if($val->status == 1)
																		<input type="button" value="Accepted" 	onclick="taskaccepted({{ $val->id}})" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" />
																		<input type="button" value="Decline" 	onclick="taskdecline({{ $val->id}})"  style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  />
																	@elseif($val->status == 2)
																		@if($val->rating >0)
																			@php($i = 0)
																			<div style="width:100%; text-align:right;">
																			@while($val->rating>$i)
																				@php($i += 1)
																				<i class="fa fa-star" style="font-size: large;" ></i>
																			@endwhile
																			</div>
																			<br>
																		@endif																	
																	@endif
																</div>
																<div class="clearfix"></div>																
																@if($val->status == 4)
																	<div class="left" style="padding-top:5px;" >
																		<div class="info">
																			<u>Property Access Details :</u>
																			<p>Access Type : {{$val->property_access_type}}</p>
																			<p>Details : {{$val->accessdetails}}</p>
																			<p>Contact Person : {{ $val->contact_person}}</p>
																			<p>Contact Number : {{ $val->contact_phonenumber}}</p>
																			<p>Bedroom : {{$val->bedroomdetails}}</p>
																			<p>Bathroom : {{$val->bathroomdetails}}</p>														
																		</div>
																	</div>
																@endif
																
																



																
																
																
																@php($images = json_decode($val->images))
																@if(is_array($images) && count($images)>0)
																	<div class="left" style="padding-top:5px;" >
																		<ul>
																		@foreach($images as $imgval)
																			@php($imgdata['src'] 	= $imgval)
																			@php($imgdata['jobid'] 	= $val->id)
																			@php($allimages[] = $imgdata)
																			<li style="float:left; border-bottom: none;">
																				<img src="/room_img/{{$imgval}}"  onclick="lightbox({{$lightbox++}})" style="width:100px; max-height:100px; border-radius:0px;" />
																			</li>
																		@endforeach
																		</ul>
																	</div>
																@endif

																@if($val->status == 2)
																	<div class="clearfix"></div>
																	<div class="left" style="padding-top:5px;" ><div class="info">{{ $val->rating_msg }}</div></div>
																@endif
																<div class="clearfix"></div>
															</a>
														</li>
														@endif
													@endif
												@endforeach
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach													
				@endif				
			</section>
			
			<section id="setting" style="display:none;">						
				<form action="{{ route('profile_update') }}" method="POST" enctype="multipart/form-data">
					@csrf			
				<div class="rows">
					<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12" style="margin-bottom:15px;">
								<h2>Profile Setting &nbsp; &nbsp;<span onclick="editProfile()" style="position:initial;" class="glyphicon glyphicon-pencil" style="cursor:pointer;">Edit</span></h2><br>
							</div>												
							<select id="selectedLocation" style="display:none; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;" >
								@foreach($userAddress as $val)
									<option value="{{ $val->id.'_'.$val->location_id}}">{{ $val->address_details }}</option>
								@endforeach
							</select>														
							<div class="col-md-8 float-left" style="padding-left:0px;">
								<div class="col-md-12 float-left" style="padding-left:0px;">
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Name</label>
										<div class="profileView">{{Auth::user()->name}}</div>
										<input type="text" class="profileEdit" name="profile_name" value="{{ Auth::user()->name}}" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Email</label>
										<div class="profileView">{{Auth::user()->email}}</div>
										<input type="text" class="profileEdit"  name="email" value="{{ Auth::user()->email }}" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Phone Number</label>
										<div class="profileView">{{Auth::user()->phone_number}}</div>
										<input type="text" class="profileEdit"  name="phone_number" value="{{ Auth::user()->phone_number }}" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Date of Birth</label>
										<div class="profileView">{{date('d/m/Y',strtotime(Auth::user()->dob))}}</div>
										<input type="text" class="profileEdit"  name="dob" value="{{ date('d/m/Y',strtotime(Auth::user()->dob)) }}" class="datepicker" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  />						
									</div>
									<div class="box-title col-md-12" style="padding-left:0px; padding-top:14px;">
										<label class="col-md-4">Gender</label>
										<div class="profileEdit" >
											<input type="radio" name="gender" value="1" checked /> Male &nbsp;
											<input type="radio" name="gender" value="2" @if(Auth::user()->gender == 2){{ 'checked' }}@endif /> Female
										</div>
										<div class="profileView">@if(Auth::user()->gender == 2){{ 'Female' }}@else{{ 'Male'}}@endif</div>							
									</div>																																																				
									<div class="box-title col-md-12" style="margin:2% 0; padding-left:0px; padding-bottom:5px;">
										<label class="col-md-4">Location</label>										
										<select class="select11 profileEdit"  name="location_id" style="color:#18191c; background-color:#898989;  margin-bottom:10px; height:30px; padding:7px 12px; width:60%;">
											@php($location_name = '')
											@foreach(locationlist() as $val)
												<option value="{{ $val['id'] }}" @if(Auth::user()->location_id == $val['id'])@php($location_name = $val['name']){{ 'selected' }}@endif >{{ $val['name'] }}</option>
											@endforeach
										</select>		
										<div class="profileView">{{$location_name}}</div>
									</div>
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Address</label>
										<textarea  name="address" class="profileEdit"  style="border-radius:0px; margin-bottom:5px; padding:7px 12px; color:#000; background-color:#898989; width:60%;">{{Auth::user()->address}}</textarea>
										<div class="profileView">{{Auth::user()->address}}</div>	
									</div>									
									<div class="box-title col-md-12 profileEdit"  style="padding-left:0px;">
										<label class="col-md-4">Profile Image</label>
										<input type="file" name="image" id="inputFile" value="{{ Auth::user()->phone_number }}" style="color:#18191c; background-color:#18191c;margin-bottom:10px; height:30px; padding:7px 12px; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12 profileEdit"  style="padding-left:0px;">
										<label class="col-md-4">Govt. ID.</label>
										<input type="file" name="govtid" id="inputFile01"  value="{{ Auth::user()->govtid }}" style="color:#18191c; background-color:#18191c;  margin-bottom:10px; height:30px; padding:7px 12px; width:60%;"  /><br>								
									</div>																		
									<div class="box-title col-md-12 profileEdit"  style="padding-left:0px;">
										<label class="col-md-4">&nbsp;</label>
										<input type="submit" value="Update" class="btn btn-success btn-sm" style="color:#000;" /> &nbsp;
										<input type="button" value="Cancel" class="btn btn-default btn-sm" onclick="cancelEditProfile()"  /><br>	
									</div>	
								</div>
							</div>
							<div class="col-md-4 float-left">																								
								@if(Auth::user()->profile_image != NULL)
									<img id="image_upload_preview" src="/profile/{{Auth::user()->profile_image}}" style="max-width:200px; margin-bottom:5px; border:1px solid #FFF;" />
								@endif								
								@if(Auth::user()->govtid != NULL)
									<img id="image_upload_preview01" src="/govtid/{{Auth::user()->govtid}}" style="max-width:200px; margin-bottom:5px; border:1px solid #FFF; @if(Auth::user()->govtid == 'govtid.jpg'){{ 'display:none;'}}@endif" /><br><br>
								@endif																
							</div>
						</div>	
					</div>
				</div>
				<div class="rows">
					<div class="box box-danger left" style="height:auto; display:none1;">						
					<div class="box-header with-border customerdefault2">
						<div class="box-title col-md-12" style="height:auto; margin-bottom:15px;">
							<h2>Notification Setting &nbsp; &nbsp;<span onclick="editProfile2()" style="position:initial;" class="glyphicon glyphicon-pencil" style="cursor:pointer;">Edit</span></h2>
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Email Notification</label>
							<div class="profileEdit2">
								<input type="radio" name="email_notification" value="1" checked /> Yes &nbsp; &nbsp; 
								<input type="radio" name="email_notification" value="0" @if(Auth::user()->email_notification == 0){{ 'checked' }}@endif /> No
							</div>
							<div class="profileView2">
								@if(Auth::user()->email_notification == 0)
									No
								@else
									Yes
								@endif
							</div>
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Text Notification</label>
							<div class="profileEdit2">
								<input type="radio" name="text_notification" value="1" checked /> Yes &nbsp; &nbsp; 
								<input type="radio" name="text_notification" value="0" @if(Auth::user()->text_notification == 0){{ 'checked' }}@endif  /> No	
							</div>
							<div class="profileView2">
								@if(Auth::user()->text_notification == 0)
									No
								@else
									Yes
								@endif							
							</div>
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Phone Call</label>
							<div class="profileEdit2">
							<input type="radio" name="phone_notification" value="1" checked /> Yes &nbsp; &nbsp; 
							<input type="radio" name="phone_notification" value="0" @if(Auth::user()->phone_notification == 0){{ 'checked' }}@endif /> No
							</div>
							<div class="profileView2">
								@if(Auth::user()->phone_notification == 0)
									No
								@else
									Yes
								@endif
							</div>
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Preference Contact Method</label>
							<div class="profileEdit2">
								<input type="radio" name="preference_notification" value="1" checked /> Email &nbsp; &nbsp; 
								<input type="radio" name="preference_notification" value="2" @if(Auth::user()->preference_notification == 2){{ 'checked' }}@endif /> Text &nbsp; &nbsp; 
								<input type="radio" name="preference_notification" value="3" @if(Auth::user()->preference_notification == 3){{ 'checked' }}@endif /> Phone Call
							</div>
							<div class="profileView2">
								@if(Auth::user()->preference_notification == 1)
									Email
								@elseif(Auth::user()->preference_notification == 2)
									Text
								@else
									Phone Call
								@endif							
							</div>
						</div>
						<div class="col-md-12 profileEdit2" style="margin:10px 0; padding-left:0px;">
							
								<label class="col-md-4">&nbsp;</label>
								<input type="submit" value="Update" class="btn btn-success btn-sm" style="color:#000;" /> &nbsp;
								<input type="button" value="Cancel" class="btn btn-default btn-sm" onclick="cancelEditProfile2()"  />
									
							
						</div>				
					</div>
					</div>
				</div>
				
				<div class="rows">
					<div class="box box-danger left" style="height:auto; display:none1;">
					<div class="box-header with-border customerdefault2">
						<div class="box-title col-md-12" style="height:auto; margin-bottom:15px;">
							<h2>Payout Method &nbsp; &nbsp;<span onclick="editProfile3()" style="position:initial;" class="glyphicon glyphicon-pencil" style="cursor:pointer;">Edit</span></h2>
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Bank Transfer</label>
							<input type="text" class="profileEdit3" name="bank_details" value="{{ Auth::user()->bank_details}}" style="margin-bottom:1px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:50%;"  />
							<div class="profileView3">{{Auth::user()->bank_details}}</div><br>
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Paypal</label>
							<input type="text" class="profileEdit3"  name="paypal_details" value="{{ Auth::user()->paypal_details}}" style="margin-bottom:1px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:50%;"  />
							<div class="profileView3">{{Auth::user()->paypal_details}}</div><br>					
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Payooneer PrePaid</label>
							<input type="text" class="profileEdit3"  name="payooneer_prepaid_details" value="{{ Auth::user()->payooneer_prepaid_details}}" style="margin-bottom:1px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:50%;"  />
							<div class="profileView3">{{Auth::user()->payooneer_prepaid_details}}</div><br>				
						</div>
						<div class="col-md-12" style="margin:10px 0; padding-left:0px;">
							<label class="col-md-4">Venmo</label>
							<input type="text" class="profileEdit3"  name="venmo_details" value="{{ Auth::user()->venmo_details}}" style="margin-bottom:1px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:50%;"  />
							<div class="profileView3">{{Auth::user()->venmo_details}}</div><br>
						</div>
						<div class="col-md-12 profileEdit3" style="margin:10px 0">
							<div class="box-title col-md-12">
								<label class="col-md-4">&nbsp;</label>
								<input type="submit" value="Update" class="btn btn-success btn-sm" style="color:#000;" /> &nbsp;
								<input type="button" value="Cancel" class="btn btn-default btn-sm" onclick="cancelEditProfile3()"  /><br>	
							</div>
						</div>		
					</div>
					</div>
				</div>
								
				</form>
			</section>
			
			<section id="calendar" style="display:none;">
				<div class="rows">
					<div class="box box-danger left" style="height:auto; display:none1; padding:15px;">						
						<div class="box-inbox right" style="height:auto;">
							<h3>
								My Availability Calendar
								<input type="button" class="btn btn-info btn-sm float-right" value="Add New Date" onclick="calendar_modal()">
							</h3>								
							<div class="box-content col-md-8">
								<ul class="inbox-list">
									@php($i=0)
									@foreach($cleanerCalendar as $key=>$cval)
										@php($i++)
										@php($txt = '')										
										<li class="waves-effect col-md-3" style="border-top:1px solid #222426; border-bottom:none;" >
											{{ date('d/m/Y',strtotime($key))}}

											@foreach($cval as $cval01)
												<p>{{ str_replace('.0','',$cval01['slot']) }}
												<?php echo $cval01['slot']<12?'AM':'PM';?>
												</p>
											@endforeach
											
										</li>
										@if($i%4 == 0)
										<div class='clearfix'></div>
										@endif
									@endforeach								
									<?php /*@php($i=0)
									@foreach($cleanerCalendar as $key=>$val)
										@php($i++)
										@php($txt = '')										
										<li class="waves-effect col-md-3" style="border-top:1px solid #222426; border-bottom:none;" onclick="calendardetails('{{$key}}')" >
											<a title="{{ $txt }}" data-toggle="tooltip" data-placement="right" title="Tooltip on right">
												<div class="left">
													<div class="info">
														<p class="name">{{ date('d/m/Y',strtotime($key))}}</p>
														@foreach($val as $val01)
															<p>{{ $val01['start_time'].'Hrs to '.$val01['end_time'].'Hrs' }}</p>
														@endforeach
														
														
														@if(isset($bookingarr[$key]))
															<p style="margin-top:5px; padding-top:5px; border-top:1px solid #000;">Booking</p>
														@foreach($bookingarr[$key] as $time)														
															<p style="margin-top:5px;">{{$time->from_time.'Hrs-'.$time->to_time.'Hrs'}}</p>
															<p>{{'Status : Accepted'}}</p>
														@endforeach
														@endif
													</div>
												</div>
												<div class="clearfix"></div>
											</a>												
										</li>
										@if($i%4 == 0)
										<div class='clearfix'></div>
										@endif
									@endforeach */?>
								</ul>
							</div>
							<div class="box-content col-md-4">
							
							<?php if(1){?>
							
								<style>
								* {box-sizing: border-box;}
								ul {list-style-type: none;}


								.month {
								  padding: 7px 5px;
								  width: 100%;
								  background: #1abc9c;
								  text-align: center;
								}

								.month ul {
								  margin: 0;
								  padding: 0;
								}

								.month ul li {
								  color: white;
								  font-size: 20px;
								  text-transform: uppercase;
								  letter-spacing: 3px;
								  border-bottom:none !important;
								  margin-left:none !important;								  
								}

								.month .prev {
								  float: left;
								  padding-top: 10px;
								}

								.month .next {
								  float: right;
								  padding-top: 10px;
								}

								.weekdays {
								  margin: 0;
								  padding: 10px 0;
								  background-color: #ddd;
								  border-bottom:none !important;
								  margin-left:none !important;
								}

								.weekdays li {
								  display: inline-block !important;
								  width: 13.6%;
								  color: #666;
								  text-align: center; 
								  border-bottom:none !important;
								  margin-left:none !important;
								}

								.days {
								  padding: 10px 0;
								  background: #eee;
								  margin: 0 !important;
								}

								.days li {
								  list-style-type: none;
								  display: inline-block !important;
								  border-bottom:none !important;
								  width: 13.6%;
								  text-align: center;
								  margin-bottom: 5px;
								  font-size:12px;
								  color: #777;
								  padding:0px !important;
								}

								.days li .active {
								  padding: 7px;
								  background: #1abc9c;
								  color: white !important
								}

								.days li .inactive {
								  padding:0px !important;
								  background: #CCC;
								  color: white !important
								}

								/* Add media queries for smaller screens */
								@media screen and (max-width:720px) {
								  .weekdays li, .days li {width: 13.1%;}
								}

								@media screen and (max-width: 420px) {
								  .weekdays li, .days li {width: 12.5%;}
								  .days li .active {padding: 2px;}
								}

								@media screen and (max-width: 290px) {
								  .weekdays li, .days li {width: 12.2%;}
								}
								</style>
								<div class="month">      
								  <ul>
									<?php /*<li class="prev">&#10094;</li>
									<li class="next">&#10095;</li>*/ ?>
									<li>
									{{date('F')}}<br>
									  <span style="font-size:18px">{{date('Y')}}</span>
									</li>
								  </ul>
								</div>

								<ul class="weekdays" style="margin-top:0px;">
								  <li>Su</li>
								  <li>Mo</li>
								  <li>Tu</li>
								  <li>We</li>
								  <li>Th</li>
								  <li>Fr</li>
								  <li>Sa</li>
								</ul>
								<ul class="days">
									<li><span style="color:#CCC;">29</span></li>
									<li><span style="color:#CCC;">30</span></li>
									<li><span style="color:#CCC;">31</span></li>
									
									@php($i = 0)
									@while($i<31)
										@php($i++)
										@if($i<date('d'))
											@php($clname = 'inactive')
											<li><a style="color:#CCC;">{{$i}}</a></li>	
										@elseif($i==date('d'))
											@php($clname = 'inactive')
											<li><a class="active" onclick="setavailabletime('{{date('Y-m').'-'.$i}}')" href="#availableCalenderModal" rel="modal:open">{{$i}}</a></li>
										@else
											@php($clname = 'active')
											<li><a style="color:#000;" onclick="setavailabletime('{{date('Y-m').'-'.$i}}')"  href="#availableCalenderModal" rel="modal:open">{{$i}}</a></li>
										@endif		
									@endwhile
									<li><span style="color:#000;">1</span></li>
								</ul>

							<?php } ?>							
							
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<section id="notification_setting" style="display:none;">
				<div class="rows">
					<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12" style="padding:10px 0;">
								<h2>Reviews</h2><br>
							</div>
							<div class="box-title col-md-12" style="padding:10px 0;">
								<label class="col-md-5">Total Review Received</label>
								{{ Auth::user()->total_review }}
							</div>
							<div class="box-title col-md-12" style="padding:10px 0;">
								<label class="col-md-5">Rating</label>
								{{ Auth::user()->rating }}
							</div>
							<div class="box-title col-md-12" style="padding:10px 0;">
								<label class="col-md-5">The number of clients that have viewed</label>
								{{$viewedcnt}}
							</div>	
					
							<?php /* Review List Cleaner Profile */ ?>
							<div class="col-md-12 float-left">							
								<div class="box-header with-border">
									<div class="box-inbox right" style="height:auto; padding-left:0px;">
										<div class="box-content" id="userdetails"></div>
										<div class="box-content">
											<ul class="inbox-list" id="cleaner_reviewlist">
											</ul>
										</div>
									</div>
								</div>								
							</div>
							<?php /* Review List Cleaner Profile */ ?>					
						</div>	
					</div>					
				</div>				
			</section>

			
			<section id="message">				   									
				<div class="box box-message">
					<div class="box-header">
						<div class="header-title">
							<img src="/userdashboard/img/icon/download.png" alt="">
							<span>Messages</span>
						</div>
					</div><!-- /.box-header -->
					<div class="box-content" style="height:auto;">
						<ul class="message-list scroll" style="height:350px;">
							@php($userids = array())
							@foreach($inboxMsg as $val)
								@if($val['sender_id'] != Auth::user()->id)
									@php($user_id = $val['sender_id'])
									@php($user_name = ucfirst($userlist[$val['sender_id']]->name))
								@else
									@php($user_id = $val['receiver_id'])
									@php($user_name = ucfirst($userlist[$val['receiver_id']]->name))
								@endif							
								@if(!in_array($user_id,$userids))
									@php($userids[] = $user_id)
									<li class="waves-effect waves-teal" onclick="msghistory({{$user_id}},'{{$user_name}}')">
										<div class="left">
											<div class="avatar">
												<img src="/profile/{{$userlist[$user_id]->profile_image}}" alt="">
												<div class="status-color blue style2 heartbit"></div>
											</div>
											<div class="content">
												<div class="username">
													<div class="name">
														{{$user_name}}
													</div>
												</div>
												<div class="text">
													<p>{{substr($val['message'],0,100)}}</p>
												</div>
											</div>
										</div>
										<div class="right">
											<div class="date">
												{{date('d/m/Y',strtotime($val['updated_at']))}}
											</div>
										</div>
										<div class="clearfix"></div>
									</li>
								@endif
							@endforeach
						</ul>						
					</div>
				</div>
				<div class="message-info right" id="msgdetails" style="display:none;">
					<div class="message-header">
						<div class="move-message" style="height:15px;">
							<?php /*<a href="#" title="">
								<span><img src="/userdashboard/img/icon/bin.png" alt=""></span>
								MOVE TO TRASH
							</a>*/ ?>
						</div>
						<div class="box-info-messager" style="margin-top:5px; padding-bottom:20px;">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="content" style="padding-top:15px;">
								<div class="username msgusername">
									
								</div>
							</div>
						</div><!-- /.box-info-messager max-height:600px;-->
					</div><!-- /.message-header -->
					<div class="message-box scroll msgcontent" style="height:auto;">
						@php($inboxMsg = array_reverse($inboxMsg))
						@foreach($inboxMsg as $val)
							
							@if($val['sender_id'] == Auth::user()->id)							
								<div class="message-in allmsg {{'urser_'.$val['receiver_id']}}"  style="display:none;">
									<div class="message-pic">
										<img src="/profile/{{Auth::user()->profile_image}}"  style="max-width:50px; max-height:50px;" alt="">
										<div class="status-color purple"></div>
									</div>
									<div class="message-body">
										<div class="message-text">
											<p>{{ $val['message']}}</p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							@endif
							@if($val['receiver_id'] == Auth::user()->id)							
								<div class="message-out allmsg {{'urser_'.$val['sender_id']}}" style="display:none;">
									<div class="message-pic">
										<img src="/profile/{{$userlist[$val['sender_id']]->profile_image}}"  style="max-width:50px; max-height:50px;" alt="">
										<div class="status-color purple"></div>
									</div>
									<div class="message-body">
										<div class="message-text">
											<p>{{$val['message']}}</p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							@endif
						@endforeach
					
					</div>
					<div class="form-chat">
						<form action="#" method="get" accept-charset="utf-8">
							<div class="message-form-chat">
								<span class="pin">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/pin.png" alt="">
									</a>
								</span><!-- /.pin -->
								<span class="message-text">
									<textarea id="msg01" name="msg01" placeholder="Type your message..." required="required"></textarea>
								</span><!-- /.message-text -->
								<span class="btn-send">
									<button onclick="sendmsg(2)" class="waves-effect" type="button">Send</button>
								</span><!-- /.btn-send -->
								<div class="icon-mobile">
									<ul>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/pin.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/camera.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/icon-message.png" alt=""></a>
										</li>
									</ul>
								</div><!-- /.icon-right -->
							</div><!-- /.message-form-chat -->
							<div class="clearfix"></div>
						</form><!-- /form -->
					</div>
				</div><!-- /.message-info -->
				<div class="clearfix"></div>
			</section><!-- /#message -->	

			
			
			<section id="calendar">				
					<div class="box box-danger left" style="height:auto; padding:20px;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12">
								<h2 id="customer_name">Customer Name</h2>
								<h5 id="customer_joining_yr" style="padding-top:5px;"></h5><br>
							</div>
							<div class="col-md-6 float-left">
								<div style="padding:20px 20px 10px 0px;">Email Id : <span id="customer_emailid"></span></div>
								<div style="padding:20px 20px 10px 0px;" id="msgbtn_hiremebtn"></div>
							</div>
							<?php /*<div class="col-md-4 float-left" id="cleaner_govtid"></div>*/ ?>
							<div class="col-md-6 float-left" id="customer_profile_image"></div>
							<div class="clearfix"></div>
							
							
<?php /* Review List Cleaner Profile */ ?>
				<div class="col-md-12 float-left">
		        	
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto; padding-left:0px;">
								<div class="box-content" id="userdetails"></div>
								<div class="box-content">
									<ul class="inbox-list" id="cleaner_reviewlist">
									</ul>
								</div>
							</div>
						</div>
					
				</div>
<?php /* Review List Cleaner Profile */ ?>


							
							
							
						</div>	
					</div>
	            <div class="clearfix"></div>	         
			</section>			
			
			
			

			<section class="member-status right">
				<div class="sidebar-member">
					<ul class="member-tab">
						<li>
							<i class="fa fa-users" aria-hidden="true"></i>
						</li>
					</ul><!-- /.member-tab -->
					<div class="content-tab">
						<div class="scroll content">
							<ul class="member-list online">
								<li class="member-header">ONLINE</li>
								@foreach($userlist as $val)	
									@if($val->type == 3)
									<li>
										<a href="#" title="">
											<div class="avatar">
												<img src="/profile/{{ $val->profile_image }}" style="max-width:45px;">
												<div class="status-color green heartbit"></div>
											</div>
											<div class="info-user">
												<p class="name">{{$val->name}}</p>
												<p class="options">Rating : {{ $val->rating }}/5, Task Complete : 785</p>
											</div>
											<div class="clearfix"></div>
										</a>		
									</li>
									@endif
								@endforeach							
							</ul><!-- /.member-list online -->												
						</div><!-- /.content scroll -->
						
					</div><!-- /.cotnent-tab -->
				</div><!-- /.sidebar-member -->
			</section><!-- /.member-status -->
		</main><!-- /main -->

		<!-- jQuery 3 -->
		<script src="/userdashboard/javascript/jquery.min.js"></script>

		<!-- Bootstrap 4 -->
		<script src="/userdashboard/javascript/tether.min.js"></script>
		<script src="/userdashboard/javascript/bootstrap4-alpha3.min.js"></script>

		<!-- Map chart  -->
		<script src="/userdashboard/javascript/ammap.js"></script>
		<script src="/userdashboard/javascript/worldLow.js"></script>

		<!-- Morris.js charts -->
		<script src="/userdashboard/javascript/raphael.min.js"></script>
		<script src="/userdashboard/javascript/morris.min.js"></script>

		<!-- Chart -->
		<script src="/userdashboard/javascript/Chart.min.js"></script>

		<!-- Calendar -->
		<script src='/userdashboard/javascript/moment.min.js'></script>
		<script src='/userdashboard/javascript/jquery-ui.js'></script>
		<script src='/userdashboard/javascript/fullcalendar.min.js'></script>

		<script type="text/javascript" src="/userdashboard/javascript/jquery.mCustomScrollbar.js"></script>
		<script src="/userdashboard/javascript/smoothscroll.js"></script>
		<script src="/userdashboard/javascript/waypoints.min.js"></script>
		<script src="/userdashboard/javascript/jquery-countTo.js"></script>
		<script src="/userdashboard/javascript/waves.min.js"></script>
		<script src="/userdashboard/javascript/canvasjs.min.js"></script>

		<script src="/userdashboard/javascript/main.js"></script>	
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>		
<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />


<?php if(1){?>
    <link href="/gallery/ninja-slider.css" rel="stylesheet" type="text/css" />
    <script src="/gallery/ninja-slider.js" type="text/javascript"></script>
    <script>
        function lightbox(idx) {
            //show the slider's wrapper: this is required when the transitionType has been set to "slide" in the ninja-slider.js
            var ninjaSldr = document.getElementById("ninja-slider");
            ninjaSldr.parentNode.style.display = "block";

            nslider.init(idx);

            var fsBtn = document.getElementById("fsBtn");
            fsBtn.click();
        }

        function fsIconClick(isFullscreen, ninjaSldr) { //fsIconClick is the default event handler of the fullscreen button
            if (isFullscreen) {
                ninjaSldr.parentNode.style.display = "none";
            }
        }
    </script>
    <style>

        ul li {padding: 10px 0;}
        
        .gallery img{
            width:179px;
            cursor:pointer;
        }
    </style>

<?php } ?>




<?php $imgg = 1;if($imgg){?>

    <!--start-->
    <div style="display:none;">
        <div id="ninja-slider">
            <div class="slider-inner">
                <ul>
					@foreach($allimages as $key=>$val)
                    <li>
                        <a class="ns-img" href="/room_img/{{$val['src']}}"></a>
                        <div class="caption">
                            <h3>Job ID : {{$val['jobid']}}</h3>
                        </div>
                    </li>
					@endforeach                    
                </ul>
                <div id="fsBtn" class="fs-icon" title="Expand/Close"></div>
            </div>
        </div>
    </div>

    <!--end-->
    
<?php }$imgg = 0; ?>



<div id="calendardetailsmodal" class="" style="display:none; max-width: 600px;width: 600px;">
	<div class="modal-body">
		<div class="zzrows" id="cleaner_calendar">
		</div>
	</div>
</div>
<a href="#calendardetailsmodal" id="calendardetails" style="display:none;" rel="modal:open">Open Modal</a>


<div id="availableCalenderModal" class="buytoken" style="display:none; max-width: 800px;width: 800px; padding:12px 22px; background-color:#1b1c20;">
	<div class="modal-body">
		<div class="box-header with-border customerdefault2">
			<div class="box-title">
				<div class="row stripebtn send_msg" style="padding:10px; margin:auto;">					
					<h3 style="text-align: center; padding: 10px; color: #1abc9c;">My Calendar</h3>
					<h5 style="text-align: center; padding: 10px; color: #1abc9c;" class="sdtd"></h5>
					<h5 id="msg04" style="text-align: center; padding: 10px; color: #1abc9c;"></h5>
					<form id="myaclendarfrm" method="POST">
					<input type="hidden" name="scheduleDate" id="availdate" value="0" />
					<input type="hidden" name="availability_status" id="availability_status" value="1" />
					<div class="col-md-9">
					<ul id="calendarTime">
						<?php 
							
						?>
					</ul>
					</div>
					<div class="col-md-3">
						<input type="button" class="btn btn-info" 		value="Available"		style="width:130px;"	onclick="sbmtfrm()" /><br>
						<input type="button" class="btn btn-danger"  	value="Un-Available" 	style="margin:10px 0;"	onclick="sbmtfrmunavailability()"	/><br>
						<input type="button" class="btn btn-default"  	value="Cancel" 			style="width:130px;" onclick="closemodal()"  />
					</div>
					</form>
				</div>
				<div class="row stripebtn send_msg_thanks" style="display:none;"></div>
			</div>
		</div>
	</div>				
</div>




<div id="ex3" class="modaltaskdecline" style="display:none; max-width: 600px;width: 600px;">
	<div class="modal-body">
		<div class="zzrows">
			<div class="col-md-4">
				<label>Date</label>
				<input id="scheduleDate" type="text" class="form-control datepicker" style="height:30px; padding:7px 6px;;" value="" />
			</div>
			<div class="col-md-4">
				<label>Start</label>
				<select id="scheduleStartTime" style="height:30px; padding:7px 6px; border-color:#000;;">
					@php($i = config('data.start_time'))
					@while($i<config('data.end_time')-1)
						@php($i++)
						<option value="{{ $i }}">@if($i<12){{ $i.' AM'}}@elseif($i == 12){{ '12 O\'Clock ' }}@else{{ ($i-12).'PM' }}@endif</option>
					@endwhile
				</select>
				
			</div>
			<div class="col-md-4">
				<label>End</label>
				<select  id="scheduleEndTime" style="height:30px; padding:7px 6px; border-color:#000;">
					@php($i = config('data.start_time')+1)
					@while($i<config('data.end_time'))
						@php($i++)
						<option value="{{ $i }}" selected >@if($i<12){{ $i.' AM'}}@elseif($i == 12){{ '12 O\'Clock ' }}@else{{ ($i-12).'PM' }}@endif</option>
					@endwhile
				</select>
			</div>
			<div class='clearfix'></div>
			<div class="col-md-12" style="margin-top:2%;">
				<button type="button" data-dismiss="modal" class="btn btn-primary btn-sm" onclick="addCalendarTime()">Add Date</button>
				<button type="button" data-dismiss="modal" class="btn btn-info btn-sm" onclick="closemodal()" >Cancel</button>			
			</div>
		</div>
	</div>
</div>
<a href="#ex3" id="calendarmodal" style="display:none;" rel="modal:open">Open Modal</a>

<div id="ex2" class="modaltaskdecline" style="display:none;">
	<div class="modal-body">
		Are you sure to decline this task?
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-primary" onclick="task_decline()">Decline</button>
		<button type="button" data-dismiss="modal" class="btn btn-default" onclick="closemodal()">Cancel</button>
	</div>
</div>
<a href="#ex2" id="modaltaskdecline" style="display:none;" rel="modal:open">Open Modal</a>

<div id="ex1" class="modaltaskaccepted" style="display:none;">
	<div class='success-box'>
		<br>
		<h4 id="msg" style="text-align:center; color:#070809;" >Thanks for accepting the schedule task!</h4>
		<br>
		<div class='clearfix'></div>
	</div>
</div>
<a href="#ex1" id="modaltaskaccepted" style="display:none;" rel="modal:open">Open Modal</a>

<div id="ex7" class="modaltaskaccepted" style="display:none;">
	<form action="/markascomplete/0" method="post" enctype="multipart/form-data">
		@csrf
		<input type="hidden" id="jid" name="jid" value="0" />
		<div class="modal-body">
			Please upload images of cleaning process : 
			<input type="file" name="room_img[]" multiple style="color:FFF; border:1px solid #000; margin-top:5px;" />
		</div>
		<div class="modal-footer">
			<button type="submit" data-dismiss="modal" class="btn btn-primary">Confirm</button>
			<button type="button" data-dismiss="modal" class="btn btn-default" onclick="closemodal()">Cancel</button>
		</div>
	</form>
</div>


<div id="sendmsgmodal" class="buytoken" style="display:none; max-width: 500px;width: 500px; padding:12px 22px;">
	<div class="modal-body">
		<div class="box-header with-border customerdefault2">
			<div class="box-title">
				<div class="row stripebtn send_msg">
					<textarea id="msg03" class="form-control" style="margin-bottom:5px;"></textarea>
					<input onclick="sendmsg(3)" type="button" class="btn btn-success btn-sm" value="Submit" />
					<input type="button" class="btn btn-default btn-sm" value="Cancel" />
				</div>
				<div class="row stripebtn send_msg_thanks" style="display:none;"></div>
			</div>
		</div>
	</div>				
</div>

<style>
.modal a.close-modal{
    top: 2.5px;
    right: 2.5px;
    display: block;
    width: 20px;
    height: 20px;
}
a{
	border-radius:5px;
}
.btn{
	border-radius:5px;
}
.cleanertime{
	cursor:pointer; margin:5px; padding: 12px 11px;; color: #fff;background-color: #1abc9c;width:85px; height:45px; float:left;
}
.cleanertime_bookslot{
	cursor:pointer; margin:5px; padding: 12px 11px;; color: #fff;background-color: #CCC;width:85px; height:45px; float:left;
}
</style>		

<script>

var taskid 			= 0;
var cleaner_id 		= {{Auth::user()->id}};
var msgReceiverId	= 0;
<?php /*var xx = '<?php echo json_encode($cleanerCalendar);?>';*/ ?>
var profileImg = '';
$(document).ready(function(){
	$('.profileEdit').hide();
	$('.profileView').show();
	
	$('.profileEdit2').hide();
	$('.profileView2').show();
	
	$('.profileEdit3').hide();
	$('.profileView3').show();
	$('[data-toggle="tooltip"]').tooltip();
	userdetails();
	profileImg = '{{ Auth::user()->profile_image}}';
	/*var yy = "2020-01-11";
	alert(xx[0]);*/
});

var availdate = '';
sbmtfrmunavailability = function(){
	$('#availability_status').val(2);
	sbmtfrm();
}

sbmtfrm = function(){
	var data = $('#myaclendarfrm').serialize();
	$('#myaclendarfrm').hide();
	$('#msg04').html('Please wait work in process.')
	$.post('addCalendarTime', data)
	.success(
		function(res){
			$('#availability_status').val(1);
			$('#msg04').html('Calendar time updated successfully!');
			$('#msg04').show();
			setTimeout(function() { location.reload(true); }, 4000);
		}
	);
	
	
	//setTimeout(function() { location.reload(true); }, 4000);
	/*$.ajax({
		type: 'POST',
		data: {
			"_token": "{{ csrf_token() }}",
			"data":data
		},
		url:'addCalendarTime',
		dataType: 'json',
		success:function(res){
			$('#msg').html('Calendar updated successfully.');
			//$('#modaltaskaccepted').click();		
			//setTimeout(function() { location.reload(true); }, 4000);		
		}
	});	*/
}


setavailabletime = function(){
	availdate = arguments[0];
	$('#availdate').val(availdate);
	$('.sdtd').html(availdate);
	$('#msg04').hide();
	$('#calendarTime').html('');
	var data = 'dtd='+availdate;
	$.post('cleanerCalendarTime', data)
	.success(
		function(res){
			$('#availability_status').val(1);
			$('#calendarTime').html(res);
			$('#calendarTime').show();
			$('#myaclendarfrm').show();
		}
	);	
	//
	
	
	//alert(availdate);
}

editProfile = function(){
	$('.profileView').hide();
	$('.profileEdit').show();
}

cancelEditProfile = function(){
	$('.profileEdit').hide();
	$('.profileView').show();
}


editProfile2 = function(){
	$('.profileView2').hide();
	$('.profileEdit2').show();
}

cancelEditProfile2 = function(){
	$('.profileEdit2').hide();
	$('.profileView2').show();
}


editProfile3 = function(){
	$('.profileView3').hide();
	$('.profileEdit3').show();
}

cancelEditProfile3 = function(){
	$('.profileEdit3').hide();
	$('.profileView3').show();
}

sendmsg = function(){
	var msg = 'TEST';
	//alert(arguments[0]);
	if(arguments[0] == 3){
		msg = $('#msg03').val();
	}
	else{
		msg = $('#msg01').val();
	}
	//msg = 'Test Test';	
	//alert(msg);
	//return false;
	//var rid = msgReceiverId;
	
	$.ajax({
		type: 'POST',
		data: {
			"_token": "{{ csrf_token() }}",
			"msg":msg,
			"msgReceiverId":msgReceiverId
		},
		url:'sendmsg',
		dataType: 'json',
		success:function(res){
			$('#msg01').val('');
			$('.send_msg').hide();
			$('.send_msg_thanks').show();
			$('.send_msg_thanks').html('<h4 style="text-align:center; color:#5cb85c;">Message sent to cleaner successfully!</h4>');
		}
	});
}
/*
sendmsg = function(){
	var msg = $('#msg01').val();
	//var rid = msgReceiverId;
	$.ajax({
		type: 'POST',
		data: {
			"_token": "{{ csrf_token() }}",
			"msg":msg,
			"msgReceiverId":msgReceiverId
		},
		url:'/sendmsg/',
		dataType: 'json',
		success:function(res){
			$('#msg01').val('');
			$('.send_msg').hide();
			$('.send_msg_thanks').show();
			$('.send_msg_thanks').html('<h4 style="text-align:center; color:#5cb85c;">Message sent to cleaner successfully!</h4>');
		}
	});
}
*/



msghistory = function(){
	var id = arguments[0];
	msgReceiverId = id;
	$('.msgusername').html(arguments[1]);
	$('.allmsg').hide();
	$('.urser_'+id).show();
	$('#msgdetails').show();
}


markascomplete = function(){
	var id = arguments[0];
	$.ajax({
		type: 'POST',
		data: {
			"_token": "{{ csrf_token() }}",
			"id":id
		},
		url:'/markascomplete/'+arguments[0],
		dataType: 'json',
		success:function(res){
			alert('Schedule cleaning accepted.');
			setTimeout(function() { location.reload(true); }, 4000);
		}
	});
}


<?php /*alert({{json_encode($cleanerCalendar)}});//var cleanerCalendar = {{$cleanerCalendar}};*/ ?>
calendardetails = function(){
	var dtd = arguments[0];
	$('#calendardetails').click();
	$.ajax({
		type: 'GET',
		url:'/cleaner/calendar/details/'+dtd,
		dataType: 'json',
		success:function(res){
			var str = '<table width="100%"><tr style="border-bottom:1px solid #CCC"><th>Date</th><th>Start Time</th><th>End Time</th><th>Action</th></tr>';
			$.each(res, function( index, value ) {
				str += '<tr><td style="padding:3px 0;">'+value.dtd+'</td><td style="padding:3px 0;">'+value.start_time+' Hrs.</td><td style="padding:3px 0;">'+value.end_time+' Hrs.</td><td style="padding:3px 0;">';
				if(value.cnt == 0){
					str += '<i style="color:#F00;" onclick="deletecalendar('+value.id+')" class="fa fa-times red" aria-hidden="true"></i>';
				}
				str += '</td></tr>';
			});
			str += '</table>';
			$('#cleaner_calendar').html(str);
		}
	});
}

deletecalendar = function(){
	$.ajax({
		type: 'GET',
		url:'/cleaner/delete/calendar/'+arguments[0],
		dataType: 'json',
		success:function(res){
			$('#cleaner_calendar').html('Selected time remove from calendar.');
			setTimeout(function() { location.reload(true); }, 4000);		
		}
	});
}

userdetails =  function(){
	$('.CleanerProfile').click();
	$.ajax({
		type: 'GET',
		url:'/cleaner/'+cleaner_id,
		dataType: 'json',
		success:function(res){
		/*	$('#cleaner_name').html(res.name);
			$('#cleaner_joining_yr').html('Joined in '+res.created_at);
			$('#cleaner_emailid').html(res.email_verified);
			$('#cleaner_govtid_verified').html(res.govtid_verified);
			$('#cleaner_contactno').html(res.phone_number_verified);
			$('#cleaner_rating').html(res.rating);
			$('#cleaner_taskcomplete').html(res.task_complete);
			$('#userdetails').html(res.details);
			//$('#cleaner_govtid').html('<img src="/govtid/'+res.govtid+'" width="200" />');
			$('#cleaner_profile_image').html('<img src="/profile/'+res.profile_image+'" width="250" style="max-width:250px;" />');*/

			var str = '';
			var i = 0;
			var star_str = '';
			$.each(res.reviewlist, function( index, value ) {
				/*if(value.rating<1){
					continue;
				}*/
				
				i = 0;
				star_str = '';
				while(value.rating>i){
					i += 1;
					star_str += '<i class="fa fa-star" style="font-size: large;" ></i>';
				}
				
				rating_msg = '';
				if(value.rating_msg != ''){
					rating_msg = value.rating_msg;
				}
				
				str += '<li class="waves-effect"><div class="left"><img src="/profile/'+value.profile_image+'" style="max-width:50px;"><div class="info"><p class="name">'+value.customer_name+'</p><p>Joined in '+value.created_at+'</p></div></div><div class="right"><p>'+star_str+'</p></div><div class="clearfix"></div><div class="right" style="right:0px;" ><p>'+rating_msg+'</p></div></li>'
			});
			$('#cleaner_reviewlist').html(str);			
		}
	});
}





addCalendarTime = function(){
	var scheduleDate 		= $('#scheduleDate').val();
	var scheduleStartTime 	= $('#scheduleStartTime').val();
	var scheduleEndTime 	= $('#scheduleEndTime').val();
	
	closemodal();
	$('#scheduleDate').val('');
	console.log('---'+scheduleDate+'***'+scheduleStartTime+'----'+scheduleEndTime);
	$.ajax({
		type: 'POST',
		data: {
			"_token": "{{ csrf_token() }}",
			"scheduleDate":scheduleDate,
			"scheduleStartTime":scheduleStartTime,
			"scheduleEndTime":scheduleEndTime
		},
		url:'/addCalendarTime',
		dataType: 'json',
		success:function(res){
			$('#msg').html('Calendar updated successfully.');
			$('#modaltaskaccepted').click();		
			setTimeout(function() { location.reload(true); }, 4000);		
		}
	});	
}

$(document).ready(function(){
	$('#dashboard').click();
});

calendar_modal = function(){
	$('#calendarmodal').click();
}

taskaccepted = function(){
	taskid = arguments[0];
	$.ajax({
		type: 'POST',
		data: {
			"_token": "{{ csrf_token() }}",
			"id":taskid
		},
		url:'/taskaccepted/'+taskid,
		dataType: 'json',
		success:function(res){
			$('#modaltaskaccepted').click();			
			setTimeout(function() { location.reload(true); }, 5000);		
		}
	});
}
taskdecline = function(){
	//alert(11);
	taskid = arguments[0];
	$('#modaltaskdecline').click();
}

task_decline = function(){	
	//alert(44);
	$('.modaltaskdecline').hide();
	$.ajax({
		type: 'POST',
		data: {
			"_token": "{{ csrf_token() }}",
			"id":taskid
		},
		url:'/taskdecline/'+taskid,
		dataType: 'json',
		success:function(res){
			$('#msg').html('Schedule cleaning decline successfully.');
			$('#modaltaskaccepted').click();	
			setTimeout(function() { location.reload(true); }, 5000);
		}
	});	
}

closemodal = function(){
	$('.close-modal').click();
}













function readURL(input,x) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		if(x == 0){
			reader.onload = function (e) {
				$('#image_upload_preview').attr('src', e.target.result);
			}			
		}
		
		if(x == 1){
			reader.onload = function (e) {
				$('#image_upload_preview01').show();
				$('#image_upload_preview01').attr('src', e.target.result);
			}			
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#inputFile").change(function () {
	readURL(this,0);
});

$("#inputFile01").change(function () {
	
	readURL(this,1);
});





</script>















		<script>
		$( function() {
			$( ".datepicker" ).datepicker({
				dateFormat: 'dd/mm/yy',
				minDate: new Date(),
				maxDate: '+1M'
			});
			//setTimeout(function() { searchcleaner(); }, 2000);			
		} );
		hidecleanerlist = function(){
			$('.cleanerdiv').hide();
			$('.loading').show();
			setTimeout(function() { showcleanerlist(); }, 4000);
		}
		showcleanerlist = function(){
			$('.loading').hide();
			$('.cleanerdiv').show();
		}


		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		bookcleaner = function(){
			var cleanerId 			= arguments[0];
			var selectedTime		= $('#selectedTime').val();
			var selectedDate		= $('#selectedDate').val();
			var selectedLocation	= $('#selectedLocation').val();


			$('.customerdefault').html('Please wait your request in processing in our system.');					
			
			$.ajax({
				type: 'POST',
				data: {
					"_token": "{{ csrf_token() }}",
					"cleanerId":cleanerId,
					"selectedTime":selectedTime,
					"selectedDate":selectedDate,
					"selectedLocation":selectedLocation
				},
				url:'/bookcleaner/'+arguments[0],
				dataType: 'json',
				success:function(res){
					if(res.status == 1){
						$('.customerdefault').html(res.msg);
			            location.reload(true);
					}
					else{
						alert('Selected cleaner not avail at selected date and time, Please select other cleaner!');
					}
				}
			});			
		}
		</script>


<script src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">
$(document).ready(function () {  
$('.button-menu-right').click();
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
});

cancelschedulesbmt = function(){
	/*if(confirm('Are you sure to cancel this cleaning schedule ?')){	}*/
		
		var canid = $('#cancid').val();
		$('#schedule_'+cancid).hide();
		$.ajax({
			type: 'POST',
			data: {
				/*"_token": "{{ csrf_token() }}",*/
			},
			url:'/cancelschedule/'+cancid,
			dataType: 'json',
			success:function(res){
				alert(res.msg);
			}
		});
	
	
}


cancelschedule = function(){
	var id = arguments[0];
	$("#modalhrefcancel").click();
}



  function pay(amount,plan,description) {
    var handler = StripeCheckout.configure({
      key: 'pk_test_4V5q3TkFoDgi2nloUPnjdLPL004PeEOnwo', // your publisher key id
      locale: 'auto',
      token: function (token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        console.log('Token Created!!');
        console.log(token)
        $('#token_response').html(JSON.stringify(token));

        $.ajax({
          url: '{{ route('stripe.store') }}',
          method: 'post',
		  dataType: 'JSON',
          data: { email:token.email, tokenId: token.id, amount: amount, plan:plan, description:description },
          success: (response) => {
			  
			$('.stripebtn').html('<div class="col-md-12"><div class="btn btn-success btn-block" >Payment complete successfully!<br>Order ID : '+response.id+'</div></div>');
		  },
          error: (error) => {
            console.log(error);
            alert('There is an error in processing.')
          }
        })
      }
    });
   
    handler.open({
      name: 'House Clean',
      description: description,
      amount: amount * 100
    });
}


review = function(){
	schedule_id = arguments[0];
	$("#modalhref").click();
}

</script>		
<script>
$(document).ready(function(){
	
  //$('#profilemenu').click();
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
	
    if (ratingValue > 1) {
        msg = "Thanks! You rated " + ratingValue + " stars to this cleaner.";
    }
    else {
        msg = "We will improve ourselves. You rated " + ratingValue + " stars to this cleaner.";
    }
    responseMessage(msg);
    
  });
  
  
});


function responseMessage(msg) {
  $('.success-box').fadeIn(200);  
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
}


customerdetails = function(){
	$('.CustomerProfile').click();
	$('#customer_name').html('');
	$('#customer_joining_yr').html('');
	$('#customer_emailid').html('');
	
	
	/*$('#cleaner_govtid_verified').html('');
	$('#cleaner_contactno').html('');
	$('#cleaner_rating').html('');
	$('#cleaner_taskcomplete').html('');
	$('#userdetails').html('');
	$('#cleaner_profile_image').html('');
	$('.send_msg_thanks').hide();
	$('.send_msg').show();
	$('#msg').val('');*/
			
	$.ajax({
		type: 'GET',
		url:'/customer/'+arguments[0],
		dataType: 'json',
		success:function(res){
			$('#customer_name').html(res.name);
			$('#customer_joining_yr').html('Joined in '+res.created_at);
			$('#customer_emailid').html(res.email_verified);
			
			/*$('#cleaner_govtid_verified').html(res.govtid_verified);
			$('#cleaner_contactno').html(res.phone_number_verified);
			$('#cleaner_rating').html(res.rating);
			$('#cleaner_taskcomplete').html(res.task_complete);
			$('#userdetails').html(res.details);*/
			$('#customer_profile_image').html('<img src="/profile/'+res.profile_image+'" width="250" style="max-width:250px;" />');
			
			$('#msgbtn_hiremebtn').html('<a href="#sendmsgmodal" onclick="setReceiverId('+res.id+')" class="btn btn-success btn-sm" rel="modal:open">Send Message</a>');
			
			

			/*
			var str = '';
			var i = 0;
			var star_str = '';
			$.each(res.reviewlist, function( index, value ) {
				i = 0;
				star_str = '';
				while(value.rating>i){
					i += 1;
					star_str += '<i class="fa fa-star" style="font-size: large;" ></i>';
				}
				str += '<li class="waves-effect"><div class="left"><img src="/profile/'+value.profile_image+'" style="max-width:50px;"><div class="info"><p class="name">'+value.customer_name+'</p><p>Joined in '+value.created_at+'</p></div></div><div class="right"><p>'+star_str+'</p></div><div class="clearfix"></div><div  class="left" style="padding-top:5px; right:0px;" ><p>'+value.rating_msg+'</p></div></li>'
			});
			$('#cleaner_reviewlist').html(str);		*/	
		}
	});
}

setReceiverId = function(){
	msgReceiverId = arguments[0];
}


</script>		
	</body>
</html>