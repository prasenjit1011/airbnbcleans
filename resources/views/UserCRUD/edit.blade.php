@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Cleaner Details</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('cleaner') }}"> Back</a>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row border border-primary bg-dark p-2">
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                Name : {{ $user['name'] }}                
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                Email Id : {{ $user['email'] }}
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                Phone Number : {{ $user['phone_number'] }}
            </div>
        </div>
		<div class="col-xs-4 col-sm-4 col-md-4">
			<div class="form-group">
				Location : 
				{{ $address[0]['location_id'] }}
				@foreach(locationlist() as $val)
					@if(!empty($address[0]['location_id']) && $val['id'] == $address[0]['location_id'])
						{{ $val['name'] }}
					@endif
				@endforeach
				@if(!empty($address[0]['address_details']))
					{!! '<br>'.$address[0]['address_details'] !!}
				@endif				
			</div>			
		</div>
		<div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">                
					Rating : 
					@if(!empty($user['rating']))
						{{ $user['rating'] }}
					@endif
					<br>
					Task Completed : 
					@if(!empty($user['task_complete']))
						{{ $user['task_complete'] }}
					@endif
					<br>
					Total Earning : 
					@if(!empty($user['total_earning']))
						{{ $user['total_earning'] }}
					@endif
            </div>
        </div>
    </div>
	<div class="row border border-primary bg-dark p-2">
		<table class="table table-bordered table-striped table-dark">
			<tr>
				<th>Date</th>
				<th>Available Cleaner</th>
				<th>Booking Cleaner</th>
			</tr>
			@foreach ($calendarmaster as $key => $val)
			<tr>
				<td>{{ date('d/m/Y',strtotime($key)) }}</td>
				<td>
					<ul style="padding-left:inherit;">
					@foreach($val as $data)							
							<li>{{$data['start_time'].'Hrs - '.$data['end_time'].'Hrs'}}</li>
					@endforeach
					</ul>
				</td>
				<td>
					@if(isset($bookingmaster[$key]))
					<ul style="padding-left:inherit;">					
						@foreach($bookingmaster[$key] as $data)
							<li>{{$data['from_time'].'Hrs - '.$data['to_time'].'Hrs'}}</li>
						@endforeach						
					</ul>
					@endif
				</td>
			</tr>
			@endforeach
		</table>	
	</div>
		</div>
	</div>
</div>

@endsection