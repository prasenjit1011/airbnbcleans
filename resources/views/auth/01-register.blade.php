<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">
<title>House Clean</title>
<link rel="stylesheet" type="text/css" href="/frontend/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/bootstrap-slider.min.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/fontawesome-all.min.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/slick.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/style.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/custom.css">
<style>
select{
	width: 100%;
    padding: 8px 20px;
    text-align: left;
    border: 0;
    outline: 0;
    border-radius: 8px;
    background-color: #f0f4fd;
    font-size: 13px;
    color: #526489;
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;	
}
</style>
</head>

<body class="fullpage">
<div id="form-section" class="container-fluid signup">
    <div class="website-logo">
        <a href="index.html">
            <div class="logo" style="width:62px;height:18px"></div>
        </a>
    </div>
    <div class="row">
        <div class="info-slider-holder">
            <div class="info-holder">
                <h6>A Service you can anytime modify.</h6>
                <div class="bold-title">it’s not that hard to get<br>
    a website <span>anymore.</span></div>
                <div class="mini-testimonials-slider">
                    <div>
                        <div class="details-holder">
                            <img class="photo" src="images/person1.jpg" alt="">
                            <h4>Chris Walker</h4>
                            <h5>CEO & CO-Founder @HelloBrandio</h5>
                            <p>“In hostify we trust. I am with them for over
    7 years now. It always felt like home!
    Loved their customer support”</p>
                        </div>
                    </div>
                    <div>
                        <div class="details-holder">
                            <img class="photo" src="images/person2.jpg" alt="">
                            <h4>Chris Walker</h4>
                            <h5>CEO & CO-Founder @HelloBrandio</h5>
                            <p>“In hostify we trust. I am with them for over
    7 years now. It always felt like home!
    Loved their customer support”</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-holder">
            <div class="menu-holder">
                <ul class="main-links">
                    <li><a class="normal-link" href="/login">You have an account?</a></li>
                    <li><a class="sign-button" href="/login">Sign in</a></li>
                </ul>
            </div>
            <div class="signin-signup-form">
                <div class="form-items">
                    <div class="form-title">Sign up for new account</div>                    
                    <form id="signupform" method="POST" action="{{ route('register') }}">
                        @csrf					
                            <div class="form-text dnone" style="display:none;">
                                Type : 
								<input type="radio" name="type" value="1" style="position:inherit;"  checked  /> Customer &nbsp; &nbsp;
								<input type="radio" name="type" value="3" style="position:inherit;" /> Cleaner &nbsp; &nbsp;<br>
                                @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror									
                            </div>                        
                            <div class="form-text">
                                <input id="name" placeholder="Full Name" type="text" class="@error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror								
                            </div>
                        
                        <div class="form-text">
                            
                                <input id="email" placeholder="Email Id" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror							
                        </div>
						 <div class="form-text">
                            
                                <input id="phone_number" placeholder="Phone Number" type="text" class="@error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number">
                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror							
                        </div>
                        
                        <div class="form-text">
                                <input id="password" placeholder="Password" type="password" class="@error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror							
                        </div>
						<div class="form-text">
							<input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
						</div>
						<div class="form-text">
								<select class="select11" name="location_id">
									@foreach(locationlist() as $val)
										<option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
									@endforeach
								</select>					
                        </div>
						<div class="form-text">
							<input id="address" placeholder="Address" type="text" class="@error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>
							@error('address')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror								
						</div>
                        <div class="form-text text-holder">
							<div class="col-md-4">
								<input type="radio" name="cleantype" class="hno-radiobtn" id="rad1" value="combo" checked><label for="rad1">Combo Clean</label>
							</div>
							<div class="col-md-4">
                            <input type="radio" name="cleantype" class="hno-radiobtn" id="rad2" value="express"><label for="rad2">Express Clean</label>
							</div>
							<div class="col-md-4">
                            

							<input type="radio" name="cleantype" class="hno-radiobtn" id="rad3" value="onetime"><label for="rad3">One Time Clean</label>
							</div>						
						
						


                        </div>						
						<div class="form-button">
                            <button id="submit" type="button" onclick="makepayment()" class="ybtn ybtn-accent-color">Create new account</button>
                        </div>
                    </form>					
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>
<script src="/frontend/js/bootstrap-slider.min.js"></script>
<script src="/frontend/js/slick.min.js"></script>
<script src="/frontend/js/main.js"></script>

<script src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">
$(document).ready(function () {  
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});
});

function makepayment() {
	var amount	= 0;
	var plan	= '';
	var cleantype = $("input[name='cleantype']:checked"). val();
	if(cleantype == "combo"){
		amount	= 100;
		plan	= 'plan_GRw8eHLS3XncsN'; 
	}
	else if(cleantype == "express"){
		amount	= 50;
		plan	= 'plan_GRw7l3V3XF8h8L';
	}
	else if(cleantype == "onetime"){
		amount	= 100;
		plan	= 'plan_GRw5WU03kngq7j';
	}
	else{
		return false;
	}
	

	  
    var handler = StripeCheckout.configure({
      key: 'pk_test_4V5q3TkFoDgi2nloUPnjdLPL004PeEOnwo', // your publisher key id
      locale: 'auto',
      token: function (token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        console.log('Token Created!!');
        console.log(token)
        $('#token_response').html(JSON.stringify(token));

        $.ajax({
          url: '{{ route('stripe.store') }}',
          method: 'post',
          data: { email:token.email, tokenId: token.id, amount: amount, plan:plan },
          success: (response) => {
			  $('#signupform').submit();
			$('.stripebtn').html('<div class="col-md-12"><button class="btn btn-success btn-block" >Payment complete successfully!</button></div>');
			console.log(response);
          },
          error: (error) => {
            console.log(error);
            alert('There is an error in processing.')
          }
        })
      }
    });
   
    handler.open({
      name: 'House Clean',
      description: 'Token Subscription',
      amount: amount * 100
    });
  }
</script>
</body>
</html>
