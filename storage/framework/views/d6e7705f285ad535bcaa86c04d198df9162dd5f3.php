<?php 
//foreach($userlist as $val){echo '<pre>';print_r($val['id']);echo '</pre>';exit;}exit;
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
	<head>
		<!-- Basic Page Needs -->
		<meta charset="UTF-8">
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		<title><?php echo e(env('APP_NAME'), false); ?></title>

		<meta name="csrf-token" content="<?php echo e(csrf_token(), false); ?>">
		<meta name="author" content="riyan04314@gmail.com">

		<!-- Mobile Specific Metas -->
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Boostrap style -->
		<!-- <link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/bootstrap.min.css"> -->

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/bootstrap4-alpha3.min.css">

		<!-- FONTS-->
		<link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">

		<!-- Theme style -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/style.css">

		<!-- Calendar -->
		<link href='/userdashboard/stylesheets/fullcalendar.min.css' rel='stylesheet' />
		<link href='/userdashboard/stylesheets/fullcalendar.print.min.css' rel='stylesheet' media='print' />

		<!-- Responsive -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/responsive.css">

		<!-- Favicon -->
	    <link href="/userdashboard/img/icon/favicon.png" rel="shortcut icon">
<style>

/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:1.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}
.pkcontainer {
  position: relative;
  text-align: center;
  color: white;
}
.pkcentered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
</style>
<script>
var ratingValue = 0;
var schedule_id = 0;
var cleaner_id	= 0;
</script>		
	</head>
	<body>
<input type="hidden" id="cancid" value="0" />
		<!-- Loader -->
		<div class="loader">
		  	<div class="inner one"></div>
		  	<div class="inner two"></div>
		  	<div class="inner three"></div>
		</div>

		<header id="header" class="header fixed">
			<div class="navbar-top">
				<div class="curren-menu info-left">
					<div class="logo">
						<a href="/" title="">
							<img src="/userdashboard/img/logo.png" alt="One Admin">
						</a>
					</div><!-- /.logo -->
					<div class="top-button">
						<span></span>
					</div><!-- /.top-button -->					
				</div><!-- /.curren-menu -->
				<ul class="info-right">
					
					<?php /* 
					<li class="notification">
						<a href="#" class="waves-effect waves-teal" title="" style="color:#FFF !important; background-color:#191a1e; width:70px;">
							<img src="/userdashboard/img/icon/letter.png" alt="" style="width:43px;" >
							{{$msgcnt}}
						</a>
					</li>
					*/?>
					<li class="notification" onclick="javascript:$('.mymessage').click();">
						<a href="#" class="waves-effect waves-teal" title="" style="color:#FFF !important; background-color:#1a1b1f; width:auto;">
							<img src="/userdashboard/img/icon/message.png" alt="" >
							<?php echo e($msgcnt, false); ?>

						</a>
					</li>
					
					
					<li class="notification">
						<a href="#buytoken" rel="modal:open"  class="waves-effect waves-teal" title="" id="token_btn" >
							Token
						</a>
					</li><!-- /.notification -->
					<li class="user">
						<div class="avatar">
							<img src="/profile/<?php echo e(Auth::user()->profile_image, false); ?>" alt="">
						</div>
						<div class="info">
							<p class="name"><?php echo e(Auth::user()->name, false); ?></p>
							<p class="address"></p>
						</div>
						<div class="arrow-down">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
							<i class="fa fa-angle-up" aria-hidden="true"></i>
						</div>
						<div class="dropdown-menu">
							<ul>
								<li>
									<div class="avatar d-none">
										<img src="/profile/<?php echo e(Auth::user()->profile_image, false); ?>" alt="">
									</div>
									<div class="clearfix"></div>
								</li>
								<li>
									<a href="#" class="waves-effect" onclick="javascript:$('.setting').click()" title="">My Account</a>
								</li>
								<li>
									<a class="waves-effect" href="<?php echo e(route('logout'), false); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <?php echo e(__('Logout'), false); ?>

                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout'), false); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>									
								</li>
							</ul>
						</div><!-- /.dropdown-menu -->
						<div class="clearfix"></div>
					</li><!-- /.user -->
					<li class="button-menu-right">
						<img src="/userdashboard/img/icon/menu-right.png" alt="">
					</li><!-- /.button-menu-right -->
				</ul><!-- /.info-right -->
				<div class="clearfix"></div>
			</div>	<!-- /.navbar-top -->
		</header><!-- /header <-->

		<section class="vertical-navigation left">
			<div id="disputeFrm" class="modal">
				<div class='success-box disputeMsgDiv'>
					<textarea rows="4" cols="35" class="form-control" id="disputeMsg" placeholder="Dispute Issue"></textarea>
					<div class='clearfix'></div>
					<div class='text-message' style="height:25px;"></div>
					<div class='clearfix'></div>
					<input type="button" class="btn btn-info btn-sm" value="Submit" onclick="markasdisputesbmt()" />
					<input type="button" class="btn btn-default btn-sm close-modal " value="Cancel" />
				</div>
			</div>
			<a href="#disputeFrm" id="disputeFrmHref" style="display:none;" rel="modal:open">Open Modal</a>
			<div id="ex1" class="modal">
				<!-- Rating Stars Box -->
				<div class='rating-stars text-center mainReviewMsg'>
				<ul id='stars'>
					<li class='star' title='Poor' data-value='1'>
					<i class='fa fa-star fa-fw'></i>
					</li>
					<li class='star' title='Fair' data-value='2'>
					<i class='fa fa-star fa-fw'></i>
					</li>
					<li class='star' title='Good' data-value='3'>
					<i class='fa fa-star fa-fw'></i>
					</li>
					<li class='star' title='Excellent' data-value='4'>
					<i class='fa fa-star fa-fw'></i>
					</li>
					<li class='star' title='WOW!!!' data-value='5'>
					<i class='fa fa-star fa-fw'></i>
					</li>
				</ul>
				</div>
				<div class='success-box mainReviewMsg'>
					<p>&nbsp;</p>
					<textarea rows="4" cols="35" class="form-control" id="rating_msg" placeholder="Review Message" ></textarea>
					<div class='clearfix'></div>
					<div class='text-message' style="height:25px; display:none;"></div>
					<div class='clearfix'></div>
					<p>&nbsp;</p>
					<input type="button" class="btn btn-info btn-sm" value="Next" onclick="sbmt_other_review()" />
					<input type="button" class="btn btn-default btn-sm close-modal " value="Cancel" onclick="javascript:closemodal()"  />
				</div>
			</div>	
			<div id="otherReviewModal" class="modal">
				<div class="otherReview" style="display:none1;" onclick="sbmt_other_review()">
				<?php ($otherReview = array('timelines'=>'Time Lines','quality_of_clean'=>'Quality Of Clean','special_touches'=>'Special Touches','communication'=>'Communication')); ?>
				<?php $__currentLoopData = $otherReview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				
				<div class="col-md-4 otherReviewStar" style="margin-top:10px;"><?php echo e($val, false); ?></div>
				<div class="col-md-8 otherReviewStar" style="margin-top:10px;">
					<div class='rating-stars text-center d-none'>
						<ul id='stars'>
							<li class='star' title='Poor' data-value='1'>
							<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='Fair' data-value='2'>
							<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='Good' data-value='3'>
							<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='Excellent' data-value='4'>
							<i class='fa fa-star fa-fw'></i>
							</li>
							<li class='star' title='WOW!!!' data-value='5'>
							<i class='fa fa-star fa-fw'></i>
							</li>
						</ul>
					</div>	
				</div>
				<div class='clearfix'></div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				
				
				
				<div class='success-box d-none otherReview' style="display:none1;">
					<p>&nbsp;</p>
					<div class='clearfix'></div>
					<div class='text-message' style="height:25px; display:none;"></div>
					<p>&nbsp;</p>
					<div class='clearfix'></div>
					<input type="button" class="btn btn-info btn-sm" value="Submit" onclick="sbmtreview()" />
					<input type="button" class="btn btn-default btn-sm close-modal " value="Cancel" onclick="javascript:closemodal()"  />
				</div>				
			</div>
			<a href="#ex1" id="modalhref" style="display:none;" rel="modal:open">Open Modal</a>
			<a href="#otherReviewModal" id="otherReviewModalhref" style="display:none;" rel="modal:open">Open Modal</a>
			


			
				<script>
					sbmt_other_review = function(){
						//$('.mainReviewMsg').hide();
						//$('.otherReview').show();
						$('#otherReviewModalhref').click();
					}
					
					sbmtreview = function(){
						$('#review_other_href').click();
						$('.otherReviewStar').hide();
						var rating_msg = $('#rating_msg').val();
						var i 	= 0;
						var str = '';
						while(i<ratingValue){
							i++;
							str += '<i class="fa fa-star" style="font-size: large;" ></i>';
						}
						
						$('#cleanerbtn_'+schedule_id).html(str);
						
						$('#btn_'+schedule_id).hide();
						$.ajax({
							url: '<?php echo e(route("addreview"), false); ?>',
							method: 'post',
							dataType: 'JSON',
							data: { schedule_id:schedule_id, ratingValue:ratingValue, rating_msg:rating_msg, cleaner_id:cleaner_id },
							success: (response) => {
								$('.rating-stars').html('');
								$('.success-box').html('<h4 style="text-align:center;">Thanks for your review.</h4>');
								setTimeout(function() { location.reload(true); }, 5000);
							},
							error: (error) => {
							}
						});						
					}
				</script>
				
			
			

			<div id="ex2" class="modal">
			<br><br>
			<h4>Are you sure to cancel the appointment ?</h4>
			<br><hr>
			<input type="button" class="btn btn-danger btn-sm" style="float:right;" onclick="cancelschedulesbmt()" value="Submit" />
			<input type="button" class="btn btn-default btn-sm" style="float:right;" value="Cancel" onclick="javascript:closemodal()" />	
			</div>
			<a href="#ex2" id="modalhrefcancel" style="display:none;" rel="modal:open">Open Modal</a>
	
			<div class="user-profile">
				<div class="user-img">
					<a href="#" title="">
						<div class="img">
							<img src="/profile/<?php echo e(Auth::user()->profile_image, false); ?>" alt="">
						</div>
						<div class="status-color blue heartbit style1"></div>
					</a>
				</div>
				<ul class="user-options">
					<li class="name"><a href="#" title=""><?php echo e(Auth::user()->name, false); ?></a></li>
					<li class="options">ADMINISTRATOR</li>
				</ul>
			</div>
			<ul class="sidebar-nav">
				<li class="dashboard waves-effect waves-teal active">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/monitor.png" alt="">
					</div>
					<span>DASHBOARD</span>
				</li>
				<li class="apps waves-effect waves-teal" >
					<div class="img-nav">
						<img src="/userdashboard/img/icon/pages.png" alt="">
					</div>
					<span>My Plans</span>
				</li>
				<li class="setting waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/user.png" alt="">
					</div>
					<span>My Profile</span>
				</li>
				<li class="pages waves-effect waves-teal mymessage">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/message.png" alt="">
					</div>
					<span>My Messages</span>
				</li>
				
				<li class="CleanerProfile waves-effect waves-teal" style="display:none;">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/message.png" alt="" >
						<span></span>
					</div>
					<span>Cleaner Profile</span>
				</li>
				
				<li class="calendar waves-effect waves-teal" style="display:none;">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/calendar.png" alt="">
					</div>
					<span>Clealner Profile</span>
				</li>
				
				<li class="appsold waves-effect waves-teal" style="display:none;">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/apps.png" alt="">
					</div>
					<span>APPS</span>
				</li>
				
			</ul>
		</section><!-- /.vertical-navigation -->

		<main>
			<section id="dashboard">				
				<?php if(Auth::user()->type == 1): ?>
				<div class="rows">
		        	<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault">
							<div class="col-md-6">
			              	<h3 class="cleanerlist112">
								Create New Work Order 
							</h3>
							</div>
							<div class="col-md-6">
								<a href="#searchCleanerModal"  style="display:none1;" class="btn btn-info" rel="modal:open">Schedule a clean</a>
								<a href="#" style="display:none;" class="btn btn-info sendMsgCleaner" onclick="sendRequestMsg()">Send Message To Cleaner</a>
							</div>
							<div class="clearfix"></div>
							<div class="box-inbox right" style="height:auto;">
								<h3 class="loading" style="display:none;">Loading....</h3>
								<h3 class="cleanerdiv2" style="display:none;">Available Cleaner List On Selected Date-Time</h3>								
								<div class="box-content cleanerdiv">
									<ul class="inbox-list cleanerlist">
									</ul>
								</div>
							</div>							
						</div>
					</div>
				</div>
							
				<div class="rows" style="display:none;">
		        	<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault">							
							<br>							
							<div class="box-title col-md-12" style="display:none;">
								Available Express Token  is 10, valid from 10-12-2019 to 09-12-2019<hr>
							</div>
							<div class="box-title col-md-6" style="padding-bottom:5px;">
								Plan : <?php /*{!! $plan_option !!}*/?>
								
							</div>							
							<div class="box-title col-md-6" style="padding-bottom:5px;" <?php /*id="remainingToken"*/ ?>>
								Remaining Token 1
							</div>
							<div class="clearfix"></div>
							<div class="box-title col-md-6 col-sm-12 cleanerfrm" style="padding-bottom:5px;">
								Address : 
								<select id="selectedLocation" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;" onchange="searchcleaner()" >
									<?php $__currentLoopData = $userAddress; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($val->id.'_'.$val->location_id, false); ?>"><?php echo e($val->address_details, false); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php /*@foreach($locationlist as $val)
										<option value="{{ $val->id}}">{{$val->name}}</option>
									@endforeach*/ ?>
								</select>
							</div>
							
							<div class="box-title col-md-3 col-sm-6 cleanerfrm" style="padding-bottom:5px;">
								Date : 
								<?php /*<input id="selectedDate" value="<?php echo date('d/m/Y',strtotime('+3 days'));?>" type="text" class="datepicker" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:100px;;" onchange="searchcleaner()" /><?php /**/?>
							</div>
							
			              	<div class="box-title col-md-3 col-sm-6 cleanerfrm" style="padding-bottom:5px;">
								Time :
								<?php ($i = config('data.start_time')); ?>									
								<?php /*<select id="selectedTime" onchange="searchcleaner()" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:115px;;" >
									@while($i<=config('data.end_time')-1)
										@php($i++)
										<option value="{{ $i.':00:00-'.($i+1).':00:00' }}">
											@if($i<12)
												{{$i}} AM
											@elseif($i == 12)
												12 O'Clock
											@else
												{{ ($i-12)}} PM
											@endif											
										</option>
									@endwhile									
								</select>*/ ?>
							</div>
							<div class="clearfix"></div>
							

						</div>	
					</div>
				</div>
				
				
				<?php ($ijp = 0); ?>
				<?php ($lightbox = 0); ?>
				<?php ($allimages = array()); ?>
				<?php $__currentLoopData = $jobStatus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="rows">
						<div class="box box-danger left" style="height:auto;">
							<div class="box-header with-border">
								<div class="box-inbox right" style="height:auto;">
									<div class="box-content">
										<ul class="inbox-list">
											<li class="waves-effect">
												<h3><div class="left"><?php echo e($jval['title'], false); ?></div>
												<div class="right"><?php echo e(date('d/m/Y h:i A'), false); ?></div></h3>
											</li>
											
											
											<?php $__currentLoopData = $bookinglist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												
												<?php if(in_array($val['status'],$jval['status'])): ?>
												<?php if($val->cleaner_id == 0): ?>
													<?php ($profileImg = '/profile/Home-Pic1.jpg'); ?>
													
												<?php else: ?>
													<?php ($profileImg = '/profile/'.$userlist[$val->cleaner_id]->profile_image); ?>
												<?php endif; ?>
												<li class="waves-effect" id="schedule_<?php echo e($val->id, false); ?>">
													<a title="">
														<div class="left col-md-10">
															<img src="<?php echo e($profileImg, false); ?>" style="max-width:50px;" alt=""  style="cursor:pointer;" <?php if($val->cleaner_id != 0): ?>onclick="userdetails(<?php echo e($val->cleaner_id, false); ?>)"<?php endif; ?> title="AirbnbCleans" >
															<div class="info col-md-10">
																<div class="col-md-6">	
																<?php if($val->cleaner_id != 0): ?>
																<p class="name"  onclick="userdetails(<?php echo e($val->cleaner_id, false); ?>)" style="cursor:pointer;">
																	Cleaner Name : 	
																	<?php if(isset($userlist[$val->cleaner_id]->name)): ?>
																		<?php echo e($userlist[$val->cleaner_id]->name, false); ?>

																	<?php endif; ?>
																</p>
																<?php endif; ?>
																
																<?php if($val->status == 1): ?>
																	<?php if($val->created_at < date('Y-m-d H:i:s',strtotime('-24 hours'))): ?>
																		<p>Status : Any cleaner did not accept job.</p>
																	<?php else: ?>
																		<p>Status : Pending</p>
																	<?php endif; ?>
																<?php elseif($val->status == 2): ?>
																	<p>Status : Completed</p>
																<?php elseif($val->status == 3): ?>
																	<p>Status : Canceled</p>
																<?php elseif($val->status == 4): ?>
																	<p>Status : Accepted by Cleaner</p>
																<?php elseif($val->status == 5): ?>
																	<p>Status : Cancel by Cleaner</p>
																<?php elseif($val->status == 6): ?>
																	<p>Status : Cancel by Admin</p>
																<?php elseif($val->status == 7): ?>
																	<p>Status : Rescheduled</p>
																<?php elseif($val->status == 8): ?>
																	<p>Status : Cleaner mark as complete</p>
																<?php endif; ?>
																
																<?php if($val->cleaning_type == 1): ?>
																	<p>Plan : One Time Clean</p>
																<?php elseif($val->cleaning_type == 2): ?>
																	<p>Plan : Express Clean</p>
																<?php elseif($val->cleaning_type == 3): ?>
																	<p>Plan : Jumbo Express Clean</p>
																<?php endif; ?>
																<p>Job ID : <?php echo e($val->id, false); ?></p>
																<p>Schedule Date : <?php echo date('d-m-Y',strtotime($val->dtd)).', '.substr($val->from_time,0,5).' - '.substr($val->to_time,0,5);?></p>
																<p>Address : <?php if(isset($addresslist[$val->address_id]->address_details)): ?><?php echo e($addresslist[$val->address_id]->address_details, false); ?><?php endif; ?></p>
																</div>
																																
																<?php if(strlen($val->rating_msg)): ?>
																	<div class="col-md-6"><p>Review : <br><?php echo e($val->rating_msg, false); ?></p></div>
																<?php elseif(in_array($val->status,array(1,4))): ?>
																	<div class="col-md-6">																			
																		<p class="name">Property Access Details :</p>
																		<p>Access Type : <?php echo e($val->property_access_type, false); ?></p>
																		<p>Details : <?php echo e($val->accessdetails, false); ?></p>
																		<p>Contact Person : <?php echo e($val->contact_person, false); ?></p>
																		<p>Contact Number : <?php echo e($val->contact_phonenumber, false); ?></p>
																		<p>Bedroom : <?php echo e($val->bedroomdetails, false); ?></p>
																		<p>Bathroom : <?php echo e($val->bathroomdetails, false); ?></p>
																	</div>
																<?php elseif(in_array($val->status,array(9))): ?>
																	<div class="col-md-6">
																		<p class="name">Dispute Message : </p>
																		<p><?php echo e($val->disputeMsg, false); ?></p>
																	</div>
																<?php endif; ?>
																																																												
																<div class="col-md-12">
																<?php ($images = json_decode($val->images)); ?>
																<?php if(is_array($images) && count($images)>0): ?>
																	<ul>
																	<?php ($imgnum = 0); ?>
																	<?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imgval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<?php ($imgdata['src'] 	= $imgval); ?>
																		<?php ($imgdata['jobid'] 	= $val->id); ?>
																		<?php ($allimages[] = $imgdata); ?>																
																		<li style="float:left;" class="col-md-3">
																			<img onclick="lightbox(<?php echo e($lightbox++, false); ?>)"   src="/room_img/<?php echo e($imgval, false); ?>" style="width:100px; max-height:100px; border-radius:0px;" />
																		</li>
																		<?php ($imgnum++); ?>
																		<?php if($imgnum%4 == 0): ?>
																			<div class="clearfix"></div>
																		<?php endif; ?>																		
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																	</ul>
																<?php endif; ?>
																
																<?php if(isset($cleanerRequestIds[$val->id]) && count($cleanerRequestIds[$val->id])>0): ?>
																	<ul style="margin-top:3px;">																	
																	<?php $__currentLoopData = $cleanerRequestIds[$val->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imgval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>																		
																		<li style="float:left;">
																			<img onclick="userdetails(<?php echo e($imgval, false); ?>)" src="/profile/<?php echo e($userlist[$imgval]->profile_image, false); ?>" style="width:35px; max-height:35px; " />
																		</li>																		
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																	</ul>
																<?php endif; ?>
																</div>
															</div>
														</div>
														<div class="right" id="cleanerbtn_<?php echo e($val->id, false); ?>">
															<?php if(in_array($val->status,array(1,3,4,5,6,7,8,9))): ?>															
																<?php if(in_array($val->status,array(8,9))): ?>
																	<input type="button" value="CONFIRM" class="roundedBtn" onclick="markascomplete(<?php echo e($val->id, false); ?>)" />
																	<?php if(in_array($val->status,array(8))): ?>
																	<input type="button"  value="DISPUTE" class="roundedBtn"  onclick="markasdispute(<?php echo e($val->id, false); ?>,'dispute')" />
																	<?php endif; ?>																
																<?php endif; ?>
																
																<?php if(strtotime($val->dtd)-strtotime(date('Y-m-d'))>24*60*60): ?>
																	<input type="button" value="Cancel"  class="roundedBtn"  onclick="cancelschedule(<?php echo e($val->id, false); ?>)" />															
																	<input type="button" value="Reschedule"  class="roundedBtn"  onclick="cancelschedule(<?php echo e($val->id, false); ?>)" />
																<?php endif; ?>
															<?php elseif(in_array($val->status,array(2))): ?>
																<?php if($val->rating>0): ?>
																	<?php ($i = 0); ?>
																	<?php while($val->rating>$i): ?>
																		<?php ($i += 1); ?>
																		<i class="fa fa-star" style="font-size: large;" ></i>
																	<?php endwhile; ?>
																<?php else: ?>
																	<input type="button" value="Give Review"  class="roundedBtn" id="btn_<?php echo e($val->id, false); ?>" onclick="review(<?php echo e($val->id.','.$val->cleaner_id, false); ?>)" />
																<?php endif; ?>
															<?php endif; ?>
														</div>
														<div class="clearfix"></div>
													</a>
												</li>
												
												<?php endif; ?>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</ul>										
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>				
			</section>

			<section id="message">
				   	<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12">
								<div class="row stripebtn">
									<h3 class="cleanerlist11">Purchase or Renew Plan</h3>
									<div class="col-md-4">
										<button class="btn btn-primary btn-block" onclick="pay(50,'plan_GSQLWvdYPjkcGJ','One Time Cleaning')">One Time Cleaning</button>
									</div>
									<div class="col-md-4">
										<button class="btn btn-success btn-block" onclick="pay(250,'plan_GSQCkEc344j1JU','Express Cleaning')">Express Cleaning</button>
									</div>
									<div class="col-md-4">
										<button class="btn btn-info btn-block" onclick="pay(400,'plan_GSQB4nqOJPNamT','Express Jumbo')">Express Jumbo</button>
									</div>
								</div>	
							</div>
						</div>
					</div>


					
		        	<div class="box box-danger left" style="height:auto;">
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto;">
								<div class="box-content">
									<h3>Valid Plan</h3>
									<ul class="inbox-list">
										<?php $__currentLoopData = $orderlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<li class="waves-effect">
												<a href="#" title="">
													<div class="left">
														<div class="info">
															<p class="name">
																Order Plan : 
																<?php if(config('data.onetimeclean') == $val->stripe_plan): ?>
																	<?php ($str = 'One Time Plan'); ?>
																	<?php echo e('One Time Clean', false); ?>

																<?php elseif(config('data.expressclean') == $val->stripe_plan): ?>
																	<?php ($str = 'Valid Till Date : '.date('d/m/Y',strtotime($val->ends_at))); ?>
																	<?php echo e('Express Clean', false); ?>

																<?php elseif(config('data.jumboclean') == $val->stripe_plan): ?>
																	<?php ($str = 'Valid Till Date : '.date('d/m/Y',strtotime($val->ends_at))); ?>
																	<?php echo e('Jumbo Express', false); ?>

																<?php endif; ?>
															</p>
															<p>Order ID :  <?php echo e($val->stripe_id, false); ?></p>
															<table class="tbl"><tr><td>Received Token : </td><td> <?php echo e($val->quantity, false); ?></td></tr>
															<tr><td>Used Token : </td><td> <?php echo e($val->used_quantity, false); ?></td></tr>
															<tr><td>Remaining Token : </td><td> <?php echo e(($val->quantity-$val->used_quantity), false); ?></td></tr></table>
														</div>
													</div>
													<div class="right">
														<input type="button" value="<?php echo e($str, false); ?>" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" />
													</div>
													<div class="clearfix"></div>
												</a>
											</li>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					
					<div class="box box-danger left" style="height:auto;">
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto;">
								<div class="box-content">
									<h3>Expired Plan</h3>
									<ul class="inbox-list">
										<?php $__currentLoopData = $orderlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php if(in_array($val->stripe_plan,array(config('data.expressclean'),config('data.jumboclean')))): ?>
												<?php if(strtotime($val->ends_at)<time()): ?>
													<li class="waves-effect">
														<a href="#" title="">
															<div class="left">
																<div class="info">
																	<p class="name">
																		Order Plan : 
																		<?php if(config('data.expressclean') == $val->stripe_plan): ?>
																			<?php ($str = 'Valid Till Date : '.date('d/m/Y',strtotime($val->ends_at))); ?>
																			<?php echo e('Express Clean', false); ?>

																		<?php elseif(config('data.jumboclean') == $val->stripe_plan): ?>
																			<?php ($str = 'Valid Till Date : '.date('d/m/Y',strtotime($val->ends_at))); ?>
																			<?php echo e('Jumbo Express', false); ?>

																		<?php endif; ?>
																	</p>
																	<p>Order ID :  <?php echo e($val->stripe_id, false); ?></p>
																	<table class="tbl"><tr><td>Received Token : </td><td> <?php echo e($val->quantity, false); ?></td></tr>
																	<tr><td>Used Token : </td><td> <?php echo e($val->used_quantity, false); ?></td></tr>
																	<tr><td>Remaining Token : </td><td> <?php echo e(($val->quantity-$val->used_quantity), false); ?></td></tr></table>
																</div>
															</div>
															<div class="right">
																<input type="button" value="<?php echo e($str, false); ?>" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" />
															</div>
															<div class="clearfix"></div>
														</a>
													</li>
												<?php endif; ?>
											<?php endif; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					
					
		        	<div class="box box-danger left" style="height:auto;">
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto;">
								<div class="box-content">
									<h3>My Purchased  List</h3>
									<ul class="inbox-list">
										<?php $__currentLoopData = $orderlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<li class="waves-effect">
												<a href="#" title="">
													<div class="left">
														<div class="info">
															<p class="name">
																Order Plan : 
																<?php if(config('data.onetimeclean') == $val->stripe_plan): ?>
																	<?php ($str = 'One Time Plan'); ?>
																	<?php echo e('One Time Clean', false); ?>

																<?php elseif(config('data.expressclean') == $val->stripe_plan): ?>
																	<?php ($str = 'Valid Till Date : '.date('d/m/Y',strtotime($val->ends_at))); ?>
																	<?php echo e('Express Clean', false); ?>

																<?php elseif(config('data.jumboclean') == $val->stripe_plan): ?>
																	<?php ($str = 'Valid Till Date : '.date('d/m/Y',strtotime($val->ends_at))); ?>
																	<?php echo e('Jumbo Express', false); ?>

																<?php endif; ?>
															</p>
															<p>Order ID :  <?php echo e($val->stripe_id, false); ?></p>
															<table class="tbl"><tr><td>Received Token : </td><td> <?php echo e($val->quantity, false); ?></td></tr>
															<tr><td>Used Token : </td><td> <?php echo e($val->used_quantity, false); ?></td></tr>
															<tr><td>Remaining Token : </td><td> <?php echo e(($val->quantity-$val->used_quantity), false); ?></td></tr></table>
														</div>
													</div>
													<div class="right">
														<input type="button" value="<?php echo e($str, false); ?>" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" />
													</div>
													<div class="clearfix"></div>
												</a>
											</li>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					
					
					
					
						
				<div class="box box-message" style="display:none;">
					<div class="box-header">
						<div class="header-title">
							<img src="/userdashboard/img/icon/download.png" alt="">
							<span>INBOX</span>
						</div>
					</div><!-- /.box-header -->
					<div class="box-content">
						<ul class="message-list scroll">
							<?php $i = 0;while($i<10){$i++;?>
							<li class="waves-effect waves-teal">
								<div class="left">
									<div class="avatar">
										<img src="/userdashboard/img/avatar/message-01.png" alt="">
										<div class="status-color blue style2 heartbit"></div>
									</div>
									<div class="content">
										<div class="username">
											<div class="name">
												Jonathan Alex
											</div>
										</div>
										<div class="text">
											<p>Hi, please loock my last design</p>
											<p>I hope you like it.</p>
										</div>
									</div>
								</div><!-- /.left -->
								<div class="right">
									<div class="date">
										Today, 10:15 PM
									</div>
								</div><!-- /.right -->
								<div class="clearfix"></div>
							</li><!-- /li.waves-effect -->
							<?php } ?>
						</ul><!-- /.message-list scroll -->
						<div class="new-message">
							<a href="#" class="waves-effect" title="">Compose New</a>
						</div><!-- /.new-message -->
					</div><!-- /.box-content -->
				</div><!-- /.box box-message -->
				<div class="message-info right"  style="display:none;">
					<div class="message-header">
						<div class="move-message">
							<a href="#" title="">
								<span><img src="/userdashboard/img/icon/bin.png" alt=""></span>
								MOVE TO TRASH
							</a>
						</div><!-- /.move-message -->
						<div class="box-info-messager">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="content">
								<div class="username">
									Ricky Martin
								</div>
								<div class="text">
									<p>Hi, please loock my last design</p>
									<p>I hope you like it.</p>
								</div>
							</div>
						</div><!-- /.box-info-messager -->
					</div><!-- /.message-header -->
					<div class="message-box scroll">
						
						<?php $i = 0;while($i<10){$i++;?>
						<div class="message-in">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, John</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design.</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-in -->
						<div class="clearfix"></div>
						<div class="message-out">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-07.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, Martin</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design. to offer to cooprate</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-out -->
						<div class="clearfix"></div>
						<?php } ?>
					</div>
					<div class="form-chat">
						<form action="#" method="get" accept-charset="utf-8">
							<div class="message-form-chat">
								<span class="pin">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/pin.png" alt="">
									</a>
								</span><!-- /.pin -->
								<span class="message-text">
									<textarea name="message" placeholder="Type your message..." required="required"></textarea>
								</span><!-- /.message-text -->
								<span class="camera">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/camera.png" alt="">
									</a>
								</span><!-- /.camera -->
								<span class="icon-message">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/icon-message.png" alt="">
									</a>
								</span><!-- /.icon-right -->
								<span class="btn-send">
									<button class="waves-effect" type="submit">Send</button>
								</span><!-- /.btn-send -->
								<div class="icon-mobile">
									<ul>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/pin.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/camera.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/icon-message.png" alt=""></a>
										</li>
									</ul>
								</div><!-- /.icon-right -->
							</div><!-- /.message-form-chat -->
							<div class="clearfix"></div>
						</form><!-- /form -->
					</div>
				</div><!-- /.message-info -->
				<div class="clearfix"></div>
			</section><!-- /#message -->

			
			<section id="setting" style="display:none;">
			
			<form action="<?php echo e(route('profile_update'), false); ?>" method="POST" enctype="multipart/form-data">
				<?php echo csrf_field(); ?>
				<div class="rows">
					<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12" style="margin-bottom:15px;">
								<h2>Profile Setting &nbsp; &nbsp;<span onclick="editProfile()" style="position:initial;" class="glyphicon glyphicon-pencil" style="cursor:pointer;">Edit</span></h2><br>
							</div>												
							<select id="selectedLocation" style="display:none; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;" >
								<?php $__currentLoopData = $userAddress; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($val->id.'_'.$val->location_id, false); ?>"><?php echo e($val->address_details, false); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>														
							<div class="col-md-8 float-left" style="padding-left:0px;">
								<div class="col-md-12 float-left" style="padding-left:0px;">
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Name</label>
										<div class="profileView"><?php echo e(Auth::user()->name, false); ?></div>
										<input type="text" class="profileEdit" name="profile_name" value="<?php echo e(Auth::user()->name, false); ?>" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Email</label>
										<div class="profileView"><?php echo e(Auth::user()->email, false); ?></div>
										<input type="text" class="profileEdit"  name="email" value="<?php echo e(Auth::user()->email, false); ?>" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Phone Number</label>
										<div class="profileView"><?php echo e(Auth::user()->phone_number, false); ?></div>
										<input type="text" class="profileEdit"  name="phone_number" value="<?php echo e(Auth::user()->phone_number, false); ?>" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12" style="padding-left:0px; display:none;">
										<label class="col-md-4">Date of Birth</label>
										<div class="profileView"><?php echo e(date('d/m/Y',strtotime(Auth::user()->dob)), false); ?></div>
										<input type="date" class="profileEdit"  name="dob" value="<?php echo e(date('d/m/Y',strtotime(Auth::user()->dob)), false); ?>" class="datepicker" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  />						
									</div>
									<div class="box-title col-md-12" style="padding-left:0px; padding-top:14px;">
										<label class="col-md-4">Gender</label>
										<div class="profileEdit" >
											<input type="radio" name="gender" value="1" checked /> Male &nbsp;
											<input type="radio" name="gender" value="2" <?php if(Auth::user()->gender == 2): ?><?php echo e('checked', false); ?><?php endif; ?> /> Female
										</div>
										<div class="profileView"><?php if(Auth::user()->gender == 2): ?><?php echo e('Female', false); ?><?php else: ?><?php echo e('Male', false); ?><?php endif; ?></div>							
									</div>																																																				
									<div class="box-title col-md-12" style="margin:2% 0; padding-left:0px; padding-bottom:5px;">
										<label class="col-md-4">Location</label>	
										<?php ($locations	= explode(',',Auth::user()->location_id)); ?>
										<select multiple class="select11 profileEdit"  name="location_id[]" style="color:#18191c; background-color:#898989;  margin-bottom:10px; height:100px; padding:7px 12px; width:60%;">
											<?php ($location_name = ''); ?>
											<?php $__currentLoopData = locationlist(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($val['id'], false); ?>" <?php if(in_array($val['id'],$locations)): ?><?php ($location_name .= $val['name'].','); ?><?php echo e('selected', false); ?><?php endif; ?> ><?php echo e($val['name'], false); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>		
										<div class="profileView"><?php echo e(substr($location_name,0,-1), false); ?></div>
									</div>
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Address</label>
										<textarea  name="address" class="profileEdit"  style="border-radius:0px; margin-bottom:5px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"><?php echo e(Auth::user()->address, false); ?></textarea>
										<div class="profileView"><?php echo e(Auth::user()->address, false); ?></div>	
									</div>									
									<div class="box-title col-md-12 profileEdit"  style="padding-left:0px;">
										<label class="col-md-4">Profile Image</label>
										<input type="file" name="image" id="inputFile" value="<?php echo e(Auth::user()->phone_number, false); ?>" style="color:#18191c; background-color:#18191c;margin-bottom:10px; height:30px; padding:7px 12px; width:60%;"  /><br>								
									</div>
									<div class="box-title col-md-12"  style="padding-left:0px; display:none;">
										<label class="col-md-4">Govt. ID.</label>
										<input type="file" name="govtid" id="inputFile01"  value="<?php echo e(Auth::user()->govtid, false); ?>" style="color:#18191c; background-color:#18191c;  margin-bottom:10px; height:30px; padding:7px 12px; width:60%;"  /><br>								
									</div>																		
									<div class="box-title col-md-12 profileEdit"  style="padding-left:0px;">
										<label class="col-md-4">&nbsp;</label>
										<input type="submit" value="Update" class="btn btn-success btn-sm" style="color:#000;" /> &nbsp;
										<input type="button" value="Cancel" class="btn btn-default btn-sm" onclick="cancelEditProfile()"  /><br>	
									</div>	
								</div>
							</div>
							<div class="col-md-4 float-left">																								
								<?php if(Auth::user()->profile_image != NULL): ?>
									<img id="image_upload_preview" src="/profile/<?php echo e(Auth::user()->profile_image, false); ?>" style="max-width:200px; margin-bottom:5px; border:1px solid #FFF;" />
								<?php endif; ?>								
								<?php if(Auth::user()->govtid != NULL): ?>
									<img id="image_upload_preview01" src="/govtid/<?php echo e(Auth::user()->govtid, false); ?>" style="max-width:200px; margin-bottom:5px; border:1px solid #FFF; <?php if(Auth::user()->govtid == 'govtid.jpg'): ?><?php echo e('display:none;', false); ?><?php endif; ?>" /><br><br>
								<?php endif; ?>																
							</div>
						</div>	
					</div>
				</div>	
				</form>
			
			
			
			<?php if(0){?>
			
				<div class="rows">
					<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12">
								<h2>Profile Setting</h2><br>
							</div>
							<select id="selectedLocation" style="display:none; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;" onchange="searchcleaner()" >
								<?php $__currentLoopData = $userAddress; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($val->id.'_'.$val->location_id, false); ?>"><?php echo e($val->address_details, false); ?></option>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</select>
							<div class="col-md-8 float-left">
							<form action="<?php echo e(route('profile_update'), false); ?>" method="POST" enctype="multipart/form-data">
								<?php echo csrf_field(); ?>
								
								
								
								<div class="col-md-12 float-left">
									<div class="box-title col-md-12">
										<label class="col-md-4">Name</label>
										<input type="text" class="profileEdit" name="profile_name" value="<?php echo e(Auth::user()->name, false); ?>" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  />
										<div class="profileView"><?php echo e(Auth::user()->name, false); ?></div>
									</div>
									<div class="box-title col-md-12">
										<label class="col-md-4">Email</label>
										<input type="text" class="profileEdit" name="email" value="<?php echo e(Auth::user()->email, false); ?>" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  />
										<div class="profileView"><?php echo e(Auth::user()->email, false); ?></div>
									</div>
									<div class="box-title col-md-12">
										<label class="col-md-4">Phone Number</label>
										<input type="text" class="profileEdit" name="phone_number" value="<?php echo e(Auth::user()->phone_number, false); ?>" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  />
										<div class="profileView"><?php echo e(Auth::user()->phone_number, false); ?></div>
									</div>
									<div class="box-title col-md-12 profileEdit">
										<label class="col-md-4">Profile Image</label>
										<input type="file" name="image" id="inputFile" value="" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#18191c; background-color:#18191c; width:60%;"  />
									</div>
								</div>
								<div class="col-md-12 float-left" style="padding-left:0px;">
									<div class="box-title col-md-12" style="padding-left:0px;">
										<label class="col-md-4">Location...</label>	


<select name="cars" multiple>
  <option value="volvo">Volvo</option>
  <option value="saab">Saab</option>
  <option value="opel">Opel</option>
  <option value="audi">Audi</option>
  <option value="audi">Auddddi</option><option value="audi">Accccudi</option>
</select>
										
										<select multiple class="select11 profileEdit" name="location_id" style="color:#18191c; background-color:#898989;  margin-bottom:10px; height:30px; padding:7px 12px; width:60%;">
											<?php ($location_name = ''); ?>
											<?php $__currentLoopData = locationlist(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($val['id'], false); ?>" <?php if(Auth::user()->location_id == $val['id']): ?><?php ($location_name = $val['name']); ?><?php echo e('selected', false); ?><?php endif; ?> ><?php echo e($val['name'], false); ?>+++++</option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										<div class="profileView"><?php echo e($location_name, false); ?></div>
									</div>
									<div class="box-title col-md-12">
										<label class="col-md-4">Address</label>
										<textarea  name="address" class="profileEdit" style="border-radius:0px; margin-bottom:5px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"><?php echo e(Auth::user()->address, false); ?></textarea>
										<div class="profileView"><?php echo e(Auth::user()->address, false); ?></div>
									</div>
								</div>
								<div class="col-md-12 float-left">
									<div class="box-title col-md-12">
										<label class="col-md-4">&nbsp;</label>
										<input type="submit" value="Update" style="" class="btn btn-info btn-sm" /><br>	
									</div>
								</div>
								
								
								
								
							</form>
							</div>
							<div class="col-md-4 float-left">
								<?php if(Auth::user()->profile_image != NULL): ?>
									<img id="image_upload_preview"  src="/profile/<?php echo e(Auth::user()->profile_image, false); ?>" style="max-width:200px; margin-bottom:5px; border:1px solid #FFF;" />
								<?php endif; ?>
							</div>
						</div>	
					</div>
				</div>
				
			<?php } ?>	
				
			</section>


<?php if(0){?>		
			<section id="apps" style="display:none;">
				<div class="rows">
		        	<div class="box box-danger left" style="height:auto;">
			
				<div class="box box-message">
					<div class="box-header">
						<div class="header-title">
							<img src="/images/icon/download.png" alt="">
							<span>Messages</span>
						</div>
					</div><!-- /.box-header -->
					<div class="box-content">
						<ul class="message-list scroll">
							<?php ($userids = array()); ?>
							<?php $__currentLoopData = $inboxMsg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($val['sender_id'] != Auth::user()->id): ?>
									<?php ($user_id = $val['sender_id']); ?>
									<?php ($user_name = ucfirst($userlist[$val['sender_id']]->name)); ?>
								<?php else: ?>
									<?php ($user_id = $val['receiver_id']); ?>
									<?php ($user_name = ucfirst($userlist[$val['receiver_id']]->name)); ?>
								<?php endif; ?>							
								<?php if(!in_array($user_id,$userids)): ?>
									<?php ($userids[] = $user_id); ?>
									<li class="waves-effect waves-teal active">
										<div class="left">
											<div class="avatar">
												<img src="images/avatar/message-04.png" alt="">
												<div class="status-color blue style2 heartbit"></div>
											</div>
											<div class="content">
												<div class="username">
													<div class="name">
														<?php echo e($user_name, false); ?>

													</div>
												</div>
												<div class="text">
													<p><?php echo e(substr($val['message'],0,100), false); ?></p>
												</div>
											</div>
										</div>
										<div class="right">
											<div class="date">
												<?php echo e(date('d/m/Y',strtotime($val['updated_at'])), false); ?>

											</div>
										</div>
										<div class="clearfix"></div>
									</li>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul><!-- /.message-list scroll -->
						<div class="new-message">
							<a href="#" class="waves-effect" title="">Compose New</a>
						</div><!-- /.new-message -->
					</div><!-- /.box-content -->
				</div><!-- /.box box-message -->
				<div class="message-info right">
					<div class="message-header">
						<div class="move-message">
							<a href="#" title="">
								<span><img src="images/icon/bin.png" alt=""></span>
								MOVE TO TRASH
							</a>
						</div><!-- /.move-message -->
						<div class="box-info-messager">
							<div class="message-pic">
								<img src="images/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="content">
								<div class="username">
									Ricky Martin
								</div>
								<div class="text">
									<p>Hi, please loock my last design</p>
									<p>I hope you like it.</p>
								</div>
							</div>
						</div><!-- /.box-info-messager -->
					</div><!-- /.message-header -->
					<div class="message-box scroll">
						<div class="message-in">
							<div class="message-pic">
								<img src="images/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, John</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design.</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-in -->
						<div class="clearfix"></div>
						<div class="message-out">
							<div class="message-pic">
								<img src="images/avatar/message-07.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, Martin</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design. to offer to cooprate</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-out -->
						<div class="clearfix"></div>
						<div class="message-in">
							<div class="message-pic">
								<img src="images/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, John</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design.</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-in -->
						<div class="clearfix"></div>
						<div class="message-out">
							<div class="message-pic">
								<img src="images/avatar/message-07.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, Martin</p>
									<p>Here is some of my best work example for ux & ui design works. Reply me with your openion about my work</p>
								</div>
								<div class="message-image">
									<ul>
										<li>
											<img src="images/message/01.png" alt="">
										</li>
										<li>
											<img src="images/message/02.png" alt="">
										</li>
										<li>
											<img src="images/message/03.png" alt="">
										</li>
										<li>
											<img src="images/message/04.png" alt="">
										</li>
									</ul>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-out -->
						<div class="clearfix"></div>
						<div class="message-in">
							<div class="message-pic">
								<img src="images/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, John</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design.</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-in -->
						<div class="clearfix"></div>
						<div class="message-out">
							<div class="message-pic">
								<img src="images/avatar/message-07.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, Martin</p>
									<p>Here is some of my best work example for ux & ui design works. Reply me with your openion about my work</p>
								</div>
								<div class="message-image">
									<ul>
										<li>
											<img src="images/message/01.png" alt="">
										</li>
										<li>
											<img src="images/message/04.png" alt="">
										</li>
									</ul>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-out -->
					</div>
					<div class="form-chat">
						<form action="#" method="get" accept-charset="utf-8">
							<div class="message-form-chat">
								<span class="pin">
									<a href="#" title="">
										<img src="images/icon/pin.png" alt="">
									</a>
								</span><!-- /.pin -->
								<span class="message-text">
									<textarea name="message" placeholder="Type your message..." required="required"></textarea>
								</span><!-- /.message-text -->
								<span class="camera">
									<a href="#" title="">
										<img src="images/icon/camera.png" alt="">
									</a>
								</span><!-- /.camera -->
								<span class="icon-message">
									<a href="#" title="">
										<img src="images/icon/icon-message.png" alt="">
									</a>
								</span><!-- /.icon-right -->
								<span class="btn-send">
									<button class="waves-effect" type="submit">Send</button>
								</span><!-- /.btn-send -->
								<div class="icon-mobile">
									<ul>
										<li>
											<a href="#" title=""><img src="images/icon/pin.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="images/icon/camera.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="images/icon/icon-message.png" alt=""></a>
										</li>
									</ul>
								</div><!-- /.icon-right -->
							</div><!-- /.message-form-chat -->
							<div class="clearfix"></div>
						</form><!-- /form -->
					</div>
				</div><!-- /.message-info -->
				<div class="clearfix"></div>
				
				</div></div>
			</section><!-- /#message -->
<?php } ?>		

			
			<section id="apps" style="display:none;">	
				<div class="rows">
		        	<div class="boxboxdangerleft" style="height:auto;">			
				<div class="box box-message">
					<div class="box-header">
						<div class="header-title">
							<img src="/userdashboard/img/icon/download.png" alt="">
							<span>Messages</span>
						</div>
					</div><!-- /.box-header -->
					<div class="box-content" style="height:auto;">
						<ul class="message-list scroll" style="height:350px;">
							<?php ($userids = array()); ?>
							<?php $__currentLoopData = $inboxMsg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($val['sender_id'] != Auth::user()->id): ?>
									<?php ($user_id = $val['sender_id']); ?>
									<?php ($user_name = ucfirst($userlist[$val['sender_id']]->name)); ?>
								<?php else: ?>
									<?php ($user_id = $val['receiver_id']); ?>
									<?php ($user_name = ucfirst($userlist[$val['receiver_id']]->name)); ?>
								<?php endif; ?>							
								<?php if(!in_array($user_id,$userids)): ?>
									<?php ($userids[] = $user_id); ?>
									<li class="waves-effect waves-teal" onclick="msghistory(<?php echo e($user_id, false); ?>,'<?php echo e($user_name, false); ?>')">
										<div class="left">
											<div class="avatar">
												<img src="/profile/<?php echo e($userlist[$user_id]->profile_image, false); ?>" alt="">
												<div class="status-color blue style2 heartbit"></div>
											</div>
											<div class="content">
												<div class="username">
													<div class="name">
														<?php echo e($user_name, false); ?>

													</div>
												</div>
												<div class="text">
													<p><?php echo e(substr($val['message'],0,100), false); ?></p>
												</div>
											</div>
										</div>
										<div class="right">
											<div class="date">
												<?php echo e(date('d/m/Y',strtotime($val['updated_at'])), false); ?>

											</div>
										</div>
										<div class="clearfix"></div>
									</li>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</ul>						
					</div>
				</div>
				<div class="message-info right" id="msgdetails" style="display:none;">
					<div class="message-header">
						<div class="move-message" style="height:15px;">
							<?php /*<a href="#" title="">
								<span><img src="/userdashboard/img/icon/bin.png" alt=""></span>
								MOVE TO TRASH
							</a>*/ ?>
						</div>
						<div class="box-info-messager" style="margin-top:5px; padding-bottom:20px;">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="content" style="padding-top:15px;">
								<div class="username msgusername">
									
								</div>
							</div>
						</div><!-- /.box-info-messager -->
					</div><!-- /.message-header /*max-height:600px;*/ -->
					<div class="message-box scroll msgcontent" style="height:auto; padding-top:10px; padding-left:40px; padding-right:40px;">
						<?php ($inboxMsg = array_reverse($inboxMsg)); ?>
						<?php $__currentLoopData = $inboxMsg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($val['sender_id'] == Auth::user()->id): ?>														
								<div class="message-in allmsg <?php echo e('urser_'.$val['receiver_id'], false); ?>" style="display:none;">
									<div class="message-pic">
										<img src="/profile/<?php echo e($userlist[$val['sender_id']]->profile_image, false); ?>"  style="max-width:50px; max-height:50px;" alt="">
										<div class="status-color purple"></div>
									</div>
									<div class="message-body">
										<div class="message-text">
											<p><?php echo e($val['message'], false); ?></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							<?php endif; ?>
							<?php if($val['receiver_id'] == Auth::user()->id): ?>						
								<div class="message-out allmsg <?php echo e('urser_'.$val['sender_id'], false); ?>" style="display:none;">
									<div class="message-pic">
										<img src="/profile/<?php echo e($userlist[$val['sender_id']]->profile_image, false); ?>"  style="max-width:50px; max-height:50px;" alt="">
										<div class="status-color purple"></div>
									</div>
									<div class="message-body">
										<div class="message-text">
											<p><?php echo e($val['message'], false); ?></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					
					</div>
					<div class="form-chat">
						<form action="#" method="get" accept-charset="utf-8">
							<div class="message-form-chat">
								<span class="pin">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/pin.png" alt="">
									</a>
								</span><!-- /.pin -->
								<span class="message-text">
									<textarea id="msg01" name="msg01" placeholder="Type your message..." required="required"></textarea>
								</span><!-- /.message-text -->
								<span class="btn-send">
									<button onclick="sendmsg01()"  class="wavesXXeffect" type="button">Send</button>
								</span><!-- /.btn-send -->
								<div class="icon-mobile">
									<ul>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/pin.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/camera.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/icon-message.png" alt=""></a>
										</li>
									</ul>
								</div><!-- /.icon-right -->
							</div><!-- /.message-form-chat -->
							<div class="clearfix"></div>
						</form><!-- /form -->
					</div>
				</div><!-- /.message-info -->
				<div class="clearfix"></div>
				</div></div>
			</section><!-- /#message -->	
	
			<section id="calendar">				
					<div class="box box-danger left" style="height:auto; padding:20px;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12">
								<h2 id="cleaner_name">Cleaner Name</h2>
								<h5 id="cleaner_joining_yr" style="padding-top:5px;"></h5><br>
							</div>
							<div class="col-md-5 float-left">
								<div class="float-left" id="cleaner_profile_image"></div>
								<div class="clearfix"></div>
								<div style="padding:20px 20px 10px 0px;">
									<span id="cleaner_emailid"></span>
									<span id="cleaner_govtid_verified"></span>
									<span id="cleaner_contactno"></span>
								</div>
								<div style="padding:20px 20px 10px 0px;">Speaks : <span id="cleaner_speaks"></span></div>
								<div style="padding:20px 20px 10px 0px;">Rating : <span id="cleaner_rating">0/5</span></div>
								<div style="padding:20px 20px 10px 0px;">Task Completed : <span id="cleaner_taskcomplete">0</span></div>
								<div style="padding:20px 20px 10px 0px;" id="msgbtn_hiremebtn"></div>
							</div>
							<?php /*<div class="col-md-4 float-left" id="cleaner_govtid"></div>*/ ?>
							<div class="col-md-7 float-left" id="cleaner_calendar">
							<?php if(1){?>
							<link rel="stylesheet" href="/css/calendar-customer.css" />							
								<div class="month currentMonth">      
								  <ul>
									<?php /*<li class="prev">&#10094;</li>*/ ?>
									<li class="next" onclick="nextMonth()" >&#10095;</li>
									<li>
									<?php echo e(date('F'), false); ?><br>
									  <span style="font-size:18px"><?php echo e(date('Y'), false); ?></span>
									</li>
								  </ul>
								</div>

								<ul class="weekdays currentMonth">
								  <li>Su</li>
								  <li>Mo</li>
								  <li>Tu</li>
								  <li>We</li>
								  <li>Th</li>
								  <li>Fr</li>
								  <li>Sa</li>
								</ul>
								<ul class="days currentMonth">
									<li><span style="color:#CCC;">29</span></li>
									<li><span style="color:#CCC;">30</span></li>
									<li><span style="color:#CCC;">31</span></li>
									
									<?php ($i = 0); ?>
									<?php while($i<31): ?>
										<?php ($i++); ?>
										<?php if($i<date('d')): ?>
											<?php ($clname = 'inactive'); ?>
											<li><a style="color:#CCC;"><?php echo e($i, false); ?></a></li>	
										<?php elseif($i==date('d')): ?>
											<?php ($clname = 'inactive'); ?>
											<li><a class="active" onclick="hiremedate('<?php echo e($i.'/'.date('m/Y'), false); ?>')" href="#availableCalenderModal" rel="modal:open"><?php echo e($i, false); ?></a></li>
										<?php else: ?>
											<?php ($clname = 'active'); ?>
											<li><a style="color:#000;" onclick="hiremedate('<?php echo e($i.'/'.date('m/Y'), false); ?>')"  href="#availableCalenderModal" rel="modal:open"><?php echo e($i, false); ?></a></li>
										<?php endif; ?>		
									<?php endwhile; ?>
									<li><span style="color:#CCC; cursor:pointer;"  onclick="nextMonth()" >1</span></li>
								</ul>
								
								<div class="month nextMonth">      
								  <ul>
									<li class="prev" onclick="currentMonth()" style="cursor:pointer;">&#10094;</li>
									<?php /*<li class="next">&#10095;</li>*/ ?>
									<li>
									<?php echo e(date('F',strtotime('+30 days')), false); ?><br>
									  <span style="font-size:18px"><?php echo e(date('Y'), false); ?></span>
									</li>
								  </ul>
								</div>

								<ul class="weekdays nextMonth">
								  <li>Su</li>
								  <li>Mo</li>
								  <li>Tu</li>
								  <li>We</li>
								  <li>Th</li>
								  <li>Fr</li>
								  <li>Sa</li>
								</ul>
								<ul class="days nextMonth">
									<?php ($i = 25); ?>
									<?php while($i<31): ?>
										<?php ($i++); ?>
										<li><span  onclick="currentMonth()" style="color:#CCC; cursor:pointer;"><?php echo e($i, false); ?></span></li>
									<?php endwhile; ?>									
									
									<?php ($i = 0); ?>
									<?php while($i<28): ?>
										<?php ($i++); ?>
										<?php ($clname = 'active'); ?>
										<li><a style="color:#000;" onclick="hiremedate('<?php echo e($i.'/'.date('m/Y'), false); ?>')"  href="#availableCalenderModal" rel="modal:open"><?php echo e($i, false); ?></a></li>
									<?php endwhile; ?>
									<li><span style="color:#CCC;">1</span></li>
								</ul>
								
								
								
								
							<?php } ?>
							</div>
							
							<div class="clearfix"></div>
							
							
<?php /* Review List Cleaner Profile */ ?>
				<div class="col-md-12 float-left">
		        	
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto; padding-left:0px;">
								<div class="box-content" id="userdetails"></div>
								<div class="box-content">
									<ul class="inbox-list" id="cleaner_reviewlist">
									</ul>
								</div>
							</div>
						</div>
					
				</div>
<?php /* Review List Cleaner Profile */ ?>


							
							
							
						</div>	
					</div>
	            <div class="clearfix"></div>	         
			</section>
		</main><!-- /main -->


		
		
		
		<section class="member-status right">
			<div class="sidebar-member">
				<ul class="member-tab">
					<li>
						<i class="fa fa-users" aria-hidden="true"></i>
					</li>
				</ul><!-- /.member-tab -->
				<div class="content-tab">
					<div class="scroll content">
						<ul class="member-list online">
							<li class="member-header">ONLINE</li>
							<?php $__currentLoopData = $userlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
								<?php if($val->type == 3): ?>
								<li>
									<a href="#" title="">
										<div class="avatar">
											<img src="/profile/<?php echo e($val->profile_image, false); ?>" style="max-width:50px;">
											<div class="status-color green heartbit"></div>
										</div>
										<div class="info-user">
											<p class="name"><?php echo e($val->name, false); ?></p>
											<p class="options">Rating : <?php echo e($val->rating, false); ?>/5, Task Complete : <?php echo e($val->task_complete, false); ?></p>
										</div>
										<div class="clearfix"></div>
									</a>		
								</li>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							
						</ul><!-- /.member-list online -->
						
						
					</div><!-- /.content scroll -->
					
				</div><!-- /.cotnent-tab -->
			</div><!-- /.sidebar-member -->
		</section><!-- /.member-status -->
		<!-- jQuery 3 -->
		<script src="/userdashboard/javascript/jquery.min.js"></script>

		<!-- Bootstrap 4 -->
		<script src="/userdashboard/javascript/tether.min.js"></script>
		<script src="/userdashboard/javascript/bootstrap4-alpha3.min.js"></script>

		<!-- Map chart  -->
		<script src="/userdashboard/javascript/ammap.js"></script>
		<script src="/userdashboard/javascript/worldLow.js"></script>

		<!-- Morris.js charts -->
		<script src="/userdashboard/javascript/raphael.min.js"></script>
		<script src="/userdashboard/javascript/morris.min.js"></script>

		<!-- Chart -->
		<script src="/userdashboard/javascript/Chart.min.js"></script>

		<!-- Calendar -->
		<script src='/userdashboard/javascript/moment.min.js'></script>
		<script src='/userdashboard/javascript/jquery-ui.js'></script>
		<script src='/userdashboard/javascript/fullcalendar.min.js'></script>

		<script type="text/javascript" src="/userdashboard/javascript/jquery.mCustomScrollbar.js"></script>
		<script src="/userdashboard/javascript/smoothscroll.js"></script>
		<script src="/userdashboard/javascript/waypoints.min.js"></script>
		<script src="/userdashboard/javascript/jquery-countTo.js"></script>
		<script src="/userdashboard/javascript/waves.min.js"></script>
		<script src="/userdashboard/javascript/canvasjs.min.js"></script>

		<script src="/userdashboard/javascript/main.js"></script>	
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
		<script src="/userdashboard/javascript/jquery-ui.js"></script>
		
		<!-- jQuery Modal -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
	



<div id="" class="modal hide fade">
	<div class="modal-body">
		<textarea class="form-control"></textarea>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
		<button type="button" data-dismiss="modal" class="btn">Cancel</button>
	</div>
</div>


<div id="availableCalenderModal" class="buytoken" style="display:none; max-width: 500px;width: 500px; padding:12px 22px; background-color:#1b1c20;">
	<div class="modal-body">
		<div class="box-header with-border customerdefault2">
			<div class="box-title">
				<div class="row stripebtn send_msg" style="padding:10px; margin:auto;">					
					<h3 style="text-align: center; padding: 10px; color: #1abc9c;">Hire ME ( <span class="cleanername"></span> )</h3>
					<h5 style="text-align: center; padding: 10px; color: #1abc9c;" class="sdtd"></h5>
					<ul id="calendarTime">
						<?php 
							$i = 6;
							while($i<20){
								$i++;
								if($i == 12)
									echo '<li style="cursor:pointer; margin:5px; padding: 12px; color: #fff;background-color: #1abc9c;width: 70px; float:left;" onclick="hiremetime('.$i.')">12 O\'Clock</li>';
								else
									echo '<li style="cursor:pointer; margin:5px; padding: 12px; color: #fff;background-color: #1abc9c;width: 70px; float:left;" onclick="hiremetime('.$i.')">'.$i.' '.($i<12?$i.'AM':($i-12).'PM').'</li>';
							}
						?>
					</ul>
				</div>
				<div class="row stripebtn send_msg_thanks" style="display:none;"></div>
			</div>
		</div>
	</div>				
</div>


<div id="sendmsgmodal" class="buytoken" style="display:none; max-width: 500px;width: 500px; padding:12px 22px;">
	<div class="modal-body">
		<div class="box-header with-border customerdefault2">
			<div class="box-title">
				<div class="row stripebtn send_msg">
					<textarea id="msg" class="form-control" style="margin-bottom:5px;"></textarea>
					<input onclick="sendmsg()" type="button" class="btn btn-success btn-sm" value="Submit" />
					<input type="button" class="btn btn-default btn-sm" value="Cancel" onclick="closemodal()" />
				</div>
				<div class="row stripebtn send_msg_thanks" style="display:none;"></div>
			</div>
		</div>
	</div>				
</div>
		

<div id="confirm" class="modal hide fade">
	<div class="modal-body smsg">
		Are you sure to accept the task?
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
		<button type="button" data-dismiss="modal" class="btn">Cancel</button>
	</div>
</div>

<div id="buytoken" class="buytoken" style="display:none; max-width: 500px;width: 500px;">
	<div class="modal-body">
		<div class="box-header with-border customerdefault2">
			<div class="box-title col-md-12">
				<div class="row stripebtn">
					<div class="col-md-12">
						<button class="btn btn-primary btn-block" onclick="pay(<?php echo e(config('data.OnetimeCleanerPlan'), false); ?>,'plan_GSQLWvdYPjkcGJ','One Time Cleaning')">One Time Cleaning</button>
					</div>
					<div class="col-md-12" style="padding-top:2%;padding-bottom:2%;">
						<button class="btn btn-success btn-block" onclick="pay(<?php echo e(config('data.expressCleanerCharge'), false); ?>,'plan_GSQCkEc344j1JU','Express Cleaning')">Express Cleaning</button>
					</div>
					<div class="col-md-12">
						<button class="btn btn-info btn-block" onclick="pay(<?php echo e(config('data.jumboCleanerPlan'), false); ?>,'plan_GSQB4nqOJPNamT','Express Jumbo')">Express Jumbo</button>
					</div>
				</div>	
			</div>
		</div>
	</div>				
</div>
		
<div id="schedulebookingmodal" class="modaltaskdecline" style="display:none; max-width: 600px;width: 600px; padding:15px; background-color:#1c1d21;">
	<div class="modal-body" style="padding-left:5px; padding-right:5px;">
		<div class="zzrows">
			<div class="col-md-12">
				<label class="col-md-5">Property Access Type</label>
				<div class="col-md-7">
					<input name="propertyAccessType" 	type="radio"	onclick="propertyAccessDetails()"	value="Key" 	checked	/> &nbsp Key &nbsp;<br>
					<input name="propertyAccessType" 	type="radio"  	onclick="propertyAccessDetails()"  	value="electronic_code"	/> &nbsp Electronic Code<br>
					<input name="propertyAccessType" 	type="radio" 	onclick="propertyAccessDetails()"	value="Others" 	 		/> &nbsp Others
				</div>
			</div>
			<div class='clearfix'></div>
			<div class="col-md-12 electronic_code_details" style="display:none;">
				<label class="col-md-4">Electronic Code:</label>
				<input type="text" id="electronic_code_details" placeholder="Electronic Code" style="height:30px; border-radius:0; padding:7px 12px; color:#000; background-color:#FFF; width:50%;;" /><br />				
			</div>			
			<div class="col-md-12 accessdetails" style="display:none;">
				<label class="col-md-4">Access Details</label>
				<input type="text" id="accessdetails" placeholder="Property Access Details" style="height:30px; border-radius:0; padding:7px 12px; color:#000; background-color:#FFF; width:50%;;" />
			</div>			
			<div class='clearfix'></div>
			<div class="col-md-12">
				<label class="col-md-4">Contact Person</label>
				<input type="text" id="contact_person" value="<?php echo e(Auth::user()->name, false); ?>" style="height:30px; border-radius:0; padding:7px 12px; color:#000; background-color:#FFF; width:50%;;" />
			</div>
			<div class='clearfix'></div>
			<div class="col-md-12">
				<label class="col-md-4">Contact Number</label>
				<input type="text" id="contact_phonenumber" value="<?php echo e(Auth::user()->phone_number, false); ?>" style="height:30px; border-radius:0; padding:7px 12px; color:#000; background-color:#FFF; width:50%;;" />
			</div>
			<div class='clearfix'></div>
			<div class="col-md-12">
				<label class="col-md-4">Bedroom</label>
				<textarea id="bedroomdetails" placeholder="Describe location of bedroom" style="padding:7px 12px; border-radius:0; border:1px solid #1c1d21; color:#000; background-color:#FFF; width:50%;;"></textarea>
			</div>
			<div class='clearfix'></div>
			<div class="col-md-12">
				<label class="col-md-4">Bathroom</label>
				<textarea id="bathroomdetails" placeholder="Describe location of bathroom" style="padding:7px 12px; border-radius:0; border:1px solid #1c1d21; color:#000; background-color:#FFF; width:50%;;"></textarea>
			</div>
			<div class='clearfix'></div>
			<div class="col-md-12" style="margin-top:2%;">
				<label class="col-md-4">&nbsp;</label>
				<button type="button" data-dismiss="modal" class="btn btn-primary btn-sm" onclick="bookcleaner()">Submit</button>
				<button type="button" data-dismiss="modal" class="btn btn-info btn-sm" onclick="closemodal()" >Cancel</button>			
			</div>
			<div class="col-md-12 electronic_code_details" style="display:none; padding-top:5px;">
				<p>- Your access code is not shared with the cleaner until the day of cleaning.</p>
				<p>- it is stored securely in an encrypted-format inaccessible by anyone else.</p>
				<p>- it is destroyed after the completion of the cleaning.</p>								
			</div>			
		</div>
	</div>
</div>
<a href="#schedulebookingmodal" id="schedulebooking" style="display:none;" rel="modal:open">Open Modal</a>


<div id="searchCleanerModal" style="display:none; max-width:600px;width:600px; padding:12px 2px;">
	<div class="modal-body">
		<div class="box-header with-border customerdefault2">
			<div class="box-title" style="color:#000;" >
					<div class="rows" style="">
					<div class="col-md-4" style="padding-top:5px; margin-bottom:10px;">Select Plan</div>
					<div class="col-md-8">
						<?php echo $plan_option; ?>

					</div>
					<div class="box-title col-md-4" style="margin-bottom:5px;"></div>
					<div class="box-title col-md-8" style="margin-bottom:15px; padding-left:40px; font-size:10pt; color:#bc2653;" id="remainingToken"></div>
							
					<div class='clearfix'></div>
					<div class="col-md-4" style="padding-top:5px;">Select Date & Time</div>
					<div class="col-md-8">
						<input id="selectedDate" value="<?php echo e(date('d/m/Y',strtotime('+1 days')), false); ?>" placeholder="dd/mm/yyyy" type="text" class="datepicker" style="height:30px; padding:7px 12px; color:#000; width:100px;;" onchange="searchcleaner()" />				
					</div>
					<div class='clearfix'></div>
					</div>
					<div class="rows">
						<?php ($i = config('data.start_time')); ?>
						<?php ($j = 1); ?>
						<input type="hidden" id="selectedTime" value="<?php echo e(($i+1).':00:00-'.($i+2).':00:00', false); ?>" />						
						<?php while($i<config('data.end_time')): ?>
							<?php ($i++); ?>
							<?php ($strtime = $i.':00:00-'.($i+1).':00:00'); ?>
							<div class="dtdColor" id="timeDiv_<?php echo e($i, false); ?>" onclick="setTime('<?php echo e($i, false); ?>')" >
								<?php if($i<12): ?>
									<?php echo e($i, false); ?> AM
								<?php elseif($i==12): ?>
									12 PM
								<?php else: ?>
									<?php echo e(($i-12), false); ?> PM
								<?php endif; ?>
							</div>
						<?php endwhile; ?>
						<div class='clearfix'></div>
					</div>
					<div class="rows">
					<div class="col-md-12" style="text-align:center;">
						<input type="button" class="btn btn-info" value="Search Cleaner" style="background-color:#1abc9c;" onclick="javascript: $('.close-modal').click();" />
					</div>
					
					</div>
			</div>
		</div>
	</div>				
</div>



		
<!--start-->
<div style="display:none;">
	<div id="ninja-slider">
		<div class="slider-inner">
			<ul>
				<?php $__currentLoopData = $allimages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li>
					<a class="ns-img" href="/room_img/<?php echo e($val['src'], false); ?>"></a>
					<div class="caption">
						<h3>Job ID : <?php echo e($val['jobid'], false); ?></h3>
					</div>
				</li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                    
			</ul>
			<div id="fsBtn" class="fs-icon" title="Expand/Close"></div>
		</div>
	</div>
</div>

<!--end-->
<style>


</style>



<link href="/gallery/ninja-slider.css" rel="stylesheet" type="text/css" />
<script src="/gallery/ninja-slider.js" type="text/javascript"></script>
<script>
	function lightbox(idx) {
		//show the slider's wrapper: this is required when the transitionType has been set to "slide" in the ninja-slider.js
		var ninjaSldr = document.getElementById("ninja-slider");
		ninjaSldr.parentNode.style.display = "block";

		nslider.init(idx);

		var fsBtn = document.getElementById("fsBtn");
		fsBtn.click();
	}

	function fsIconClick(isFullscreen, ninjaSldr) { //fsIconClick is the default event handler of the fullscreen button
		if (isFullscreen) {
			ninjaSldr.parentNode.style.display = "none";
		}
	}
</script>

<script>
var msgReceiverId 	= 0;
var profileImg 		= '';
var selectedTimeDiv	= <?php echo e(config('data.start_time'), false); ?>+1;
$(document).ready(function(){
	$('.profileEdit').hide();
	$('.profileView').show();
	profileImg = '<?php echo e(Auth::user()->profile_image, false); ?>';
	$('.nextMonth').hide();
});


nextMonth = function(){
	$('.currentMonth').hide();
	$('.nextMonth').show();
}

currentMonth = function(){
	$('.nextMonth').hide();
	$('.currentMonth').show();
}


setTime = function(){
	var i = arguments[0];
	var k = parseInt(i)+1;
	var j = i+':00:00-'+k+':00:00';
	$('#selectedTime').val(j);
	$('#timeDiv_'+selectedTimeDiv).removeClass('dtdColorSelected');
	$('#timeDiv_'+i).addClass('dtdColorSelected');
	selectedTimeDiv = i;
	searchcleaner();
	setTimeout(function() {closemodal();},3000);
}

hiremedate = function(){
	//$('#selectedPlan').val();
	//$('#selectedTime').val();
	$('#selectedDate').val(arguments[0]);
	$('.sdtd').html(arguments[0]);
	//$('#selectedLocation').val();
	$('#calendarTime').html('<li class="cleanertime" style="font-size:12px; color:#FFF; width:100%; text-align:center;" >please wait.</li>');
	
	var data = 'dtd='+arguments[0]+'&cleanerId='+msgReceiverId;
	$.post('cleanerCalendarAvailableTime', data)
	.success(
		function(res){
			//$('#availability_status').val(1);
			$('#calendarTime').html(res);
			//$('#calendarTime').show();
			//$('#myaclendarfrm').show();
		}
	);		
	
	
	
	
	/*
	var sdtd = arguments[0];
	//$('#msg01').val('');
	$.ajax({
		type: 'GET',
		data: {
			"_token": "<?php echo e(csrf_token(), false); ?>",
			"msg":cleanerId,
			"msgReceiverId":msgReceiverId
		},
		url:'sendmsg',
		dataType: 'json',
		success:function(res){
			$('#msg01').val('');
			//alert('message sent successfully.');
			//$('.send_msg').hide();
			$('.send_msg_thanks').show();
			$('.send_msg_thanks').html('<h4 style="text-align:center; color:#5cb85c;">Message sent to cleaner successfully!</h4>');
		}
	});		*/
	
}

hiremetime = function(){
	
	
	
	//cleanerId 	= msgReceiverId;
	console.log(cleanerId);
	var stime 	= arguments[0];
	var strtime	= stime+':00:00-'+(stime+1)+':00:00';
	//01-02-2020 :: need to uncomment asap
	//$("#selectedTime option[value='"+strtime+"']").attr('selected', 'selected'); 
	
	//alert(4);
	schedulebooking(cleanerId);/* */
}

msghistory = function(){
	var id = arguments[0];
	msgReceiverId = id;
	$('.msgusername').html(arguments[1]);
	$('.allmsg').hide();
	$('.urser_'+id).show();
	$('#msgdetails').show();
	
	var data = 'id='+id;
	$.post('msghistory/'+id, data)
	.success(
		function(res){
			
		}
	);	
}


setHiremeId = function(){
	hiremeCleanerId = arguments[0];
}

setReceiverId = function(){
	msgReceiverId = arguments[0];
}

sendmsg01 = function(){
	var msg = $('#msg01').val();
	//$('#msg01').val('');
	$.ajax({
		type: 'POST',
		data: {
			"_token": "<?php echo e(csrf_token(), false); ?>",
			"msg":msg,
			"msgReceiverId":msgReceiverId
		},
		url:'sendmsg',
		dataType: 'json',
		success:function(res){
			$('#msg01').val('');
			//alert('message sent successfully.');
			//$('.send_msg').hide();
			$('.send_msg_thanks').show();
			$('.send_msg_thanks').html('<h4 style="text-align:center; color:#5cb85c;">Message sent to cleaner successfully!</h4>');
		}
	});	
}


sendmsg = function(){
	var msg = $('#msg').val();
	//alert(msg);
	//alert(msgReceiverId);
	
	//var rid = msgReceiverId;
	$.ajax({
		type: 'POST',
		data: {
			"_token": "<?php echo e(csrf_token(), false); ?>",
			"msg":msg,
			"msgReceiverId":msgReceiverId
		},
		url:'sendmsg',
		dataType: 'json',
		success:function(res){
			$('.send_msg').hide();
			$('.send_msg_thanks').show();
			$('.send_msg_thanks').html('<h4 style="text-align:center; color:#5cb85c;">Message sent to cleaner successfully!</h4>');
		}
	});
}



editProfile = function(){
	$('.profileView').hide();
	$('.profileEdit').show();
}

cancelEditProfile = function(){
	$('.profileEdit').hide();
	$('.profileView').show();
}

var cleanerId 	= 0;
schedulebooking = function(){
	cleanerId 	= arguments[0];
	//closemodal();
	//alert(876);
	$('#schedulebooking').click();
}

propertyAccessDetails = function(){
	$('.accessdetails').hide();
	$('.electronic_code_details').hide();
	
	if($("input[name='propertyAccessType']:checked"). val() == "Others"){
		$('.accessdetails').show();
	}
	else if($("input[name='propertyAccessType']:checked"). val() == "electronic_code"){
		$('.electronic_code_details').show();
	}
	
	
	
}
		
bookcleaner = function(){
	var selectedPlan		= $("#selectedPlan"). val();
	var selectedTime		= $('#selectedTime').val();
	var selectedDate		= $('#selectedDate').val();
	var selectedLocation	= $('#selectedLocation').val();

	var propertyAccessType 	= $("input[name='propertyAccessType']:checked"). val();
	var accessdetails		= $('#accessdetails').val();
	var contact_person		= $('#contact_person').val();
	var contact_phonenumber	= $('#contact_phonenumber').val();
	var bathroomdetails		= $('#bathroomdetails').val();
	var bedroomdetails 		= $('#bedroomdetails').val();

	
	console.log("cleanerId :: "+cleanerId+" ---selectedPlan :: "+selectedPlan+" ---selectedTime :: "+selectedTime+" ---selectedDate :: "+selectedDate+" ---selectedLocation :: "+selectedLocation+" ---propertyAccessType :: "+propertyAccessType+" ---accessdetails :: "+accessdetails+" ---contact_person :: "+contact_person+" ---contact_phonenumber :: "+contact_phonenumber+" ---bathroomdetails :: "+bathroomdetails+" ---bedroomdetails :: "+bedroomdetails);
	
	
	
	//$('.customerdefault').html('Please wait your request in processing in our system.');					
	//closemodal();
	console.log('+++++++++++');
	$.ajax({
		type: 'POST',
		data: {
			"_token": "<?php echo e(csrf_token(), false); ?>",
			"cleanerId":cleanerId,
			"selectedPlan":selectedPlan,
			"selectedTime":selectedTime,
			"selectedDate":selectedDate,
			"selectedLocation":selectedLocation,
			"propertyAccessType":propertyAccessType,
			"accessdetails":accessdetails,
			"contact_person":contact_person,
			"contact_phonenumber":contact_phonenumber,
			"bathroomdetails":bathroomdetails,
			"bedroomdetails":bedroomdetails
		},
		url:'/bookcleaner/'+cleanerId,
		dataType: 'json',
		success:function(res){
			if(res.status == 1){
				$('.customerdefault').html(res.msg);
				$('.smsg').html(res.msg);
				alert(res.msg);
				$('#confirm').show();
				setTimeout(function() { location.reload(true); }, 5000);
			}
			else{
				alert('Selected cleaner not avail at selected date and time, Please select other cleaner!');
			}
		}
	});			
}



markascomplete = function(){
	var id = arguments[0];
	$.ajax({
		type: 'POST',
		data: {
			"_token": "<?php echo e(csrf_token(), false); ?>",
			"id":id,
			"type":'complete'
		},
		url:'/markascomplete/'+arguments[0],
		dataType: 'json',
		success:function(res){
			alert('Job mark as completed.');
			setTimeout(function() { location.reload(true); }, 5000);
		}
	});
}


var disputeId = 0;

markasdispute = function(){
	disputeId = arguments[0];
	$("#disputeFrmHref").click();
	//alert(disputeId);
}	
markasdisputesbmt = function(){	
	var disputeMsg = $('#disputeMsg').val();
	$.ajax({
		type: 'POST',
		data: {
			"_token": "<?php echo e(csrf_token(), false); ?>",
			"id":disputeId,
			"type":'dispute',
			"disputeMsg":disputeMsg,
		},
		url:'/markascomplete/'+disputeId,
		dataType: 'json',
		success:function(res){
			$('.disputeMsgDiv').html(res.msg);
			setTimeout(function() { location.reload(true); }, 5000);
		}
	});
}


userdetails = function(){
	$('.CleanerProfile').click();

	$('#cleaner_name').html('');
	$('#cleaner_joining_yr').html('');
	$('#cleaner_emailid').html('');
	$('#cleaner_govtid_verified').html('');
	$('#cleaner_contactno').html('');
	$('#cleaner_rating').html('');
	$('#cleaner_taskcomplete').html('');
	$('#userdetails').html('');
	$('#cleaner_profile_image').html('');
	$('.send_msg_thanks').hide();
	$('.send_msg').show();
	$('#msg').val('');
	
	cleanerId = arguments[0];
	
	$.ajax({
		type: 'GET',
		url:'/cleaner/'+arguments[0],
		dataType: 'json',
		success:function(res){
			$('#cleaner_name').html(res.name);
			$('#cleaner_joining_yr').html('Joined in '+res.created_at);
			$('#cleaner_emailid').html(res.email_verified);
			$('#cleaner_govtid_verified').html(res.govtid_verified);
			$('#cleaner_contactno').html(res.phone_number_verified);
			
			$('#cleaner_speaks').html(res.speaks);
			$('#cleaner_rating').html(res.rating);
			$('#cleaner_taskcomplete').html(res.task_complete);
			$('#userdetails').html(res.details);
			$('#cleaner_profile_image').html('<img src="/profile/'+res.profile_image+'" width="80%" style="max-width:250px;" />');			
			//$('#msgbtn_hiremebtn').html('<a href="#sendmsgmodal" onclick="setReceiverId('+res.id+')" class="btn btn-success btn-sm" rel="modal:open">Send Message</a> &nbsp; &nbsp;	<a href="#hirememodal" onclick="setHiremeId('+res.id+')" class="btn btn-info btn-sm" rel="modal:open">Hire Me</a>');
			$('#msgbtn_hiremebtn').html('<a href="#sendmsgmodal" onclick="setReceiverId('+res.id+')" class="btn btn-success btn-sm" rel="modal:open">Send Message</a> &nbsp; &nbsp;	<a href="#" onclick="hireCleaner()" class="btn btn-info btn-sm">Hire Me</a>');
			
			$('.cleanername').html(res.name);
			msgReceiverId = res.id;
			//alert(msgReceiverId);
			
			var str = '';
			var i = 0;
			var star_str = '';
			$.each(res.reviewlist, function( index, value ) {
				i = 0;
				star_str = '';
				while(value.rating>i){
					i += 1;
					star_str += '<i class="fa fa-star" style="font-size: large;" ></i>';
				}
				str += '<li class="waves-effect"><div class="left"><img src="/profile/'+value.profile_image+'" style="max-width:50px;"><div class="info"><p class="name">'+value.customer_name+'</p><p>Joined in '+value.created_at+'</p></div></div><div class="right"><p>'+star_str+'</p></div><div class="clearfix"></div><div  class="left" style="padding-top:5px; right:0px;" ><p>'+value.rating_msg+'</p></div></li>'
			});
			$('#cleaner_reviewlist').html(str);			
		}
	});
}

hireCleaner = function(){
	$('.active').click();
	
}


closemodal = function(){
	/*if($('.close-modal').length){
		$('.close-modal').click();
	}*/
}


		$( function() {
			$('.button-menu-right').click();
			$( ".datepicker" ).datepicker({
				dateFormat: 'dd/mm/yy',
				minDate: new Date(),
				maxDate: '+1M'
			});
			
			//setTimeout(function() { searchcleaner(); }, 2000);			
		} );
		hidecleanerlist = function(){
			$('.cleanerdiv').hide();
			$('.loading').show();
			setTimeout(function() { showcleanerlist(); }, 4000);
		}
		showcleanerlist = function(){
			$('.loading').hide();
			$('.cleanerdiv').show();
		}

		changePlan = function(){
			var x = arguments[0];
			$("#selectedPlan").prop('checked', false);
			$("#selectedPlan2").prop('checked', false);
			$("#selectedPlan3").prop('checked', false);
			searchcleaner();
			if(x == 1){
				$('#sp1').css('background-color',"#bc2653");
				$('#sp2').css('background-color',"#1abc9c");
				$('#sp3').css('background-color',"#1abc9c");
				$("#selectedPlan").val(1);
				//$("input:radio[name='selectedPlan']")[0].checked = true;
			}
			else if(x == 2){
				$('#sp2').css('background-color',"#bc2653");
				$('#sp1').css('background-color',"#1abc9c");
				$('#sp3').css('background-color',"#1abc9c");
				$("#selectedPlan").val(2);
				//$("input:radio[name='selectedPlan']")[1].checked = true;
			}
			else if(x == 3){
				$('#sp3').css('background-color',"#bc2653");
				$('#sp1').css('background-color',"#1abc9c");
				$('#sp2').css('background-color',"#1abc9c");
				$("#selectedPlan").val(3);
				//$("input:radio[name='selectedPlan']")[2].checked = true;		
			}
		}

		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		searchcleaner = function(){
			var selectedTime		= $('#selectedTime').val();
			var selectedDate		= $('#selectedDate').val();
			var selectedLocation	= $('#selectedLocation').val();
			var selectedPlan		= $("#selectedPlan"). val();
			var remainingToken		= 0;
			
			//alert(selectedPlan);
			
			if(selectedPlan == 1){
				remainingToken	= <?php echo e($tokenArr['onetimeclean'], false); ?>;
			}
			else if(selectedPlan == 2){
				remainingToken	= <?php echo e($tokenArr['expressclean'], false); ?>;
			}
			else if(selectedPlan == 3){
				remainingToken	= <?php echo e($tokenArr['jumboclean'], false); ?>;				
			}			
			$('#remainingToken').html('Remaining Token : '+remainingToken);
			if(remainingToken<1){
				$('#token_btn').click();
				$('.stripebtn').append('<div class="col-md-12" style="color:#F00; padding-top:15px; text-align:center;">You dont have remaining token for cleaning......</div>');
				
				$('.cleanerfrm').hide();
				
				return false;
			}
			
			//alert(888);
			$('.cleanerdiv').hide();
			$('.loading').show();			
			$.ajax({
				data: {
					"_token": "<?php echo e(csrf_token(), false); ?>",
					"selectedTime":selectedTime,
					"selectedDate":selectedDate,
					"selectedLocation":selectedLocation,
					"selectedPlan":selectedPlan
				},
				url:'/searchcleaner',
				dataType: 'json',
				type: 'POST',
				success:function(res){
					var str = '<li class="waves-effect"><a href="#" title=""><div class="left">Available Cleaner List On Selected Date-Time</div></a></li>';
					
					var ijk = 0;
					var clrNum = 0;
					while(ijk<1){ijk++;
					$.each(res, function( index, value ) {
						/*str += '<li class="waves-effect col-md-4"><a title="'+value.name+'"><div class="left"><img src="/profile/'+value.profile_image+'" style="max-width:50px;"><div class="info"><p class="name">'+value.name+'</p><p>Rating : '+value.rating+'/5, Task Complete : '+value.task_complete+'</p></div></div><div class="right"><input type="button" value="Select Cleaner" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  onclick="schedulebooking('+value.id+')" /><input type="button" value="View Profile" style="height:30px; margin-left:5px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  onclick="userdetails('+value.id+')" /></div><div class="clearfix"></div></a></li>';	*/
						/*str += '<li class="waves-effect col-md-12" onclick="selectCleaner('+value.id+')"><a title="'+value.name+'" id="clrDiv_'+value.id+'"><div class="left"><img src="/profile/'+value.profile_image+'" style="max-width:50px;"><div class="info"><p class="name">'+value.name+'<input type="checkbox" style="float:right;" name="clrId['+value.id+']" id="clr_'+value.id+'" value="'+value.id+'" /></p><p>Rating : '+value.rating+'/5, Task Complete : '+value.task_complete+'</p></div></div><div class="clearfix"></div></a></li>';*/

						str += '<li class="waves-effect col-md-12" onclick="userdetails('+value.id+')"><a title="'+value.name+'" id="clrDiv_'+value.id+'"><div class="left"><img src="/profile/'+value.profile_image+'" style="max-width:50px;"><div class="info"><p class="name">'+value.name+'</p><p>Rating : '+value.rating+'/5, Task Complete : '+value.task_complete+'</p></div></div><div class="clearfix"></div></a></li>';



						
						clrNum++;
						if(clrNum%3 == 0){
							str += "<div class='clearfix'></div>";
						}
					});
					}
					$('.cleanerlist').html(str);					
					$('.loading').hide();		
					$('.cleanerdiv').show();
					
					
				}
			});
			
		}
		

		</script>

<script src="https://checkout.stripe.com/checkout.js"></script>

<script type="text/javascript">
var cleanerArr = [];
var cleanerIdStr	= '';
selectCleaner = function(){
	var i = arguments[0];
	if($('#clr_'+i). prop("checked") == false){
		cleanerArr.push(i); 
		$('.sendMsgCleaner').show();
		$("#clr_"+i). prop("checked", true);
		$('#clrDiv_'+i).addClass('cleanerSelected');
	}
	else{
		$("#clr_"+i). prop("checked", false);
		$('#clrDiv_'+i).removeClass('cleanerSelected');
	}
	
	/*$.each($("input[name='clrId']:checked"), function(){
		alert();
		$('.sendMsgCleaner').show();
	});*/
}

sendRequestMsg = function(){
	cleanerId = '';
	$.each(cleanerArr,function(i,j){
		cleanerId += j+'_';
	});
	$('#schedulebooking').click();
}


function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#image_upload_preview').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

$("#inputFile").change(function () {
	readURL(this);
});



$(document).ready(function () {  
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
});


var canid = 0;
cancelschedule = function(){
	canid = arguments[0];
	$("#modalhrefcancel").click();
}

cancelschedulesbmt = function(){
	/*if(confirm('Are you sure to cancel this cleaning schedule ?')){	}*/
		
		//var canid = $('#cancid').val();
		closemodal();
		$('#schedule_'+canid).hide();
		$.ajax({
			type: 'POST',
			data: {
				"_token": "<?php echo e(csrf_token(), false); ?>",
			},
			url:'/cancelschedule/'+canid,
			dataType: 'json',
			success:function(res){
				alert(res.msg);
			}
		});
	
	
}




  function pay(amount,plan,description) {
    var handler = StripeCheckout.configure({
      key: 'pk_test_4V5q3TkFoDgi2nloUPnjdLPL004PeEOnwo', // your publisher key id
      locale: 'auto',
      token: function (token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        console.log('Token Created!!');
        console.log(token)
        $('#token_response').html(JSON.stringify(token));

        $.ajax({
          url: '<?php echo e(route('stripe.store'), false); ?>',
          method: 'post',
		  dataType: 'JSON',
          data: { email:token.email, tokenId: token.id, amount: amount, plan:plan, description:description },
          success: (response) => {
			  
			$('.stripebtn').html('<div class="col-md-12"><div class="btn btn-success btn-block" >Payment complete successfully!<br>Order ID : '+response.id+'</div></div>');
			setTimeout(function() { location.reload(true); }, 5000);
		  },
          error: (error) => {
            console.log(error);
            alert('There is an error in processing.')
          }
        })
      }
    });
   
    handler.open({
      name: 'House Clean',
      description: description,
      amount: amount * 100
    });
}


review = function(){
	schedule_id = arguments[0];
	cleaner_id	= arguments[1];
	$("#modalhref").click();
}

</script>		
<script>
$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
	
    if (ratingValue > 1) {
        msg = "Thanks! You rated " + ratingValue + " stars to this cleaner.";
    }
    else {
        msg = "We will improve ourselves. You rated " + ratingValue + " stars to this cleaner.";
    }
    responseMessage(msg);
    
  });
  
  
});


function responseMessage(msg) {
  $('.success-box').fadeIn(200);  
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
}
</script>		
	</body>
</html><?php /**PATH D:\server734\htdocs\projects\laravel\airbnb\resources\views/home.blade.php ENDPATH**/ ?>